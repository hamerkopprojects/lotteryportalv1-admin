<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ticket_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function ticket_list() {
        $this->db->select('*');
        $this->db->from('tickets');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function get_todays_ticket_list($date) {
        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('date', $date);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function update($Details, $id) {
        $this->db->where('id', $id);
        $this->db->update('tickets', $Details);
    }

    function todaysTicketsCount() {

        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('date', date('Y-m-d'));
        $query = $this->db->get();
        $result = $query->result_array();

        return count($result);
    }

    function getTicketDetails($ticket_id) {

        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('id', $ticket_id);
        $query = $this->db->get();
        $result = $query->row_array();

        return $result;
    }

    function get_ticket_code($ticketid) {
        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('id', $ticketid);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function checkTicketUpdated($ticketname) {

        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('ticket_name', $ticketname);
        $this->db->where('date', date('Y-m-d'));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function updateTodaysTicket($ticketname, $drawcode) {
        $data = array(
            'date' => date('Y-m-d'),
            'draw_code' => $drawcode,
        );
        $this->db->where('ticket_name', $ticketname);
        $this->db->update('tickets', $data);
    }

    function clearunsolddata($ticket, $unsold_type) {
        $date = date('Y-m-d');
        $this->db->where('unsold_data.tickets_id', $ticket);
        $this->db->where('unsold_data.type', $unsold_type);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->delete('unsold_data');
    }

    function clearunsoldconsolidated($ticket, $unsold_type) {
        $date = date('Y-m-d');
        $this->db->where('consolidated_sold_data.tickets_id', $ticket);
        $this->db->where('consolidated_sold_data.type', $unsold_type);
        $this->db->where('consolidated_sold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->delete('consolidated_sold_data');
    }

    function checkPrizeIsWon($parameters = array(), $digitcondition) {

        $date = date('Y-m-d');

        $this->db->select('unsold_data.ticket_code as ticket_code,unsold_data.type as unsold_type , unsold_data.ticket_from as ticket_from, '
                . 'unsold_data.ticket_to as ticket_to, user.firstname as firstname, tickets.ticket_name as ticket_name, '
                . 'user.lastname as lastname, unsold_data.user_id as userid, unsold_data.id as unsoldid');
        $this->db->from('unsold_data');
        $this->db->join('user', 'user.id = unsold_data.user_id');
        $this->db->join('tickets', 'tickets.id = unsold_data.tickets_id');
        $this->db->where('tickets.ticket_name', $parameters['ticket_name']);
        $this->db->where('unsold_data.type', $parameters['type']);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $query = $this->db->get();
        $unsolddetails = $query->result_array();
//        echo '<pre>';
//        print_r($unsolddetails);
//        exit;
        $ticketcodearrs = array();
        $winningshops = array();
        //echo $digitcondition;
        foreach ($unsolddetails as $key => $val) {
            $ticketcode = trim($unsolddetails[$key]['ticket_code']);
            $ticketfrom = trim($unsolddetails[$key]['ticket_from']);
            $ticketto = trim($unsolddetails[$key]['ticket_to']);
            $ticket_name = trim($unsolddetails[$key]['ticket_name']);
            $unsold_type = trim($unsolddetails[$key]['unsold_type']);
            $shopname = $unsolddetails[$key]['firstname'] . ' ' . $unsolddetails[$key]['lastname'];

            if ($digitcondition == "fulldigits") {
                $ticketcodearrs = explode(",", $ticketcode);
                $won_serialarr = explode(" ", $parameters['serial_number']);
                $serial_code = trim($won_serialarr[0]);
                $serial_number = trim($won_serialarr[1]);
                if (in_array($serial_code, $ticketcodearrs)) {


                    if (($serial_number >= $ticketfrom) && ($serial_number <= $ticketto)) {
                        $winningshops[] = array(
                            'won_ticket' => $serial_code . ' ' . $serial_number,
                            'ticket_number' => $serial_code . ' ' . $serial_number,
                            'ticket_type' => $parameters['price_type'],
                            'shop_name' => $shopname,
                            'price_amount' => $parameters['price_amount'],
                            'ticket_name' => $ticket_name,
                            'unsold_type'=> $unsold_type
                        );
                    }
                }
            }

            if ($digitcondition == "lastfourdigits") {
                $won_serialarr = explode(" ", $parameters['serial_number']);
                $display_serial_number = trim($won_serialarr[1]);
                
                $serial_number = $parameters['serial_number'];
                $fromfirsttwo = substr($ticketfrom, 0, 2);
                $tofirsttwo = substr($ticketto, 0, 2);
                $ticketfrom = substr($ticketfrom, 2);
                $ticketto = substr($ticketto, 2);

                //exit;
                if (($serial_number >= (int) $ticketfrom) && ($serial_number <= (int) $ticketto)) {
                    $winningshops[] = array(
                        'won_ticket' => $parameters['serial_number'],
                        'ticket_number' => $ticketcode . ' ' . $fromfirsttwo.$parameters['serial_number'], 
                        'ticket_type' => $parameters['price_type'],
                        'shop_name' => $shopname,
                        'price_amount' => $parameters['price_amount'],
                        'ticket_name' => $ticket_name,
                        'unsold_type'=> $unsold_type
                    );
                }
            }
        }

        return $winningshops;
    }

    public function savewonshopdetails($data) {
        $insert = $this->db->insert("daily_winning", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function checkWinningSaved($ticket, $date, $type) {

        $this->db->select('*');
        $this->db->from('daily_winning');
        $this->db->where('date', $date);
        $this->db->where('ticket_id', $ticket);
        $this->db->where('type', $type);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

   function clearwinningddata($ticket) {
        $date = date('Y-m-d');
      //  $this->db->where('daily_winning.id', $ticket);
      //  $this->db->where('daily_winning.type', $unsold_type);
        $this->db->where('daily_winning.date', $date);
        $this->db->where('daily_winning.ticket_id', $ticket);
       // $this->db->where('daily_winning.date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->delete('daily_winning');
    }
    
    function getDcDetails($params){
     $this->db->select('*');
     $this->db->from('daily_dc');
     $this->db->where('date', $params['date']);
     $this->db->where('bill_no', $params['billno']);
     $this->db->where('shop_code',$params['shopid']);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
        
    }
    
    function getTotalDc($params){
     $this->db->select('sum(total_dc) as totaldc');
     $this->db->from('daily_dc');
     $this->db->where('date', $params['date']);
     $this->db->where('bill_no', $params['billno']);
     $this->db->where('shop_code',$params['shopid']);
     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
        
    }
    
    function reportTotalUnsold($date, $ticket){
        
     $this->db->select('sum(unsold_data.ticket_quantity) as totalunsolds');
     $this->db->from('unsold_data');
     $this->db->where('unsold_data.sold_date BETWEEN "'. "$date 00:00:00". '" and "'. "$date  23:59:59".'"');
     $this->db->where('unsold_data.tickets_id', $ticket);

     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
        
        
    }
    function reportTotalDc($date, $ticket){
        
     $this->db->select('sum(daily_dc.total_dc) as totaldc');
     $this->db->from('daily_dc');
     $this->db->where('daily_dc.date' , $date);
     $this->db->where('daily_dc.ticket_id', $ticket);

     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
        
        
    }
    
    function reportTotalWinning($date, $ticket){
        
     $this->db->select('daily_winning.winning_details');
     $this->db->from('daily_winning');
     $this->db->where('daily_winning.date' , $date);
     $this->db->where('daily_winning.ticket_id', $ticket);

     $query = $this->db->get();
     $result = $query->result_array();
     return $result;
        
        
    }
    
    function get_todays_ticket($day) {
        $this->db->select('*');
        $this->db->from('tickets');
        $this->db->where('day', $day);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }
    
function get_unsold_from_dc($shopcode, $date){
        //exit;
    $date = explode('/', $date);
    $date  = date('Y-m-d', strtotime(implode('-', array_reverse($date))));
     $this->db->select('sum(daily_dc.total_dc) as totaldc');
     //$this->db->select('daily_dc.total_dc as totaldc');
     $this->db->from('daily_dc');
     $this->db->where('daily_dc.bill_no' , 0);
     $this->db->where('daily_dc.date' , $date);
     $this->db->where('daily_dc.shop_code' , $shopcode);
     //$this->db->where('daily_dc.ticket_id', $ticket);
     
     $query = $this->db->get();
     $result = $query->result_array();
      
     $totaldc = (isset($result[0]['totaldc']))?$result[0]['totaldc']:0;
     return $totaldc;
        
    }
    
}
