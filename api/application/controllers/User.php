<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        error_reporting(E_ALL);
        $this->load->model('Lottery_model');
        $this->load->model('Ticket_model');
        $this->load->model('keys');
        $this->load->helper('directory');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /**
     * To login
     */
    public function login_post() {
        if ($this->post('username') && $this->post('password') && $this->post('user_type')) {
//        if ($this->post('username') && $this->post('password')) {
            $con['conditions'] = array(
                'username' => $this->post('username'),
                'password' => $this->post('password'),
                'user_type' => $this->post('user_type'),
                //'status' => 'ACTIVE'
            );
            $checkLogin = $this->Lottery_model->login($con);
            if ($checkLogin) {
                $generatedKey = $this->_generate_key();
                $data = array(
                    'user_id' => $checkLogin['id'],
                    'key' => $generatedKey,
                    'level' => '1'
                );
                $return = $this->keys->insert($data);
                $condn = array();
                $condn['user_id'] = $checkLogin['id'];
                $loginData = array();
                $loginData['logged_in'] = date('Y-m-d H:i:s');
                $this->Lottery_model->logUpdate($loginData, $condn);
                $message = [
                    'status' => TRUE,
                    'message' => 'Logged in successfully',
                    'user_id' => $checkLogin['id'],
                    'shop_code' => $checkLogin['shop_code'],
                    'key' => $generatedKey
                ];
            } else {
                $message = 'Invalid Username and Password';
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => $message,
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'username or Password not supplied'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    /**
     * To logout
     */
    public function logout_post() {
        if ($this->post('key')) {
            $message = [
                'status' => TRUE,
                'message' => 'Logged out successfully'
            ];
            $conditions = array(
                'key' => $this->post('key')
            );
            $this->keys->delete($conditions);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Wrong Try'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    /**
     * To handle change password
     */
    public function change_password_() {
        if ($this->post('email') && $this->post('newpassword') && $this->post('key')) {
            $conditions = array(
                'email' => $this->post('email'),
                'status' => 'ACTIVE'
            );
            $checkUser = $this->directors->checkUser($conditions);
            if ($checkUser > 0) {
                $userId = $checkUser;
                $keycodn['conditions'] = array(
                    'user_id' => $userId,
                    'key' => $this->post('key')
                );
                $available = $this->keys->getRows($keycodn);
                if (!$available) {
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Wrong Try'
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code                      
                } else {
                    $this->directors->changePassword($userId, $this->post('newpassword'));

                    $message = [
                        'status' => TRUE,
                        'message' => 'Password has been changed successfully'
                    ];
                }
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Email or User does not exists'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code                 
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Wrong Try'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function domains_get() {
        $domains = $this->Lottery_model->listAgency();
        // echo '<pre>';print_r($domains);exit;
        $message = [
            'status' => TRUE,
            'message' => 'successfully fetched domains',
            'domains' => $domains
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function saveunsoldetails_post() {
        if (($this->post('date')) || ($this->post('type')) || ($this->post('selectedticket')) || ($this->post('user_id'))) {
            // if (($this->post('date'))&&($this->post('type'))&&($this->post('selectedticket'))){
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
//            }
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }

            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            $data['ticket_details'] = $this->Lottery_model->getTicketDetails($data['selectedticket']);
            $selected_ticketname = $data['ticket_details']['ticket_name'];
            //echo '<pre>'; print_r($data['ticket_details']); exit;
            $unsold_type = $data['unsold_type'];
            $unsoldarray = array();
            $shopcodes = $this->Lottery_model->getShopCodes();

            $dateofunsold = explode('-', $data['date_of_unsold']);
            $sold_date = $dateofunsold[0] . '-' . $dateofunsold[1] . '-' . $dateofunsold[2];
            $curdate = $dateofunsold[0] . '/' . $dateofunsold[1] . '/' . $dateofunsold[2];
            $dcdate = $dateofunsold[2] . '-' . $dateofunsold[1] . '-' . $dateofunsold[0];
            $winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];

            $p = 0;
            //echo 'one';exit;
            $host = $_SERVER['HTTP_HOST'];

            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_unsoldretail/$curdate/");
            } else {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/data_unsoldretail/$curdate/");
            }
            //echo $files;
            //echo 'one';exit;
            //echo '<pre>'; print_r($files); exit;

            foreach ($files as $key => $filenames) {

                $filename = explode("_", $filenames);
                $shopcode = $filename[0];
                $file_type = $filename[1];
                $ticketname = $filename[2];
                $ticketnamefinal = explode(".", $ticketname);

                if ($file_type == 'RETAIL') {

                    $shopcode = substr($shopcode, 0, -3);
                }

                if ($file_type == $unsold_type && $ticketnamefinal[0] == $selected_ticketname) {
                    if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_unsoldretail/$curdate/$filenames";
                    } else {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_unsoldretail/$curdate/$filenames";
                    }

                    $unsold_content = file_get_contents($file_path);
                    // echo '<pre>'; print_r($unsold_content); exit;

                    $unsold_arrs = array();
                    $unsold_detail = array();
                    $unsoldsarr = array();
                    $unsold_arrs = explode("#", $unsold_content);
                    $i = 0;
                    $j = 0;

                    foreach ($unsold_arrs as $key => $val) {
                        if (!empty($val)) {
                            //echo $val;
                            $unsold_detail = explode('$', $val);

                            if ($i == 1) {
                                $recievedshopcode = $unsold_detail[2];
                                if ($file_type == 'RETAIL') { // for retail value 200 will be got appended with shopcode for riyas software
                                    $shopcode = substr($recievedshopcode, 0, -3);
                                }

                                $unsoldarray['shop_code'] = $shopcode;
                            }
                            if ($i == 3) {
                                $unsoldarray['date'] = $unsold_detail[2];
                            }
                            if ($i == 4) {
                                $unsoldarray['ticket'] = $unsold_detail[2];
                            }
                            if ($i == 5) {
                                $unsoldarray['draw_code'] = $unsold_detail[2];
                            }
                            if ($i == 6) {
                                $unsoldarray['grant_total'] = $unsold_detail[2];
                            }

                            if ($i > 7) {
                                $unsoldarray['details'][$j]['is_set'] = ($unsold_detail[1] == 'SET') ? 'YES' : 'NO';
                                $unsoldarray['details'][$j]['ticket_code'] = $unsold_detail[2];
                                $unsoldarray['details'][$j]['ticket_from'] = $unsold_detail[3];
                                $unsoldarray['details'][$j]['ticket_to'] = $unsold_detail[4];
                                $unsoldarray['details'][$j]['ticket_count'] = $unsold_detail[5];
                                $unsoldarray['details'][$j]['ticket_quantity'] = $unsold_detail[6];
                                $unsoldarray['details'][$j]['ticket_total'] = $unsold_detail[7];
                                $j++;
                            }
                        }

                        $i++;
                    }

                    //update todays ticket date and drawcode
                    //$this->Lottery_model->updateTodaysTicket($ticketnamefinal[0], $unsoldarray['draw_code']);
                    $this->Lottery_model->updateTodaysTicketwithSync($ticketnamefinal[0], $unsoldarray['draw_code'], $unsoldarray['date']);

                    $time = explode(" ", $unsoldarray['date']);
                    $unsolddatetime = $sold_date . " " . $time[1];

                    $useridarray = $this->Lottery_model->getShopCodeUser($unsoldarray['shop_code']);
                    $ticketarray = $this->Lottery_model->getTicketId($ticketnamefinal[0]);

                    foreach ($unsoldarray['details'] as $details) {
                        $unsold_insert_array = array('user_id' => $useridarray[0]['userId'],
                            'is_set' => $details['is_set'],
                            'ticket_code' => $details['ticket_code'],
                            'ticket_from' => $details['ticket_from'],
                            'ticket_to' => $details['ticket_to'],
                            'total' => $details['ticket_total'],
                            'tickets_id' => $ticketarray[0]['ticketId'],
                            'ticket_count' => $details['ticket_count'],
                            'ticket_quantity' => $details['ticket_quantity'],
                            'is_draft' => 0,
                            'type' => $unsold_type,
                            'sold_date' => $unsolddatetime,
                        );
                        $this->Lottery_model->sync_unsold_data($unsold_insert_array);
                    }
                    $consolidated_array = array('tickets_id' => $ticketarray[0]['ticketId'],
                        'sold_total' => 0,
                        'count_total' => $unsoldarray['grant_total'],
                        'pwt_dc' => 0,
                        'winning' => 0,
                        'profit_loss' => 0,
                        'sold_date' => $unsolddatetime,
                        'ticket_name' => $unsoldarray['ticket'],
                        'draw_code' => $unsoldarray['draw_code'],
                        'type' => $unsold_type,
                            //'day' => $dayname
                    );

                    $this->Lottery_model->sync_consolidated_sold_data($consolidated_array);
                    $p++;
                }
                unset($unsoldarray['details']);
            }
            // exit;
            //if data is nil or no data to sync  p==0 otherwise p==1 and loop will
            // echo '<pre>'; print_r($p); exit;
            if ($p == 0) {
                $data_status = '&&data=nil';
                $message = 'No Data to Sync';
                $this->response([
                    'status' => FALSE,
                    'message' => $message,
                    'data' => $data,
                        ], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $data_status = '';
            }

            $message = $this->unsoldlist($data['date_of_unsold'], $data['unsold_type'], $data['selectedticket']);
        } else {
            $message = 'process is unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function unsoldlist($date, $type, $ticket) {
        $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
        $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
//        if ($data['date_of_unsold'] == date('Y-m-d')) {
//            $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//        } else {
//            $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }

        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];

        // echo '<pre>'; print_r($data["ticketlist"]); exit;
        //echo '<pre>'; print_r($data); exit;

        $data['unsolddata'] = $this->Lottery_model->getUnsoldConsolidatedForTheDay($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['consolidatedunsolddata'] = $this->Lottery_model->consolidatedDailyData($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['sold_data_val'] = '';
        $data['pwt_dc_val'] = '';
        $data['sold_winning'] = '';
        $data['sold_profit_loss'] = '';
        if (!empty($data['consolidatedunsolddata'])) {
            $data['sold_data_val'] = $data['consolidatedunsolddata'][0]['sold_total'];
            $data['sold_winning'] = $data['consolidatedunsolddata'][0]['winning'];
            $data['sold_profit_loss'] = $data['consolidatedunsolddata'][0]['profit_loss'];
            $data['pwt_dc_val'] = $data['consolidatedunsolddata'][0]['pwt_dc'];
        }
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        //echo '<pre>'; print_r($data); exit;
        $message = [
            'status' => TRUE,
            'message' => 'Unsold fetched successfully',
            'unsolddata' => $data
        ];

        return $message;
    }

    public function unsoldsummarylist_post() {
        if (($this->post('date')) || ($this->post('type')) || ($this->post('selectedticket')) || ($this->post('user_id'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }

            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];

            // echo '<pre>'; print_r($data["ticketlist"]); exit;
            //echo '<pre>'; print_r($data); exit;

            $data['unsolddata'] = $this->Lottery_model->getUnsoldConsolidatedForTheDay($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
            $data['consolidatedunsolddata'] = $this->Lottery_model->consolidatedDailyData($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
            $data['sold_data_val'] = '';
            $data['pwt_dc_val'] = '';
            $data['sold_winning'] = '';
            $data['sold_profit_loss'] = '';
            if (!empty($data['consolidatedunsolddata'])) {
                $data['sold_data_val'] = $data['consolidatedunsolddata'][0]['sold_total'];
                $data['sold_winning'] = $data['consolidatedunsolddata'][0]['winning'];
                $data['sold_profit_loss'] = $data['consolidatedunsolddata'][0]['profit_loss'];
                $data['pwt_dc_val'] = $data['consolidatedunsolddata'][0]['pwt_dc'];
            }
            $i = 0;
            $total = 0;
            foreach ($data['unsolddata'] as $key => $val) {
                $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
                $total += $val['total'];
                $i++;
            }
            $data['total'] = $total;
            //echo '<pre>'; print_r($data); exit;
            if (empty($data['unsolddata'])) {
                $message = 'No Data to Display';
                // Set the response and exit
                $this->response([
                    'message' => $message,
                    'status' => FALSE,
                    'data' => $data,
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
            } else {
                $message = 'Unsold details Listed Successfully';
                // Set the response and exit
                $this->response([
                    'message' => $message,
                    'status' => TRUE,
                    'data' => $data,
                        ], REST_Controller::HTTP_CREATED); // NOT_FOUND (404) being the HTTP response code 
            }
        } else {
            $message = 'Invalid data';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function viewunsolddetails_post() {
        if (($this->post('date')) || ($this->post('type')) || ($this->post('selectedticket')) || ($this->post('user_id'))) {
            //if ($this->post('user_id')){       
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            // print_r($data); exit;
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            $data['user_id'] = (($this->post('user_id')) ? ($this->post('user_id')) : '0');
            $data['ticket_details'] = $this->Lottery_model->getTicketDetails($data['selectedticket']);
//            echo '<pre>'; print_r($data['ticket_details']); exit;
            $data['unsolddata'] = $this->Lottery_model->getUnsoldByUserForTheDay($data['date_of_unsold'], $data['user_id'], $data['selectedticket'], $data['unsold_type']);

            //echo '<pre>'; print_r($data); exit;
            $i = 0;
            $total = 0;
            foreach ($data['unsolddata'] as $key => $val) {
                $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
                $total += $val['total'];
                $i++;
            }
            $data['ticket_details']['name'] = $data['unsolddata'][0]['name'];
            //$datas = array($data['ticket_details'],$data['unsolddata'][0]['name']);
            //  echo '<pre>'; print_r($data['unsolddata'] ); exit;
            $data['total'] = $total;
            if (empty($data['unsolddata'])) {
                $message = 'Please Sync Unsold details';
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => $message,
                    'data' => $data,
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
            } else {
                $message = 'Unsold details Listed Successfully';
                // Set the response and exit
                $this->response([
                    'message' => $message,
                    'status' => TRUE,
                    'data' => $data,
                        ], REST_Controller::HTTP_CREATED); // NOT_FOUND (404) being the HTTP response code 
            }
        } else {
            $message = 'Invalid data';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function clearunsolddata_post() {
        if (($this->post('date')) || ($this->post('type')) || ($this->post('selectedticket')) || ($this->post('user_id'))) {
            //if (($this->post('date'))&&($this->post('type'))&&($this->post('selectedticket'))){
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
//            }
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }

            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];

            $this->Lottery_model->clearunsolddata($data['selectedticket'], $data['unsold_type'], $data['date_of_unsold']);
            $this->Lottery_model->clearunsoldconsolidated($data['selectedticket'], $data['unsold_type'], $data['date_of_unsold']);
            $message = [
                'status' => TRUE,
                'message' => 'Unsold Cleared successfully',
                'unsolddata' => $data
            ];
        } else {
            $message = 'Process unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function saveunsoldwinning_post() {
        if (($this->post('date')) && ($this->post('type')) && ($this->post('selectedticket'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            // $this->Lottery_model->clearwinningddata($data['selectedticket'], $data['unsold_type']);
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            $todays_winning = $this->Lottery_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);

            // echo '<pre>'; print_r($todays_winning); exit;

            if (empty($todays_winning)) {
                // echo '<pre>'; exit;
                $message = array();
                $message = $this->savewinning($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
                $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
            } else {

                // if ($todays_winning[0]['winning_details'] == 'NODATA') {
                $message = 'No Winning from Synced Unsold Tickets';
                //} else if ($todays_winning[0]['winning_details'] == 'Result Not Published') {
                //    $message = 'Result Not Published';
                //}
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => $message,
                        ], REST_Controller::HTTP_CREATED); // NOT_FOUND (404) being the HTTP response code 
            }
        } else {
            $message = 'Not Supplied with suffiecient parameters';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function savewinning($ticketId, $date, $unsold_type) {
//        $year = (int) date("Y");
//        $month = (int) date("m");
//        $month = sprintf("%02d", $month);
//        $day = (int) date("d");
//        $day = sprintf("%02d", $day);
//        $dayname = strtoupper(date("D") . 'day');
//        $curdate = "$year/$month/$day";
//        $sold_date = date("Y-m-d");
//        $winningfiledate = $year . $month . $day;
        //$winningfiledate = "20190531";
        $data['date_of_unsold'] = $date;
        $dateofunsold = explode('-', $data['date_of_unsold']);
        $sold_date = $dateofunsold[0] . '-' . $dateofunsold[1] . '-' . $dateofunsold[2];
        //$curdate = $dateofunsold[0] . '/' . $dateofunsold[1] . '/' . $dateofunsold[2];
        $curdate = $dateofunsold[0].$dateofunsold[1].$dateofunsold[2];
        $dcdate = $dateofunsold[2] . '-' . $dateofunsold[1] . '-' . $dateofunsold[0];
        $winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];
        $ticketdetails = $this->Lottery_model->getTicketDetails($ticketId);
        $data['ticket_name'] = $ticketdetails['ticket_name'];
        $finalfilename = $winningfiledate . '_' . $data['ticket_name'] . '.txt';
        $host = $_SERVER['HTTP_HOST'];
        $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
//        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
//            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$curdate/$finalfilename";
//        } else {
//            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_winning/$curdate/$finalfilename";
//        }

        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$curdate/$finalfilename";
        } else {
            $file_path = winning_file_url .$data['ticket_name']."/".$curdate."_".$data['ticket_name'].'.txt';
        }        
        //$file_path = winning_file_url .$data['ticket_name']."/".$curdate."_".$data['ticket_name'].'.txt';
//echo $file_path; exit;
//        $resultfilepath = $this->getResultFile($ticketId);
//        $file_path = $resultfilepath['file_path'];
        $winning_content = file_get_contents($file_path);
        $winning_arrs = array();
        $winnig_detail = array();
        $wonshopsarr = array();
        $winning_arrs = explode($winningfiledate, $winning_content);
        //echo '<pre>'; print_r($winning_arrs); exit;
        foreach ($winning_arrs as $key => $val) {
            if (!empty($val)) {
                $winnig_detail = explode('$', $val);
                $ticket_name = strtoupper($winnig_detail[1]);
                $price_type = $winnig_detail[2];
                $price_amount = $winnig_detail[3];
                $won_serial = $winnig_detail[6];

                //exit;
                //echo $price_type;
                if (strlen($won_serial) == 9) {
                    $digit = "fulldigits";
                }
                if (strlen($won_serial) == 4) {
                    $digit = "lastfourdigits";
                }
                //exit;
                //echo $price_type;
                $parameters = array('ticket_name' => $ticket_name, 'price_type' => $price_type,
                    'serial_number' => $won_serial, 'price_amount' => $price_amount, 'type' => $unsold_type, 'date' => $sold_date);
                //echo '<pre>'; print_r($parameters ); exit;
                //if ($price_type == "1st Prize" || $price_type == "Consolation Prize" || $price_type == "2nd Prize" || $price_type == "3rd Prize") {
                if (!empty($this->Lottery_model->checkPrizeIsWon($parameters, $digit))) {
                    $wonshopsarr[] = $this->Lottery_model->checkPrizeIsWon($parameters, $digit);
                }
                // echo '<pre>'; print_r($wonshopsarr[]); exit;
                //}
//                if ($price_type == "4th Prize" || $price_type == "5th Prize" ||
//                        $price_type == "6th Prize" || $price_type == "7th Prize" ||
//                        $price_type == "8th Prize" || $price_type == "9th Prize") {
//
//                    if (!empty($this->Lottery_model->checkPrizeIsWon($parameters, "lastfourdigits"))) {
//                        $wonshopsarr[] = $this->Lottery_model->checkPrizeIsWon($parameters, "lastfourdigits");
//                        }
//                    }
            }
        }

        foreach ($wonshopsarr as $wonshopsarrs) {
            foreach ($wonshopsarrs as $key => $val) {
                $data['details'][] = $val;
            }
        }

        if (empty($data['details'])) {
            $data = 'NODATA';
        } else {
            $data = serialize($data);
        }
//        if ($resultfilepath['result_published'] == "Result Not Published") {
//            $data = "Result Not Published";
//        }
        $datas = array('winning_details' => $data,
            'type' => $unsold_type,
            'ticket_id' => $ticketId,
            'date' => $sold_date);
        //  echo '<pre>'; print_r($datas); exit;
        $this->Lottery_model->savewonshopdetails($datas);
        $datasaved = array('winning_saved' => unserialize($data),
            'type' => $unsold_type,
            'ticket_id' => $ticketId,
            'date' => $sold_date);
        $message = [
            'status' => TRUE,
            'message' => 'winning saved successfully',
            'winningdata' => $datasaved,
        ];

        return $message;
    }

    public function listunsoldwinning_post() {
        if (($this->post('date')) && ($this->post('type')) && ($this->post('selectedticket'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
            $todays_winning = array();
            $winningdata = array();

//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'WINNING');
//            }
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            //   echo '<pre>'; print_r($data); exit;

            $checkunsold = $this->Lottery_model->checkUsold($data['date_of_unsold']);

            if ($checkunsold < 1) {

                $this->response([
                    'status' => FALSE,
                    'message' => 'Unsold is not synced by Admin',
                    'unsoldwinning' => $data
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
            $todays_winning = $this->Lottery_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
            //echo '<pre>'; print_r($todays_winning); exit;
            if (!empty($todays_winning)) {
                if ($todays_winning[0]['winning_details'] != "NODATA") {
                    $winningdata = unserialize($todays_winning[0]['winning_details']);
                } else {
                    $data['Winningstatus'] = $todays_winning[0]['winning_details'];
                    $message = 'No Winning from Synced Unsold Tickets';
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => $message,
                        'unsoldwinning' => $data
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
                }
            }
            // echo '<pre>'; print_r($winningdata); exit;
            if (!empty($winningdata)) {
                $data = array_merge($winningdata, $data);
            } else {
                $data['details'] = $this->Lottery_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
                //echo '<pre>'; print_r($todays_winning); exit;
            }

            $message = 'Winning Displayed successfully';
            $this->response([
                'status' => TRUE,
                'message' => 'Winning Displayed successfully',
                'unsoldwinning' => $data
                    ], REST_Controller::HTTP_CREATED);
        } else {
            $message = 'Not Supplied with sufficcient parameters';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code  
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function clearunsoldwinning_post() {

        if (($this->post('date')) && ($this->post('type')) && ($this->post('selectedticket'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['unsold_type'] = ($this->post('type')) ? $this->post('type') : 'RETAIL';
            // echo '<pre>'; print_r(  $data['unsold_type'] ); exit;
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'WINNING');
//            }
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }

            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            $this->Lottery_model->clearwinningddata($data['selectedticket'], $data['unsold_type'], $data['date_of_unsold']);

            $data['details'] = $this->Lottery_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
            //echo '<pre>'; print_r($todays_winning); exit;
            $message = [
                'status' => TRUE,
                'message' => 'Winning Cleared successfully',
                'unsoldwinning' => $data
            ];
        } else {
            $message = 'Process unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    public function listusertype_get() {
        $usertype = $this->Lottery_model->listUsertype();
        //echo '<pre>';print_r( $usertype );exit;
        $message = [
            'status' => TRUE,
            'message' => 'successfully fetched usertypes',
            'usertype' => $usertype
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function listdcshopcode_post() {
        if (($this->post('date')) && ($this->post('selectedticket')) && ($this->post('id')) && ($this->post('user_type'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['shopcode'] = ($this->post('shopcode')) ? $this->post('shopcode') : '';
            $data['id'] = $this->post('id');
            $data['user_type'] = $this->post('user_type');
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'DC');
//            }             
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            // $data["details"] = $this->Lottery_model->getshops();

            if ($data['user_type'] == 'SHOP' || $data['user_type'] == 'AGENT' || $data['user_type'] == 'OTHERS') {
                $shopid = $data['id'];
                // $data["details"] = $this->Lottery_model->getByShopid($data['id'],$data['shopcode']);
            } else {

                $shopid = '';
                //shope code search
                //  $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);
                // $datas["details"] = $this->Lottery_model->getByShopCode($data['date_of_unsold'], $data['selectedticket'], $data['shopcode']);
            }

            $datas["details"] = $this->Lottery_model->getByShopCode($data['date_of_unsold'], $data['selectedticket'], $data['shopcode'], $shopid);

            //echo '<pre>'; print_r($datas["details"]); exit;
            foreach ($datas["details"] as $key => $val) {
                $shopcode = $datas["details"][$key]['shop_code'];
                $shopname = $datas["details"][$key]['shopname'];
                $count = $datas["details"][$key]['count'];
                $totaldc = $this->Ticket_model->get_unsold_from_dc($shopcode, $data['date_of_unsold']);
//              $data['shops_daily_dc'] = $this->Lottery_model->getShopswithDCdataCount($shopcode, $data['date_of_unsold'], $data['selectedticket']);
//              $count = $data['shops_daily_dc'][0]['count'];

                $data["details"][] = array(
                    'shop_code' => $shopcode,
                    'shopname' => $shopname,
                    'count' => $count,
                    'totaldc' => $totaldc
                );
            }


            $data['display_date'] = date("d-m-Y", strtotime($data['date_of_unsold']));

            $message = [
                'status' => TRUE,
                'message' => 'shop code fetched successfully',
                'dc_details' => $data
            ];
        } else {
            $message = 'NO TICKET SET-CONTACT ADMIN';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function syncdcdetailssave_post() {

        if (($this->post('date')) && ($this->post('selectedticket')) && ($this->post('shopcode'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['shopcode'] = ($this->post('shopcode')) ? $this->post('shopcode') : '';

            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];

//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'DC');
//            }
            //$data["ticketlist"] = $this->Ticket_model->ticket_list();            
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $selected_ticketname = $data["ticketlist"][0]['ticket_name'];

            //To fetch the file 
            //$data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);
//            $year = (int) date("Y");
//            $month = (int) date("m");
//            $month = sprintf("%02d", $month);
//            $day = (int) date("d");
//            $day = sprintf("%02d", $day);
//            $dayname = strtoupper(date("D") . 'day');
//            $curdate = "$year/$month/$day";
//            $dcdate = "$day-$month-$year";
//            $sold_date = date("Y-m-d");
//
//            $winningfiledate = $year . $month . $day;
            //$winningfiledate = "20190531";

            $dateofunsold = explode('-', $data['date_of_unsold']);
            $sold_date = $dateofunsold[0] . '-' . $dateofunsold[1] . '-' . $dateofunsold[2];
            $curdate = $dateofunsold[0] . '/' . $dateofunsold[1] . '/' . $dateofunsold[2];
            $dcdate = $dateofunsold[2] . '-' . $dateofunsold[1] . '-' . $dateofunsold[0];
            //$winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];  

            $ticketdetails = $this->Lottery_model->getTicketDetails($data['selectedticket']);
            $data['ticket_name'] = $ticketdetails['ticket_name'];
            $finalfilename = $data['shopcode'] . '_' . 'DC' . '_' . $data['ticket_name'] . '_' . $dcdate . '.txt';
            $selectedshopcode = $data['shopcode'];
            $host = $_SERVER['HTTP_HOST'];

            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_dc/$curdate/$selectedshopcode/$finalfilename";
            } else {
                $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_dc/$curdate/$selectedshopcode/$finalfilename";
            }
            $dc_content = file_get_contents($file_path);
            $dc_arrs = array();
            $dc_detail = array();
            if (empty($dc_content)) {

                $message = [
                    'status' => FALSE,
                    'message' => 'No data to sync'
                ];
            } else {

                $dc_arrs = array();
                $dc_detail = array();
                $dc_arrs = explode("=", $dc_content);
                $j = 0;
//              echo '<pre>';
//              print_r($dc_arrs);exit;
                foreach ($dc_arrs as $key => $val) {
                    if (!empty($val)) {
                        //echo $val;
                        $dc_detail = explode('$', $val);
                        //echo '<pre>'; print_r($dc_detail); exit;
                        $dcarray['details'][$j]['price_type'] = $dc_detail[1];
                        $dcarray['details'][$j]['ticket_id'] = $data['selectedticket'];
                        $dcarray['details'][$j]['agent_name'] = $dc_detail[2];
                        $dcarray['details'][$j]['shop_code'] = $data['shopcode'];
                        $dcarray['details'][$j]['serial_code'] = $dc_detail[3];
                        $dcarray['details'][$j]['ticket_number'] = $dc_detail[4];
                        $dcarray['details'][$j]['bill_no'] = $dc_detail[5];
                        $dcarray['details'][$j]['total_dc'] = $dc_detail[6];
                        $dcarray['details'][$j]['date'] = $data['date_of_unsold'];
                        $j++;
                    }
                }

                foreach ($dcarray['details'] as $details) {

                    $dc_insert_array = array(
                        'price_type' => $details['price_type'],
                        'ticket_id' => $details['ticket_id'],
                        'agent_name' => $details['agent_name'],
                        'shop_code' => $details['shop_code'],
                        'serial_code' => $details['serial_code'],
                        'ticket_number' => $details['ticket_number'],
                        'bill_no' => $details['bill_no'],
                        'total_dc' => $details['total_dc'],
                        'payment' => "UNPAID",
                        'date' => $sold_date
                    );

                    $this->Lottery_model->save_dc_data($dc_insert_array);
                }
                $message = [
                    'status' => TRUE,
                    'message' => 'DC Synced Successfully',
                    'dc_details' => $dc_insert_array
                ];
            }
        } else {
            $message = 'Invalid date or ticket';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function agentsdclist_post() {

        if (($this->post('date')) && ($this->post('selectedticket')) && ($this->post('shopcode')) || ($this->post('shopname'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['shopcode'] = ($this->post('shopcode')) ? $this->post('shopcode') : '';
            $data['shopname'] = ($this->post('shopname')) ? $this->post('shopname') : '';
            //  echo '<pre>'; print_r($data['shopname']); exit;
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
            $billno = ($this->post('billno')) ? $this->post('billno') : '';
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'DC');
//            }
            //$data["ticketlist"] = $this->Ticket_model->ticket_list();         
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            //To get todays ticket name
            $selected_ticketname = $data["ticketlist"][0]['ticket_name'];

            //To fetch the file 
            $data["details"] = $this->Lottery_model->getDcByShopCode($data['shopcode'], $data['shopname']);
            $data['display_date'] = date('d-m-Y', strtotime($data['date_of_unsold']));
            $params = array(
                'date_of_dc' => $data['date_of_unsold'],
                'selectedticket' => $data['selectedticket'],
                'selectedshopcode' => $data['shopcode'],
                'search' => $billno
            );
            //echo '<pre>'; print_r($params); exit;
            //To get todays ticket name
            $data['todays_dc_details'] = $this->Lottery_model->get_dc_list_by_bill($params);
            $data['billno'] = $billno;
            $message = [
                'status' => TRUE,
                'message' => 'Agents dc list displayed Successfully',
                'dc_details' => $data
            ];
        } else {
            $message = 'No Data to display';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function todaysticket_get() {
        if ($this->get('date') && $this->get('module')) {
            $data['todays_date'] = ($this->get('date')) ? $this->get('date') : date('Y-m-d');

//            if ($data['todays_date'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['todays_date']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['todays_date'], $this->get('module'));
//            }
            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            $message = [
                'status' => TRUE,
                'message' => 'Ticket list displayed Successfully',
                'Ticket_details' => $data
            ];
        } else {
            $message = 'No Data to display';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREAT     
    }

    public function agentsdcdetail_get() {
        //http://kairaliagencies.com/admin/qrcodewondcprice?shopid=714&&billid=2345
        //  echo '<pre>'; exit;
        if (($this->get('lc')) && $this->get('lc') != '') {
            $curdate = $this->get('lc');
            $purchasecode = $this->get('b');
            $host = $_SERVER['HTTP_HOST'];
            $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://';
            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/$curdate/");
            } else {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$curdate/");
            }
            //  echo '<pre>'; print_r($files); exit;
            foreach ($files as $key => $filenames) {
                $filename = explode("_", $filenames);
//                $shopcode = $filename[0];
//                $file_type = $filename[1];
                $purchasefilecode = $filename[2];
                // $ticketnamefinal = explode(".", $ticketname);

                if ($purchasecode == $purchasefilecode) {
                    $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
                    if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/$curdate/$filenames";
                        $data['url'] = $protocol . $host . "/$subfolder/$curdate/$filenames";
                    } else {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$curdate/$filenames";
                        $data['url'] = $protocol . $host . "/$curdate/$filenames";
                    }
                    $this->load->view('dashboard/purchasefileview', $data);
                }
            }
        } else if ($this->get('date')) {
            $data['date'] = ($this->get('date')) ? $this->get('date') : date('Y-m-d');
            $params = array('shopid' => $this->get('s'),
                'billno' => $this->get('b'),
                'date' => $this->get('date'));

            //echo '<pre>'; print_r($params); exit;
            $data['details'] = $this->Lottery_model->getDcDetails($params);
            //echo '<pre>'; print_r($data['details']); exit;
            $dctotal = $this->Lottery_model->getTotalDc($params);
            $data['totaldc'] = $dctotal[0]['totaldc'];
            //  echo '<pre>'; print_r($data['totaldc']); exit;
            $data['user'] = ($this->get('user')) ? $this->get('user') : "agent";
            $data['ticket'] = ($this->get('ticket')) ? $this->get('ticket') : '';
            $data['billno'] = ($this->get('b')) ? $this->get('b') : '';
            $data['shopcode'] = $this->get('s');
            $data['paymentstatus'] = $data['details'][0]['payment'];

            $message = [
                'status' => TRUE,
                'message' => 'Agents dc details displayed Successfully',
                'dc_details' => $data
            ];
            if (empty($data['details'])) {
                $message = 'please sync dc details';
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => $message,
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 

                $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
            }
        } else {
            $message = 'No data to disaplay';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function cleardcdetails_post() {

        if (($this->post('date')) && ($this->post('selectedticket')) && ($this->post('shopcode'))) {
            $data['date_of_unsold'] = ($this->post('date')) ? $this->post('date') : date('Y-m-d');
            $data['shopcode'] = ($this->post('shopcode')) ? $this->post('shopcode') : '';

            $dayname = strtoupper(date("D"));
            $data["ticketlist"] = $this->Ticket_model->ticket_list();
            $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

            $data['selectedticket'] = ($this->post('selectedticket')) ? $this->post('selectedticket') : $data["todaysticket"][0]['id'];
//            if ($data['date_of_unsold'] == date('Y-m-d')) {
//                $data["ticketlist"] = $this->Lottery_model->get_todays_ticket_list($data['date_of_unsold']);
//            } else {
//                $data["ticketlist"] = $this->Lottery_model->get_ticket_list($data['date_of_unsold'], 'DC');
//            }
            //$data["ticketlist"] = $this->Ticket_model->ticket_list(); 
            $i = 0;
            foreach ($data["ticketlist"] as $tickets) {
                $ticketarr[] = $data["ticketlist"][$i]['id'];

                $i++;
            }
            if (!in_array($data['selectedticket'], $ticketarr)) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
            $selected_ticketname = $data["ticketlist"][0]['ticket_name'];
            //To fetch the file 
            //$data["details"] = $this->Lottery_model->getByShopCode($data['date_of_unsold'],$data['selectedticket']);
            //echo '<pre>'; print_r($data); exit;
            $this->Lottery_model->delete_dc_list($data['date_of_unsold'], $data['selectedticket'], $data['shopcode']);
            $message = [
                'status' => TRUE,
                'message' => 'DC cleared Successfully',
                'dc_details' => $data
            ];
        } else {
            $message = 'process is unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function livestatements_post() {
        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";

        if (($this->post('date'))) {
            $dates = explode("/", $this->post('date'));
            $getdate = $this->post('date');
            $date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
        } else {
            $date = $curdate;
        }
        // echo $date; exit;
        $host = $_SERVER['HTTP_HOST'];

        $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
            if ($this->post('type') == 'UNSOLD') {
                $url = "http://" . $host . "/$subfolder/data_unsoldretail/" . $date . "/" . $this->post('shopcode') . "_Unsold.html";
            } else if ($this->post('type') == 'REPORT') {
                $url = "http://" . $host . "/$subfolder/data_shopdetail/" . $date . "/" . $this->post('shopcode') . "/" . $this->post('shopcode') . "_" . ($this->post('type')) . "_ShopeDetails.html";
            } else if ($this->post('type') == 'LIVE') {
                $url = "http://" . $host . "/$subfolder/data_shopdetail/" . $date . "/" . $this->post('shopcode') . "/" . $this->post('shopcode') . "_ShopeDetails.html";
            }
        } else {
            if ($this->post('type') == 'UNSOLD') {
                $url = "http://" . $host . "/data_unsoldretail/" . $date . "/" . $this->post('shopcode') . "_Unsold.html";
            } else if ($this->post('type') == 'REPORT') {
                $url = "http://" . $host . "/data_shopdetail/" . $date . "/" . $this->post('shopcode') . "/" . $this->post('shopcode') . "_" . $this->post('type') . "_ShopeDetails.html";
            } else if ($this->post('type') == 'LIVE') {
                $url = "http://" . $host . "/data_shopdetail/" . $date . "/" . $this->post('shopcode') . "/" . $this->post('shopcode') . "_ShopeDetails.html";
            }
        }
        // echo  $url; exit;
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[7] == 'Content-Type: text/html; charset=UTF-8') {
            $message = 'No Data to display';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        } else {
            $data['url'] = $url;
            $message = [
                'status' => TRUE,
                'message' => 'live statement displayed Successfully',
                'url' => $data,
            ];
        }
        //echo $url;
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function shoplist_post() {
        if (($this->post('date'))) {
            $year = (int) date("Y");
            $month = (int) date("m");
            $month = sprintf("%02d", $month);
            $day = (int) date("d");
            $day = sprintf("%02d", $day);
            $dayname = strtoupper(date("D") . 'day');
            $curdate = "$day/$month/$year";
            $date = ($this->post('date')) ? date('d/m/Y', strtotime($this->post('date'))) : $curdate;
            //echo  $curdate;exit;
            $datas["details"] = $this->Lottery_model->getShops();

            $host = $_SERVER['HTTP_HOST'];

            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if (isset($_SERVER['HTTPS']) &&
                    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $protocol = 'https://';
            } else {
                $protocol = 'http://';
            }
            foreach ($datas["details"] as $key => $val) {
                $shopcode = $datas["details"][$key]['shop_code'];
                $shopname = $datas["details"][$key]['shopname'];
                $dctotal = $this->Ticket_model->get_unsold_from_dc($shopcode, $date);

                if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                    $unsold_url = $protocol . $host . "/$subfolder/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD&&platform=app";
                    $report_url = $protocol . $host . "/$subfolder/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=REPORT&&platform=app";
                    $live_url = $protocol . $host . "/$subfolder/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=LIVE&&platform=app";
                    $retail_unsold_url = $protocol . $host . "/$subfolder/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD_RETAIL&&platform=app";
                    $retail_report_url = $protocol . $host . "/$subfolder/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD_REPORT&&platform=app";
                } else {
                    $unsold_url = $protocol . $host . "/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD&&platform=app";
                    $report_url = $protocol . $host . "/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=REPORT&&platform=app";
                    $live_url = $protocol . $host . "/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=LIVE&&platform=app";
                    $retail_unsold_url = $protocol . $host . "/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD_RETAIL&&platform=app";
                    $retail_report_url = $protocol . $host . "/admin/dashboard/livestatementsapp?shopcode=$shopcode&&date=$date&&type=UNSOLD_REPORT&&platform=app";
                }

                if ($dctotal > 0) {
                    $unsold_url = $protocol . $host . "/admin/dc?s=$shopcode&&b=0&&date=$date&&type=UNSOLD&&platform=app";
                }



                $data["details"][] = array(
                    'shop_code' => $shopcode,
                    'shopname' => $shopname,
                    'count' => $count,
                    'dctotal' => $dctotal,
                    'unsold_url' => $unsold_url,
                    'report_url' => $report_url,
                    'live_url' => $live_url,
                    'retail_unsold_url' => $retail_unsold_url,
                    'retail_report_url' => $retail_report_url
                );
            }

            $message = [
                'status' => TRUE,
                'message' => 'shop displayed Successfully',
                'shoplist' => $data,
            ];
        } else {
            $message = 'No Data to display';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        //echo $url;
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function edituser_post() {
        if ($this->post('id')) {
            $userid = $this->post('id');
            $condition = array('userid' => $userid);
            $data['user'] = $this->Lottery_model->get_user($condition);
            $data['user'][0]['deleted_at'] = ($data['user'][0]['deleted_at'] == NULL) ? 'ACTIVE' : 'INACTIVE';
            //echo '<pre>'; print_r($data); exit;
            $message = [
                'status' => TRUE,
                'message' => 'Data displayed successfully',
                'data' => $data['user'],
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP
    }

    public function updateusers_post() {
        if ($this->post('id')) {
            $data = $this->input->post();
            $userid = $this->post('id');
            $condition = array('userid' => $userid);
            $data['user'] = $this->Lottery_model->get_user($condition);

            $data['user'][0]['username'] = $this->post('username');
            $data['user'][0]['password'] = $this->post('password');
            $data['user'][0]['user_type'] = $this->post('user_type');
            $data['user'][0]['firstname'] = $this->post('firstname');
            $data['user'][0]['lastname'] = $this->post('lastname');
            $data['user'][0]['lastname'] = $this->post('lastname');
            $data['user'][0]['deleted_at'] = ($this->post('deleted_at') == 'ACTIVE') ? NULL : date('Y-m-d H:i:s');
            // echo '<pre>'; print_r($data); exit;
            if ($userid == ($data['user'][0]['id'])) {
                if ($this->post('username') == ($data['user'][0]['username'])) {
                    $this->Lottery_model->update_user($condition, $data);
                    $message = [
                        'status' => TRUE,
                        'message' => 'User updated successfully',
                    ];
                } else {
//            $this->username_exists();
                    $username = $this->input->post('username');
                    $exists = $this->Lottery_model->username_exists($username);
                    $count = count($exists);
                    //echo $count;
                    if (empty($count)) {
                        $this->Lottery_model->update_user($condition, $data);
                        $message = [
                            'status' => TRUE,
                            'message' => 'User updated successfully',
                        ];
                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Username is Available',
                                ], REST_Controller::HTTP_NOT_FOUND);
                    }
                }
            }
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) be
    }

    public function viewusers_post() {
        $search = '';
        if ($this->post('search')) {
            $search = $this->post('search');
        }
        $data["users"] = $this->Lottery_model->get_users_list($search);
        //$data["superadmins"] = $this->Lottery_model->get_super_user_list();
        $message = [
            'status' => TRUE,
            'message' => 'user list displayed Successfully',
            'userlist' => $data
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function results_post() {

        if ($this->post('id') && $this->post('date')) {
            $params = "";
            $host = $_SERVER['HTTP_HOST'];

            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if (isset($_SERVER['HTTPS']) &&
                    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $protocol = 'https://';
            } else {
                $protocol = 'http://';
            }
            $params = "&&ticket=" . $this->post('id') . "&&date=" . $this->post('date');
//            if ($this->post('id')) {
//                $ticket = "&&ticket=" . $this->post('id');
//            } else {
//                $ticket = "";
//            }
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $url = $protocol . $host . "/$subfolder/admin/dashboard/results?platform=app$params";
            } else {
                $url = winning_reference . "/admin/dashboard/results?platform=app$params";
            }
            $message = [
                'status' => TRUE,
                'message' => 'Result displayed Successfully',
                'result_url' => $url
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function tickets_get() {

        $ticketsarr = $this->Ticket_model->ticket_list();
        $message = [
            'status' => TRUE,
            'message' => 'Ticket list displayed Successfully',
            'tickets' => $ticketsarr
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function ticketedit_post() {

        if ($this->post('id')) {

            $ticketsarr = $this->Ticket_model->getTicketDetails($this->post('id'));
            $message = [
                'status' => TRUE,
                'message' => 'Ticket details displayed Successfully',
                'ticketdetails' => $ticketsarr
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function ticketupdate_post() {

        if ($this->post('id') && $this->post('draw_code') && $this->post('set_count') && $this->post('date')) {

            $details = array(
                'draw_code' => $this->post('draw_code'),
                'set_count' => $this->post('set_count'),
                'cost' => $this->post('cost'),
                'date' => date('Y-m-d', strtotime($this->post('date')))
            );
            $this->Ticket_model->update($details, $this->post('id'));
            $message = [
                'status' => TRUE,
                'message' => 'Ticket Updated Successfully'
                    //'tickets' => $ticketsarr
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

    public function getResultFile($ticketId) {

        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";
        $sold_date = date("Y-m-d");
        $winningfiledate = $year . $month . $day;

        // $winningfiledate = "20190523";
        $data['result_published'] = '';
        $ticketlists = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
        $data['ticketlist'] = $ticketlists;
        $_GET['ticket'] = $ticketId;
        if (isset($_GET['ticket'])) {

            $ticketdetails = $this->Ticket_model->getTicketDetails($_GET['ticket']);
            $drawcode = explode('-', $ticketdetails['draw_code']);
            $selectedticket = trim($winningfiledate . '_' . $ticketdetails['ticket_name'] . $drawcode[1]);
            $data['selectedticket'] = $_GET['ticket'];
        }
        $ticket1 = '';
        $ticket2 = '';

        $ticketdetails = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
        $data['ticketdetails'] = $ticketdetails;


        $file_path_main = "http://riyaskk.in/Lottery/PWT/dwnldRes.php";
        $ticket_main = file_get_contents($file_path_main);
        $ticket_main_arr = explode($winningfiledate, $ticket_main);

        if ($ticket_main_arr[1] == '') {
            $data['result_published'] = 'Result Not Published';
        }
        // echo '<pre>'; print_r($ticket_main_arr[1]); exit;
        if (isset($ticket_main_arr[1])) {
            $ticket1 = explode('[', $ticket_main_arr[1]);
            $ticket1 = $winningfiledate . $ticket1[0];
            $ticket1 = trim($ticket1);
        }


        if (isset($ticket_main_arr[2])) {
            $ticket2 = explode('[', $ticket_main_arr[2]);
            $ticket2 = explode(')', $ticket2[0]);
            $ticket2 = $winningfiledate . $ticket2[0];
            $ticket2 = trim($ticket2);
        }
        // echo $selectedticket;
        if ($ticket1 == $ticket2 && $selectedticket == '') {
            $finalticketresultname = $ticket1;
        } elseif ($selectedticket == $ticket1 && $selectedticket != '') {
            $finalticketresultname = $ticket1;
        } elseif ($selectedticket == $ticket2 && $selectedticket != '') {
            $finalticketresultname = $ticket2;
        } else {
            $finalticketresultname = $ticket1;
        }
//echo $finalticketresultname; exit;
        $finalticketresultname = str_replace(" ", '%20', $finalticketresultname);
        if (strpos($finalticketresultname, ')') !== false) {

            $finalticketresult = explode(')', $finalticketresultname);
            $finalticketresultname = trim($finalticketresult[0]);
            // echo 'true';
        }
        $ticketdate = explode('_', $finalticketresultname);
        $file_path = "http://riyaskk.in/Lottery/PWT/$finalticketresultname";
        $data['file_path'] = $file_path;

        return $data;
    }

    public function barcodewinning_get() {
    if (($this->get('ticketnumber')&&$this->get('tickettype'))) {
            $scannedticket = ($this->get('ticketnumber')) ? $this->get('ticketnumber') : 0;
            $tickettype = ($this->get('tickettype')) ? $this->get('tickettype') : 'WEEKLY';
            $identifyticket = substr($scannedticket, 0, 1);
            if ($identifyticket == 'A') {
                $ticketname = 'AKSHAYA';
            } else if ($identifyticket == 'K') {
                $ticketname = 'KARUNYA';
            } else if ($identifyticket == 'P') {
                $ticketname = 'KARUNYA PLUS';
            } else if ($identifyticket == 'N') {
                $ticketname = 'NIRMAL';
            } else if ($identifyticket == 'R') {
                $ticketname = 'POURNAMI';
            } else if ($identifyticket == 'S') {
                $ticketname = 'STHREE SAKTHI';
            } else if ($identifyticket == 'W') {
                $ticketname = 'WIN-WIN';
            } else {
                $ticketname = 'BUMPER';
            }
            
            if($tickettype == 'BUMPER'){
                $ticketname = 'BUMPER';
            }

            $host = $_SERVER['HTTP_HOST'];
            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$ticketname/");
            } else {
                //$files = directory_map("/home/v7y2d48kqdn6/public_html/starluckycentre.com/data_winning/$ticketname/");
                $files = directory_map(winning_file_url .$ticketname."/");
                
            }
                        
            $files = array_reverse($files);
            $inc = 1;
            $wonshopsarr = array();
            $winning_drawcode = array();
            $winning_drawcode_arr = array();
            foreach ($files as $key => $filenames) {

                if ($inc < 6) {
                    
                    $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
                    if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$ticketname/$filenames";
                    } else {
                        //$file_path = "/home/v7y2d48kqdn6/public_html/starluckycentre.com/data_winning/$ticketname/$filenames";
                        $file_path = winning_file_url.$ticketname."/".$filenames;
                    }

                    $winning_content = file_get_contents($file_path);
//                    echo '<pre>'; print_r($winning_content);
//                    exit;
                    $winning_arrs = array();
                    $winnig_detail = array();
                    $won_serial_code_arr = array();
                    
                    $winning_arr = array();
                    $winning_arr = explode('$', $winning_content);
                    $winning_arrs = explode($winning_arr[0], $winning_content);
                    
                    //$winning_drawcode  = explode('$', $winning_arrs[1]);
                    //$winning_drawcode_arr[] = $winning_drawcode[5].'-'.$winning_drawcode[4];
                    foreach ($winning_arrs as $key => $val) {

                        if (!empty($val)) {
                            //echo $val;
                            $winnig_detail = explode('$', $val);

                            $ticket_name = strtoupper($winnig_detail[1]);
                            $price_type = $winnig_detail[2];
                            $price_amount = $winnig_detail[3];
                            $draw_code = $winnig_detail[5].'-'.$winnig_detail[4];
                            $won_serial = $winnig_detail[6];
                            
                                if($price_type == '1st Prize' || $price_type == 'Consolation Prize'){

                                    $won_serial = explode(' ',$won_serial);
                                    $won_serial_code_arr[] = $won_serial[0];

                                }  
                    
                            }
                    
                   }                  
                    //echo '<pre>'; print_r($won_serial_code_arr); exit;
                    
                    
                   //echo '<pre>'; print_r($winning_arrs[1]); exit;
                   $winning_no_prize_arr = explode('$', $winning_arrs[1]);
                   
                   foreach ($winning_arrs as $key => $val) {

                        if (!empty($val)) {
                            //echo $val;
                            $winnig_detail = explode('$', $val);

                            $ticket_name = strtoupper($winnig_detail[1]);
                            $price_type = $winnig_detail[2];
                            $price_amount = $winnig_detail[3];
                            $draw_code = $winnig_detail[5].'-'.$winnig_detail[4];
                            $won_serial = $winnig_detail[6];

                            if (strlen($won_serial) == 9) {
                                $digit = "fulldigits";
                            }
                            if (strlen($won_serial) == 4) {
                                $digit = "lastfourdigits";
                            }

                            $scannedticketcode = explode('-',$scannedticket);
                            if (in_array($scannedticketcode[0],$won_serial_code_arr)) {
                                $parameters = array('ticket_name' => $ticket_name, 'price_type' => $price_type,
                                    'serial_number' => $won_serial, 'price_amount' => $price_amount, 'date' => $winning_arr[0], 'scannedticket' => $scannedticket, 'drawcode' => $draw_code);
                                if (!empty($this->Lottery_model->checkbarcodewinning($parameters, $digit))) {
                                    $wonshopsarr[] = $this->Lottery_model->checkbarcodewinning($parameters, $digit);
                                    $winning_drawcode_arr[] = $draw_code;
                                }
                            }
                        }
                    }
                    

                }
                $inc++;
               // if(empty($wonshopsarr)){
                if(!in_array($winning_no_prize_arr[5].'-'.$winning_no_prize_arr[4], $winning_drawcode_arr)){
                    $wonshopsarr[0][] = array(
                        'won_ticket' => '',
                        'ticket_number' => $scannedticket,
                        'prize' => 'No Prize',
                        'prize_amount' => '',
                        'ticket_name' => strtoupper($winnig_detail[1]),
                        'drawcode' => $winning_no_prize_arr[5].'-'.$winning_no_prize_arr[4],
                        'date' => $winning_arr[0],
                    );
                //}
                }
            }

            $wonshopsarr = array_reverse($wonshopsarr);
//            if (!empty($wonshopsarr)) {
                foreach ($wonshopsarr as $wonshopsarrs) {
                    foreach ($wonshopsarrs as $key => $val) {
                        $data['details'][] = $val;
                    }
                }
                
//            }
//                                    echo '<pre>'; print_r($data);
//exit;
            if (empty($data['details'])) {
                $data = 'NODATA';
                $status = FALSE;
            } else {
                $data = $data;
                $status = TRUE;
            }
            $finaldata = array('winning_details' => $data);
            $message = 'Winning Displayed successfully';
            $this->response([
                'status' => $status,
                'message' => 'Winning Displayed successfully',
                'winningdata' => $finaldata,
                //'barcodewinning_url' => $barcodewinning_url,
                    ], REST_Controller::HTTP_CREATED);
        } else {
            $message = 'Not Supplied with sufficcient parameters';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code  
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP 
    }

    private function _generate_key() {
        do {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE) {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        } while ($this->_key_exists($new_key));

        return $new_key;
    }

    /* Private Data Methods */

    private function _get_key($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->get(config_item('rest_keys_table'))
                        ->row();
    }

    private function _key_exists($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function _insert_key($key, $data) {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        return $this->rest->db
                        ->set($data)
                        ->insert(config_item('rest_keys_table'));
    }

    private function _update_key($key, $data) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->update(config_item('rest_keys_table'), $data);
    }

    private function _delete_key($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->delete(config_item('rest_keys_table'));
    }

}
