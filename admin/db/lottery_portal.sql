-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2019 at 06:28 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lottery_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_type` enum('ADMIN','AGENT','SHOPPE') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `middlename`, `lastname`, `password`, `created_at`, `updated_at`, `deleted_at`, `user_type`) VALUES
(1, 'admin', 'satheesh kumar', NULL, 'kumar', '123456', '2019-02-08 08:35:29', '2019-02-08 08:35:29', '2019-02-08 08:35:29', 'ADMIN'),
(2, 'kamar', 'fathima  beevi kutty', NULL, 'kamar', '123', '2019-02-08 18:07:23', '2019-02-08 18:07:23', '2019-02-08 18:07:23', 'AGENT'),
(3, 'anvar', 'vahab', NULL, 'anvar', '000', '2019-01-31 05:38:49', '2019-01-31 05:38:49', '2019-01-31 05:38:49', 'SHOPPE'),
(5, 'shome', 'bhaiiiii', NULL, 'ashref', '963', '2019-02-10 16:58:01', '2019-02-10 16:58:01', '2019-02-10 16:58:01', 'SHOPPE'),
(6, 'prajeesh', 'prabhakaran', NULL, 'achu', '823', '2019-01-31 05:38:49', '2019-01-31 05:38:49', '2019-01-31 05:38:49', 'SHOPPE'),
(7, 'abhi', 'abhilash', NULL, 'achu', '120', '2019-02-06 06:55:43', '2019-02-06 06:55:43', '2019-02-06 06:55:43', 'AGENT'),
(8, 'fathima beevi', 'fathima beevi', NULL, 'anvar', '000', '2019-02-08 18:02:04', '2019-02-08 18:02:04', '2019-02-08 18:02:04', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `user_files`
--

CREATE TABLE `user_files` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `enable_archive_option` enum('YES','NO') DEFAULT 'YES',
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_files`
--

INSERT INTO `user_files` (`id`, `user_id`, `file_name`, `file_path`, `enable_archive_option`, `created_at`, `deleted_at`) VALUES
(109, 1, 'sathhesh', '.gitignore', 'YES', '2019-02-10 15:18:54', '2019-02-10 15:18:54'),
(110, 1, 'abhiphoto', 'license.txt', 'YES', '2019-02-10 15:21:49', '2019-02-10 15:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_files_viewable`
--

CREATE TABLE `user_files_viewable` (
  `id` int(11) NOT NULL,
  `user_files_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_files_viewable`
--

INSERT INTO `user_files_viewable` (`id`, `user_files_id`, `user_id`) VALUES
(69, 109, 6),
(71, 110, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_files`
--
ALTER TABLE `user_files`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_user_files_user_idx` (`user_id`);

--
-- Indexes for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  ADD PRIMARY KEY (`id`,`user_files_id`,`user_id`),
  ADD KEY `fk_user_files_viewable_user_files1_idx` (`user_files_id`),
  ADD KEY `fk_user_files_viewable_user1_idx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_files`
--
ALTER TABLE `user_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_files`
--
ALTER TABLE `user_files`
  ADD CONSTRAINT `fk_user_files_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  ADD CONSTRAINT `fk_user_files_viewable_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_files_viewable_user_files1` FOREIGN KEY (`user_files_id`) REFERENCES `user_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
