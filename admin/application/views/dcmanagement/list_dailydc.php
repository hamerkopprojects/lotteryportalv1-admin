<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        
        <div class="table-agile-info">
            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                  <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
                <?php } ?>
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Please fill all the fields.
                </div>
                <div class="panel-heading">
                    <b style="color:#444">Daily Dc List</b>
                </div>
                
                    <div class="pull-LEFT" style="margin-left: 20px;margin-top: 10px;">
                            <input type="text" class="form-control" id="dateofdc" name="dateofdc"  value="<?php echo $date_of_dc;?>" placeholder="Select a date" onchange="dateselect();">
                    </div>
                    <div class="pull-LEFT" style="margin-left: 20px;margin-right: 20px;padding-top: 10px;">
                            <select name="tickets_id" id="ticketname" class="form-control select2" style="width: 100%;">
                            <?php 
                                foreach ($ticketlist as $key => $value) { ?>
                                 <!-- echo '<pre>'; print_r($ticketlist ); exit;-->
                                <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id'];?>"><?php echo $value['ticket_name'];?></option>
                            <?php 
                                }
                            ?>
                            </select>
                    </div>
                            <div class="pull-LEFT" style="margin-left: 20px;margin-right: 20px;padding-top: 10px;">
                            <select name="shopname" id="shopcode" class="form-control select2" style="width: 100%;">
                                 <?php 
                                foreach ($shop_details as $key => $value) { ?>
                                <option <?php if (isset($selectedshopcode) && $selectedshopcode == $value['shop_code']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['shop_code'];?>"><?php echo $value['shopname'];?></option>
                                 <?php 
                                }
                            ?>
                            </select>
                            </div>
                    <?php if($date_of_dc == date('Y-m-d')) { ?> 
                        <?php if(!empty($todays_dc_details)) { ?>
                   
                            <div class="pull-LEFT" style="margin-right: 20px;margin-top: 10px;font-weight: bolder">
                                     <input type="button" class="btn btn-secondary" value="Sync Daily Dc" onclick="alert('Please Clear Dc Data to Sync Again');"> 
                            </div>
                        <?php } else { ?>
                        
                            <div class="pull-LEFT" style="margin-right: 20px;margin-top: 10px;font-weight: bolder">
                                     <input type="button" class="btn btn-info" value="Sync Daily Dc" onclick="syn_dc();"> 
                            </div>
                        <?php } ?>

                            <div class="pull-LEFT" style="margin-right: 20px;margin-top: 10px;font-weight: bolder">
                                     <input type="button" class="btn btn-info" value="Clear Daily DC" onclick="clearunsolddata();"> 
                            </div>
                    <?php } ?>
                <div class="scrollmenu" >
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="10%">Bill Number</th>
                                <th width="15%">Price</th>
                                <th width="10%">Agent Name</th>
<!--                                <th width="15%">shop Name</th>-->
                                <th width="15%">Serial code</th>
                                <th width="10%">Ticket Number</th>
                                <th width="10%">Total DC</th>
                                <th width="10%">Payment Status</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                     
                            <?php
                            $i = 0;
                            foreach ($todays_dc_details as $key) {
                                $i++;
                                $date = $key['date'];
                                $newDate = $date;
                                $newDate = date("d-m-Y ", strtotime($newDate));
                                ?>
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td width="10%"><?php echo $key['bill_no']; ?></td>
                                    <td width="15%"><b><?php echo $key['price_type']; ?></b></td>
                                    <td width="10%"><?php echo $key['agent_name']; ?></td>
                                    
                                    <?php //$shopnames = $this->Lottery_model->getShopName($key['shop_code']); ?>
                                    
<!--                                    <td width="15%"><?php echo $shopnames[0]['shopname']; ?></td>-->
                                    <td width="15%"><?php echo $key['serial_code']; ?></td>
                                    <td width="10%"><?php echo $key['ticket_number']; ?></td>
                                    
                                    <td width="10%"><b><?php echo $key['total_dc']; ?></b></td>
                                    <td width="10%">
                                        <b>
                                      <?php echo  $key['payment']; ?>
                                        
                                        </b>
<!--                                        <select name="payment_status" id="payment_status">
                                            <option <?php if (isset($key['payment']) && $key['payment'] == "UNPAID") { ?> selected="selected" <?php } ?>  value="UNPAID" style="background-color: green;color:white;font-weight: bolder">UNPAID</option>
                                             <option <?php if (isset($key['payment']) && $key['payment'] == "PAID") { ?> selected="selected" <?php } ?>  value="PAID" style="background-color: red;color:white;font-weight: bolder">PAID</option>
                                        </select>-->
                                    </td>
                                </tr>
                                    
                            <?php
                            }
                            ?>
                                <?php if($_GET['data'] == 'nil'){ ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="color:red;font-weight: bolder;font-size: 15px">NO DATA TO SYNC</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php } ?>                            
                            
                        </tbody>
                    </table>

                 
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function dateselect(){
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc/"+$('#dateofdc').val();
            $(location).attr('href', url);        
        }
        function syn_dc(){
        jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
             
     
        var url = "<?php echo base_url(); ?>Dcmanagement/sync_dailydc_save/"+$('#dateofdc').val()+"/"+$('#ticketname').val()+"/"+$('#shopcode').val();
        $(location).attr('href', url); 
            
        }
        
        function clearunsolddata(){
            var url = "<?php echo base_url(); ?>Dcmanagement/clear_dc_details/<?php echo $date_of_dc;?>/"+$('#ticketname').val()+"/"+$('#shopcode').val();
            $(location).attr('href', url); 
        }
        $("#ticketname").change(function () {
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc/"+$('#dateofdc').val()+"/"+$('#ticketname').val()+"/"+$('#shopcode').val();
            $(location).attr('href', url); 
        });
         $("#shopcode").change(function () {
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc/"+$('#dateofdc').val()+"/"+$('#ticketname').val()+"/"+$('#shopcode').val();
            $(location).attr('href', url); 
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000); 
            $('.alert-warning').hide();
            $("#dateofdc").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
            
            $("#fbody").on('keyup', ".sold_total, .sold_winning, .pwt_dc", function (e) {
                var total = 0;
                var sold_total = $('.sold_total').val();
                var sold_winning = $('.sold_winning').val();
                var pwt_dc = $('.pwt_dc').val();

                if(sold_total !='' && sold_winning =='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc !=''){
                    total = parseInt(sold_total)-(parseInt(sold_winning)+parseInt(pwt_dc));
                }
                if(total > 0){
                    
                    $('#profit_loss_save').val('-'+total)
//                    $("#profit_loss").html("<div style='color:red;font-weight:bolder;'>-"+Math.abs(total)+"</div>");
                } else if(total < 0){
                    $('#profit_loss_save').val(Math.abs(total))
//                    $("#profit_loss").html("<div style='color:green;font-weight:bolder;'>"+Math.abs(total)+"</div>");
                }
            });
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>