<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        
        <div class="table-agile-info">
            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                  <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
                <?php } ?>
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Please fill all the fields.
                </div>
                <div class="panel-heading">
                    <b style="color:#444">Daily Dc List - <?php echo $display_date; ?>/ Shop Code: <?php echo $shopcode; ?></b>
                    
                </div>
                <div class="scrollmenu" >
<div class="row">
    <div class="col-md-3">
    <div class="form-group">
    <label for="lastname">Bill No:</label>
    <input type="text" class="form-control" id="billno" name="billno"  value="<?php echo $billno;?>">
    </div>
    </div>
    <div class="col-md-1">
        <div class="form-group" style="padding-top: 25px;">
        <label for="lastname"></label>
    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Search" onclick="search();">
    </div>
    </div>
        <div class="col-md-1">
        <div class="form-group" style="padding-top: 25px;">
        <label for="lastname"></label>
    <input type="button" class="btn btn-warning" style="font-weight:bolder!important;" value="Reset" onclick="reset();">
    </div>
    </div>
        <div class="col-md-1">
        <div class="form-group" style="padding-top: 25px;">
        <label for="lastname"></label>
    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Go Back" onclick="goback();">
    </div>
    </div>
</div>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="10%">Bill Number</th>
                                <th width="10%">Agent Name</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                     
                            <?php
                            $i = 0;
                            foreach ($todays_dc_details as $key) {
                                $i++;
                                $date = $key['date'];
                                $newDate = $date;
                                $newDate = date("d-m-Y ", strtotime($newDate));
                                ?>
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td width="10%" style="font-weight:bolder;"><?php echo $key['bill_no']; ?></td>
                                    <td width="10%" style="font-weight:bolder;"><?php echo $key['agent_name']; ?></td>
                                    <td width="10%"><input type="button" class="btn btn-info" value="View Details" onclick="dcdetails(<?php echo $key['bill_no']; ?>);"></td>
                                </tr>
                                    
                            <?php
                            }
                            ?>
                                <?php if($_GET['data'] == 'nil'){ ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="color:red;font-weight: bolder;font-size: 15px">NO DATA TO SYNC</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php } ?>                            
                            
                        </tbody>
                    </table>

                 
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function dateselect(){
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc?date="+$('#dateofdc').val();
            $(location).attr('href', url);        
        }
        function syn_dc(){
        jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
             
     
        var url = "<?php echo base_url(); ?>Dcmanagement/sync_dailydc_save?date="+$('#dateofdc').val()+"&&ticket="+$('#ticketname').val()+"&&shopcode="+$('#shopcode').val();
        $(location).attr('href', url); 
            
        }
        
        function clearunsolddata(){
            var url = "<?php echo base_url(); ?>Dcmanagement/clear_dc_details?date=<?php echo $date_of_unsold;?>&&ticket="+$('#ticketname').val()+"&&shopcode="+$('#shopcode').val();
            $(location).attr('href', url); 
        }
        
        function dcdetails(billno){
            var url = "<?php echo base_url(); ?>dc?s=<?php echo $shopcode; ?>&&b="+billno+"&&user=admin&&ticket=<?php echo $selectedticket; ?>&&date=<?php echo $date_of_unsold; ?>";
            $(location).attr('href', url);             
        }
        function search(){
        jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
             
     
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc_details?date=<?php echo $date_of_unsold; ?>&&ticket=<?php echo $selectedticket; ?>&&shopcode=<?php echo $shopcode; ?>&&billno="+$('#billno').val();
            $(location).attr('href', url); 
            
        }
        function reset(){
        jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
             
     
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc_details?date=<?php echo $date_of_unsold; ?>&&ticket=<?php echo $selectedticket; ?>&&shopcode=<?php echo $shopcode; ?>&&billno=";
            $(location).attr('href', url); 
            
        }
        function goback(){
            var url = "<?php echo base_url(); ?>Dcmanagement/dc_details/?date=<?php echo $date_of_unsold;?>&&ticket=<?php echo $selectedticket; ?>&&shopcode=<?php echo $shopcode; ?>";
            $(location).attr('href', url); 
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000); 
            $('.alert-warning').hide();
            $("#dateofdc").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
            
            $("#fbody").on('keyup', ".sold_total, .sold_winning, .pwt_dc", function (e) {
                var total = 0;
                var sold_total = $('.sold_total').val();
                var sold_winning = $('.sold_winning').val();
                var pwt_dc = $('.pwt_dc').val();

                if(sold_total !='' && sold_winning =='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc !=''){
                    total = parseInt(sold_total)-(parseInt(sold_winning)+parseInt(pwt_dc));
                }
                if(total > 0){
                    
                    $('#profit_loss_save').val('-'+total)
//                    $("#profit_loss").html("<div style='color:red;font-weight:bolder;'>-"+Math.abs(total)+"</div>");
                } else if(total < 0){
                    $('#profit_loss_save').val(Math.abs(total))
//                    $("#profit_loss").html("<div style='color:green;font-weight:bolder;'>"+Math.abs(total)+"</div>");
                }
            });
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>