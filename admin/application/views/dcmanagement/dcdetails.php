<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(10000);
        $('.alert-danger').show().fadeOut(10000);
    });
</script>
<section id="main-content">
    <section class="wrapper">


        <div class="panel panel-default">
            <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                    <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('error')) { ?> 
                <div class="alert alert-danger">
                    <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php } ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> Please fill all the fields.
            </div>
            <div class="panel-heading">

                <div class="row" >
                    <div class="col-md-8" ><b style="color:#444">DC Details - <?php echo $display_date; ?></b></div>
                </div>
            </div>  
        </div>
        <div class="row">
            <div class="col-md-3">
                <b>Select Date: </b><input type="text" class="form-control" id="dateofdc" name="dateofdc"  value="<?php echo $date_of_unsold; ?>" placeholder="Select a date" onchange="dateselect();" >
            </div>
            <div class="col-md-3">
                <b>Tickets:</b>
                <select name="tickets_id" id="ticketnames" class="form-control select2" style="width: 100%;">
                    <?php foreach ($ticketlist as $key => $value) { ?>
                         <!-- echo '<pre>'; print_r($ticketlist ); exit;-->
                        <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id']; ?>"><?php echo $value['ticket_name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>  
            <div class="col-md-3">
                <div class="form-group">
                    <label for="lastname">Shopcode:</label>
                    <input type="text" class="form-control" id="shopcode" name="shopcode"  value="<?php echo $shopcode; ?>">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top: 25px;">
                    <label for="lastname"></label>
                    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Search" onclick="search();">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top: 25px;">
                    <label for="lastname"></label>
                    <input type="button" class="btn btn-warning" style="font-weight:bolder!important;" value="Reset" onclick="reset();">
                </div>
            </div>
        </div>
            <div class="table-responsive">
                <table class="table" ui-jq="footable" ui-options='{
                       "paging": {
                       "enabled": true
                       },
                       "filtering": {
                       "enabled": true
                       },
                       "sorting": {
                       "enabled": true
                       }}'>
                    <thead>
                        <tr>
<!--                                <th width="5%">No</th>-->
<!--                                <th width="10%">Shop Code</th>-->
                            <th width="10%">Shop Name</th>
                            <th width="10%">DC Details</th>
                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="fbody">

                        <?php
                        $i = 0;
                       
                        foreach ($details as $key) {
                            $i++;
                            //$date = $key['date'];
                            $newDate = $date;
                            $newDate = date("d-m-Y ", strtotime($newDate));
                            $color = ($i % 2 == 0) ? "#ddede0" : "#ffff";
                            ?>
                            <tr style ="background-color: <?php echo $color; ?>!important;">
    <!--                                    <td width="5%"><?php echo $i; ?></td>-->
    <!--                                    <td width="10%"><?php echo $key['shop_code']; ?></td>-->
                                <td width="10%" style="font-weight:bolder;font-size: 15px"><b><?php echo $key['shopname']; ?></b></td>
    <!--                                    <td width="5%"><input type="button" class="btn" style="font-weight:bolder;font-size: 20px;background-color: #b3b3b3!important;" value="<?php echo $key['shop_code']; ?>" onclick="dcdetails(<?php echo $key['shop_code']; ?>)"></td>-->

                                <?php
                                //echo $date; exit;
                                $selectdate = (isset($_GET['date'])) ? date('Y-m-d', strtotime($_GET['date'])) : date('Y-m-d');
                                $shops_daily_dc = $this->Lottery_model->getShopswithDCdataCount($key['shop_code'], $selectdate, $selectedticket);
                                $count = $shops_daily_dc[0]['count'];
                                //$count = 1;
                                if ($count < 1) {
                                    ?>
                                    <td width="5%"><input type="button" class="btn" style="font-weight:bolder;font-size: 20px;background-color: #b3b3b3!important;" value="<?php echo $key['shop_code']; ?>" onclick="alert('DC not synced please sync data')"></td></td>
                                        <?php } ?> 
<!--                                    <td width="10%">-->
                                        <?php //if ($date_of_unsold == date("Y-m-d")) { ?>
<!--                                            <input type="button" class="btn btn-info"  style="font-weight:bolder!important;" value="Sync DC" onclick="syn_dc(<?php echo $key['shop_code']; ?>);"><br><br>-->
                                          <!--  <input type="button" class="btn btn-danger" style="font-weight:bolder!important;" value="Clear DC" onclick="clearunsolddata(<?php echo $key['shop_code']; ?>);"><br><br>-->
            <!--                                        <input type="button" class="btn btn-success" style="font-weight:bolder!important;" value="Clear DC" onclick="dcdetails(<?php echo $key['shop_code']; ?>)">-->
                                        <?php //} ?> 
<!--                                    </td>-->

                                    <?php //} else {
                                    ?> 
                                <?php  if ($count > 0) { ?>
                                    <td width="5%"><input type="button" class="btn" style="font-weight:bolder;font-size: 20px;background-color: #b3b3b3!important;" value="<?php echo $key['shop_code']; ?>" onclick="dcdetails(<?php echo $key['shop_code']; ?>)"></td>
                                <?php } ?>
                                    <td width="10%">
        <?php //if ($date_of_unsold == date("Y-m-d")) { ?>
                                            <input type="button" class="btn btn-info"  style="font-weight:bolder!important;" value="Sync DC" onclick="syn_dc(<?php echo $key['shop_code']; ?>);"><br><br>
                                          <!--  <input type="button" class="btn btn-danger" style="font-weight:bolder!important;" value="Clear DC" onclick="clearunsolddata(<?php echo $key['shop_code']; ?>);"><br><br>--->
        <?php //} ?>
                                    <?php  if ($count > 0) { ?>
                                        <input type="button" class="btn btn-success" style="font-weight:bolder!important;" value="View DC Details" onclick="dcdetails(<?php echo $key['shop_code']; ?>)">
                                    <?php } ?>
                                    </td>

        

                            </tr>

    <?php
}
?>                          

                    </tbody>
                </table>


            </div>
    </section>
    <script type="text/javascript">
        function dateselect() {
            var url = "<?php echo base_url(); ?>Dcmanagement/dc_details/?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + $('#shopcode').val();
            //var url = "<?php echo base_url(); ?>dashboard/shoplist/"+$('#dateofdc').val();
            $(location).attr('href', url);
        }

        function syn_dc(shopcode) {
            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');


            var url = "<?php echo base_url(); ?>Dcmanagement/sync_dcdetails_save?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + shopcode;
            $(location).attr('href', url);

        }
//        function clearunsolddata(shopcode) {
//            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
//            var url = "<?php echo base_url(); ?>Dcmanagement/clear_dc_details_list?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + shopcode;
//            $(location).attr('href', url);
//        }

        function dcdetails(shopcode) {
            var url = "<?php echo base_url(); ?>Dcmanagement/list_dailydc_details?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + shopcode;
            $(location).attr('href', url);
        }
        ;
        $("#ticketnames").change(function () {
            var url = "<?php echo base_url(); ?>Dcmanagement/dc_details/?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + $('#shopcode').val();
            $(location).attr('href', url);
        });
        function search() {
            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');


            var url = "<?php echo base_url(); ?>Dcmanagement/dc_details?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=" + $('#shopcode').val();
            $(location).attr('href', url);

        }
        function reset() {
            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');


            var url = "<?php echo base_url(); ?>Dcmanagement/dc_details?date=" + $('#dateofdc').val() + "&&ticket=" + $('#ticketnames').val() + "&&shopcode=";
            $(location).attr('href', url);

        }
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000);
            $('.alert-warning').hide();
            $("#dateofdc").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });

        });
    </script>
    <?php
    $this->load->view('footer');
    ?>