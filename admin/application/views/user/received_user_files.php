<?php
$this->load->view('header');
?>

<script>
    $(document).ready(function() {

        ajaxCall();
    });

    function resetfile(){
        location.reload();
    }

    function ajaxCall() {   
          $.ajax({
            url: '<?php echo base_url(); ?>user/received_user_file_data',
            type: 'POST',
            success: function(data) {
               $("#recievedusers").html(data);
            }
        });
    } 

    function ajaxFilterCall() {  

        var assign_from = $('#assign_from').val();
        var assign_to = $('#assign_to').val();
        var datepicker = $('#datepicker').val();


        $.post('<?php echo base_url(); ?>user/received_user_file_data', { assign_from : assign_from, assign_to : assign_to, datepicker: datepicker}, 
            function(returnedData){
                $("#recievedusers").html(returnedData);
            });
    }

    
    function filesearch() {
        var file_search = $("#file_search").val();
        
        $.post('<?php echo base_url(); ?>user/received_user_file_data', { file_search : file_search}, 
            function(returnedData){
                $("#recievedusers").html(returnedData);
            });
    }
    
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        //$('#datepicker').datepicker().datepicker('setDate', 'today');
    });
</script>

<section id="main-content">
    <section class="wrapper">

        <div class="table-agile-info">
            <form method="post" action="<?php echo base_url(); ?>user/searchdata"  autocomplete="off">
                <div class="row">
                    <div class="col-md-3">  <div class="form-group">
                            <label for="usertype">Sent From</label>
                            <select class="form-control m-bot15" name="assign_from" id="assign_from">
                                <option value="">Please select</option>
                                <?php
                                foreach ($username as $usernames) {
                                    ?>
                                    <option  value="<?php echo $usernames->id; ?>"><?php echo $usernames->username; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">  <div class="form-group">
                            <label for="usertype">Sent To</label>
                            <select class="form-control m-bot15" name="assign_to" id="assign_to">
                                <option value="">Please select</option>
                                <?php
                                foreach ($username as $usernames) {
                                    ?>
                                    <option  value="<?php echo $usernames->id; ?>"><?php echo $usernames->username; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">  <div class="form-group">
                            <label for="usertype">Date</label>
                            <input type="text" class="form-control" id="datepicker" name="cal_date"  value="<?php echo date('Y-m-d');?>" placeholder="Select Date" >
                        </div>
                    </div>
                    
<!--                    <div class="col-md-2">  <div class="form-group">
                            <label for="usertype">File Search</label>
                            <input type="text" class="form-control" id="file_search" name="file_search"  placeholder="File Search" onkeyup="filesearch()">
                        </div>
                    </div>-->
                    <div class="col-md-1">
                        <div class="form-group">
                            <br>
                            <input type="button" class="btn btn-info" value="Go" onclick="ajaxFilterCall()">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <br>
                            <input type="button" class="btn btn-info" value="Reset" onclick="resetfile()">
                        </div>
                    </div>
                </div>
            </form>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b style="color:#444"> received File Listing</b>
                    <button onclick="adduserfile()" class="btn btn-info pull-right">Upload File</button>
                    <div class="panel-title pull-LEFT">
                        <input type="text" class="form-control" placeholder="Search" id="searchInput" style="background-color:#5bc0de" name="searchInput" onkeypress="return blockSpecialChar(event)" >

                    </div>
                </div>
                <div id="recievedusers">
                </div>
            </div>
        </div>
    </section>
    </section>


    <script type="text/javascript">
        function adduserfile()
        {
            var url = "<?php echo base_url() ?>user/create_user_file";
            $(location).attr('href', url);
        }
    </script>
<script>
    $(document).ready(function () {
        $("#searchInput").keyup(function () {
            var rows = $("#fbody").find("tr").hide();
            if (this.value.length) {
                var data = this.value.split(" ");
                $.each(data, function (i, v) {
                    rows.filter(":contains('" + v + "')").show();
                });
            } else
                rows.show();
        });
    });
</script>
    <?php
    $this->load->view('footer');
    ?>