<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(3000);  
        $('.alert-warning').hide();
    });
    function saveposition() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#saveposition").submit();
        }

    }
</script>


<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <b style="color:#444"> Time Settings</b>
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <?php if ($this->session->flashdata('message')) { ?> 
                                            <div class="alert alert-success">
                                              <strong><?php echo $this->session->flashdata('message'); ?></strong>
                                            </div>
                                            <?php } ?>
                                            <div class="alert alert-warning">
                                                <strong>Warning!</strong> Please fill all the fields.
                                            </div>
                                            <div class="x_content">
                                                <br />
                                                <form id="saveposition" action="savetime" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>&nbsp;</label>
                                                                    <div class="input-group">
                                                                        <b>FILE UPLOAD</b>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>From time</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="file_from_time" value="<?php echo $file_from_time; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>To time</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="file_to_time"  value="<?php echo $file_to_time; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2" style="margin-top: 25px">
                                                            <?php
                                                            $file_checked = 'checked';
                                                            if ($file_is_active == 'NO') {
                                                                $file_checked = '';
                                                            }
                                                            ?>
                                                            <input type="checkbox" <?php echo $file_checked; ?> data-toggle="toggle" data-on="Restriction ON" data-off="Restriction OFF" name="file_is_active">
                                                        </div>
                                                    </div>



                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>&nbsp;</label>
                                                                    <div class="input-group">
                                                                        <b>UNSOLD UPLOAD</b>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>From time  </label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="unsold_from_time"  value="<?php echo $unsold_from_time; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>To time</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="unsold_to_time" value="<?php echo $unsold_to_time; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2" style="margin-top: 25px">
                                                            <?php
                                                            $unsold_checked = 'checked';
                                                            if ($unsold_is_active == 'NO') {
                                                                $unsold_checked = '';
                                                            }
                                                            ?>
                                                            <input type="checkbox" <?php echo $unsold_checked; ?> data-toggle="toggle" data-on="Restriction ON" data-off="Restriction OFF" name="unsold_is_active">
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <button type="submit" class="btn btn-info" onclick="saveposition()">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</section>

<?php
$this->load->view('footer_time');
$this->load->view('footer');
?>
</html>

