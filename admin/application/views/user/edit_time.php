<?php
$this->load->view('header');
?>
<!--<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-warning').hide();
    });
    function saveposition() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#saveposition").submit();
        }

    }
</script>-->

<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <b style="color:#444"> Edit Time Settings</b>
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">

                                            <div class="x_content">
                                                <br />
                                                <form role="form" id="createuser" method="post" action="<?php echo base_url(); ?>user/updatetime"/>
                                                <?php
                                                foreach ($time as $time_details) {
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>Type  :</label>
                                                                    <div class="input-group">
                                                                        <b><?php echo $time_details['type']; ?></b>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>                                                       
                                                        <div class="col-md-5">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>From time  :</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="from_time" value="<?php echo $time_details['from_time']; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="bootstrap-timepicker">
                                                                <div class="form-group">
                                                                    <label>To time</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control timepicker" name="to_time" value="<?php echo $time_details['to_time']; ?>">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php } ?>
                                                <input type="hidden" name="timeid" value="<?php echo $time_details['id']; ?>">
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                        <button type="submit" class="btn btn-info" onclick="saveposition()">Submit</button>
                                                    </div>
                                                </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</section>

<?php
$this->load->view('footer_time');
$this->load->view('footer');
?>
</html>

