<?php
if($user != 'agent'){
 $this->load->view('header');
} else {
 $this->load->view('header-agent');  
}
?>
<style>
  @media only screen and (max-width: 600px) {
  iframe {
    width: 100%;
    height: 1200px
  }
    span {
  display:block;
  width:45%;
  word-wrap:break-word;      
}
}
  @media only screen and (min-width: 600px) {
  iframe {
    width: 100%;
    height: 1200px
  }
p
</style>
<script type="text/javascript">
        function goback(){
            var url = "<?php echo $url; ?>";
            $(location).attr('href', url);        
        }
</script>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="panel-body">
                <div class="col-md-12 w3ls-graph">

                    <div class="">
                        <div class="agileits-box">
                            <header class="agileits-box-header clearfix">
                                <div class="col-md-4" ></div>
                                <div class="col-md-4" >
                                <h3>DC Details</h3>
                                </div>
                                <div class="col-md-4" ></div>
                                <div class="toolbar">
                                </div>
                            </header>
<!DOCTYPE html>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;  
  padding: 8px;
}
tr:nth-child(even) {
  background-color: #dddddd;
}
.borderless td, .borderless th {
    border: none !important;
}
</style>
<?php 

if($user != 'agent'){ ?>
<div class="row">
    <div class="col-md-6">
    <div class="form-group">
    <label for="lastname"></label>
    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Go Back" onclick="goback();">
    </div>
    </div>
</div>
 
<?php } ?>
<?php $shopdetail = $this->Lottery_model->getByShopCode($details[0]['shop_code']); ?>
<?php if(!empty($details))
{?>
<table><tr><th bgcolor='#87CEFA'><font color='#000080' size='3'>SHOP CODE : <?php echo $details[0]['shop_code'];?>  (<?php echo $shopdetail[0]['shopname']; ?>)</font></th></tr></table>

<table>
<tr>
    <td><b>Agent Name :</b></td>
    <td align="right" style="background-color:lemonchiffon;color:red"><b> <?php echo $details[0]['agent_name']; ?></b></td>
  </tr>
  <tr>
    <td><b>DC Date :</b></td>
    <td align="right" style="background-color:lemonchiffon;color:red"><b><?php echo date("d-m-Y", strtotime($details[0]['date'])); ?></b></td>
  </tr>
 <tr>
    <td style="background-color:white"><b>Bill No :</b></td>
    <td align="right" style="background-color:lemonchiffon;color:red"><b><?php echo $details[0]['bill_no']; ?></b></td>
  </tr>
  <tr>
    <td style="background-color:white"><b>DC Total :</b></td>
    <td align="right" style="background-color:lemonchiffon;color:red"><b><?php echo $totaldc[0]['totaldc']; ?></b></td>
  </tr>
    <tr>
    <td style="background-color:white"><b>Payment Status :</b></td>
    <?php $color = ($details[$key]['payment'] == 'PAID')?"green":"red"; ?>
    <td align="right" style="background-color:lemonchiffon;color:<?php echo $color; ?>"><b><?php echo $details[0]['payment']; ?></b></td>
  </tr>
</table>
<?php }else{?>
<table class='table borderless' >
 <tr>
          <td align="center" style="color:red;font-weight: bolder;font-size: 15px">No DC Found - Contact Shop</td>

             </tr>
</table>
            <?php } ?>  
<br>
<?php 
$i = 1; 
foreach($details as $key => $val) {?>
<table><tr style="width:100%;"><th bgcolor='#800000'><font color='#ffffff' size='2'>DC DETAIL - <?php echo $i; ?></font></th></tr></table>
<table>
    <tr style="width:100%;"><td align="center" ><b>Price Detail</b></td><td align="center" ><?php echo $details[$key]['price_type']; ?></td></tr>
    <tr style="width:100%;"><td align="center" ><b>Serial</b></td><td align="center"><span><?php echo $details[$key]['serial_code']; ?></span></td></tr>
    <tr><td align="center"><b>Ticket Number</b></td><td align="center"><?php echo $details[$key]['ticket_number']; ?></td></tr>
    <tr><td align="center"><b>DC Amount</b></td><td align="center"><?php echo $details[$key]['total_dc']; ?></td></tr>
    <?php //$color = ($details[$key]['payment'] == 'PAID')?"green":"red"; ?>
<!--    
    <tr><td align="center"><b>Payment Status</b></td>
        <td align="center" style="color:<?php echo $color; ?>;font-weight:bolder;"><?php echo $details[$key]['payment']; ?></td>
    </tr>-->
</table>
<br>
<?php 
$i++;
} ?>
<?php if($user != 'agent'){ ?>
<div class="row">
    <div class="col-md-6">
    <div class="form-group">
    <label for="lastname"></label>
    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Go Back" onclick="goback();">
    </div>
    </div>
</div>
<?php } ?>
<!--                            </div>-->
<!--                            <div class="agileits-box-body clearfix">
                                <div id="hero-area"></div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

</section>
<?php

$this->load->view('footer');
?>