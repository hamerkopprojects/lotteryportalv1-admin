<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-warning').hide();
    });
    function createuser() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#createuser").submit();
        }
    }
    function back() {
        var url = "<?php echo base_url(); ?>user/view_users";
        $(location).attr('href', url);
    }
</script>

<section id="main-content">
  <?php  $usertype = $this->session->userdata['user']['user_type'];?>
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning">
                        <strong>Warning!</strong> Please fill all the fields.
                    </div>
                    <section class="panel">
                        <header class="panel-heading">
                            <b style="color:#444">USER CREATION</b>

                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" id="createuser" method="post" action="<?php echo base_url() ?>user/saveusers">
                                    <div class="form-group">
                                        <label for="firstname">First name</label>
                                        <input type="text" class="form-control" id="firstname" name="firstname" required="required" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Last name</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Password" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">User name</label>
                                        <input type="text" class="form-control" id="username" name="username" onblur="check_username();" placeholder="Password" required="">
                                        <span class="msg" id="msg" style="color:red"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" class="form-control" id="password" name="password" placeholder="Password" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="confpassword">Confirm Password</label>
                                        <input type="text" class="form-control" id="confpassword" name="confpassword" placeholder="Password" required="">
                                    </div>
                                    <div class="form-group">
                                        <span class="error" style="color:red"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="usertype">User type</label>
                                        <select class="form-control m-bot15" name="usertype" id="usertype" required="">
                                            
                                
                                            <option value="">Please select</option>
                                            <option value="ADMIN">ADMIN</option>
                                            <option value="AGENT">AGENT</option>
                                            <option value="SHOP">SHOP</option>
                                            <option value="OTHERS">OTHERS</option>
                                        </select>
                                         
                                    </div>
                                      <?php
                                   if ($usertype == 'TECHNICIAN') {?>
                                     <div class="form-group">
                                      <label for="shop_code">Shop Code</label>
                                     <input type="text" class="form-control" id="shop_code" name="shop_code" placeholder="shop_code" >
                                    </div>
                                    <?php }?> 

                                    <div class="form-group">
                                        <label for="usertype">Select shop for Agents</label>
                                        <select class="form-control m-bot15" name="shop_id" id="shop_id" required="">
                                            
                                            
                                
                                            <option value="0">Please select</option>
                                            <?php foreach($shops as $shop){ ?>
                                            <option value="<?php echo $shop['shop_code']; ?>"><?php echo $shop['shopname']; ?></option>
                                            <?php } ?>
                                        </select>
                                         
                                    </div>                                    
                                    
                                    
                                     
                                    <div id="buttons">
                                        <button type="submit" onclick="createuser()" class="btn btn-info" id="submitt" style="float:left">Submit</button>
                                        <button type="submit" onclick="back()" class="btn btn-info" style="float:right">Back</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

</section>

<script type="text/javascript">
    var allowsubmit = false;
    $(function () {
        //on keypress
        $('#confpassword').keyup(function (e) {
            //get values
            var password = $('#password').val();
            var confpassword = $(this).val();
            //check the strings
            if (confpassword == password) {
                //if both are same remove the error and allow to submit
                $('.error').text('');
                allowsubmit = true;
            } else {
                //if not matching show error and not allow to submit
                $('.error').text('Password not matching');
                allowsubmit = false;
            }
        });
    });

    function check_username()
    {
        var username = $("#username").val();
        $.ajax(
                {
                    type: "post",
                    url: "<?php echo base_url(); ?>user/username_exists",
                    data: {username: username},
                    success: function (response)
                    {
                        if (response == true)
                        {
//                            $('#msg').html('<span style="color: green;">' + msg + "</span>");
                            $('#msg').html('<span style="color:red;">Username is not available </span>');
                            
                        } else
                        {
                            $('#msg').html('<span style="color:green;">Username is available</span>');
                             
                        }
                    }
                });

    }
</script>
<?php
$this->load->view('footer');
?>