    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Sent From</th>
                                <th>Sent To</th>
                                <th>Sent Date</th>
                                <th>Download File</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            if(!empty($users_file)){
                            $i = 0;
                            foreach ($users_file as $userfiles):
                                $i++;
                                $originalDate = $userfiles['created_at'];
                                $newDate = date("d-m-Y g:i:s A", strtotime($originalDate));
                                $filepath = explode('@#@$@%*&', $userfiles['file_path']);
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $userfiles['file_name']; ?></td>
                                    <td><?php echo $userfiles['username']; ?></td>
                                    <td><?php echo $userfiles['assigned_user']; ?></td>
                                    <td><?php echo $newDate; ?>
<!--                                    <td><a href="<?php echo base_url() . 'user/download/' . $userfiles['user_files_id']; ?>" class="btn btn-primary">Download </a></td>-->
                                    <td>
                                        <select id="downloadfile" name="downloadfile" class="form-control select2 downloadfile" style="width: 100%;">
                                            <option value="">Select a file</option>
                                            <?php 
                                                if (!empty($userfiles['file_path'])) {
                                                    foreach ($filepath as $key => $value) {
                                                        $filenamelast =  end(explode('/', $value));
                                                        $filenameArray =  explode('random', $filenamelast);
                                                        $filenamedisplay = $filenameArray[0];
                                                        if (sizeof($filenameArray) > 1) {
                                                            $filenamedisplay = $filenameArray[1];
                                                        }
                                                        
                                            ?>
                                            <option value="<?php echo $value;?>"><?php echo $filenamedisplay;?></option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </td>
                                    </td>
                                <?php endforeach;
                            } else {
                                ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>No Files Recieved this day</b></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".downloadfile").change(function(){
            //$( "#recievedusers" ).on( "change", "#downloadfile", function() {
                var selectedFile = $(this).children("option:selected").val();
                if(selectedFile != ''){
                    var url = "<?php echo base_url(); ?>user/download?filepath=" + selectedFile;
                    $(location).attr('href', url);
                } else {
                    location.reload(true);
                }
            });
        });
    </script>

