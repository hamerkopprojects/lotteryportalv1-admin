<?php
$this->load->view('header');
?>
<script type="text/javascript">


    function back() {
        var url = "<?php echo base_url() ?>user/view_users";
        $(location).attr('href', url);
    }
    function createuser() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#createuser").submit();
        }
    }

</script>
<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">

                        <header class="panel-heading">
                            <b style="color: 444">  UPDATE USER</b>
                        </header>
                        <div class="panel-body">
                            <?php $usertype = $this->session->userdata['user']['user_type']; ?>
                            <div class="position-center">
                                <form role="form" id="createuser" method="post" action="<?php echo base_url(); ?>user/updateusers"/>

                                <?php
                                foreach ($user as $userview) {
                                    ?>

                                    <div class="form-group">
                                        <label for="firstname">First name</label>
                                        <input type="text" class="form-control" id="firstname" name="firstname" required="required" value="<?php echo $userview['firstname']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Last name</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $userview['lastname']; ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label for="username">User name</label>
                                        <input type="text" class="form-control" id="username" name="username"  value="<?php echo $userview['username']; ?>"  >
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" class="form-control" id="password" name="password" value="<?php echo $userview['password']; ?>" >
                                    </div>
                                <?php } ?>
                                <div class="form-group">


                                    <?php if (($userview['user_type'] != 'SUPERADMIN') && ($userview['user_type'] != 'TECHNICIAN')) { ?>

                                        <label for="usertype">User type</label>
                                        <select class="form-control m-bot15" name="usertype" id="usertype"/>

                                        <option <?php echo ($userview['user_type'] == "AGENT") ? 'selected="selected"' : ''; ?> value="AGENT">AGENT</option>
                                        <option <?php echo ($userview['user_type'] == "SHOP") ? 'selected="selected"' : ''; ?> value="SHOP">SHOP</option>
                                        <option <?php echo ($userview['user_type'] == "ADMIN") ? 'selected="selected"' : ''; ?> value="ADMIN">ADMIN</option>
                                        <option <?php echo ($userview['user_type'] == "OTHERS") ? 'selected="selected"' : ''; ?> value="OTHERS">OTHERS</option>
                                        </select>
                                    <?php }
                                    ?>

       </div>
                                <?php
                                if ($usertype == 'TECHNICIAN') {
                                        ?>
                                        <div class="form-group">
                                            <label for="shop_code">Shop Code</label>
                                            <input type="text" class="form-control" id="shop_code" name="shop_code"  value="<?php echo $userview['shop_code']; ?>" >
                                        </div>   
                                <?php } else { ?> 
                                       <input type="hidden" class="form-control" id="shop_code" name="shop_code"  value="<?php echo $userview['shop_code']; ?>" >
                                <?php } ?>
                                       <?php //echo '<pre>'; print_r($userview); exit; ?>
                                    <div class="form-group">
                                        <label for="usertype">Select shop for Agents</label>
                                        <select class="form-control m-bot15" name="shop_id" id="shop_id" required="">
                                            
                                            
                                
                                            <option value="0">Please select</option>
                                            <?php foreach($shops as $shop){ ?>
                                            <option  <?php echo ($userview['shop_code'] == $shop['shop_code']) ? 'selected="selected"' : ''; ?>  value="<?php echo $shop['shop_code']; ?>"><?php echo $shop['shopname']; ?></option>
                                            <?php } ?>
                                        </select>
                                         
                                    </div>                                        
                                <button type="submit" onclick="createuser()" class="btn btn-info">Update</button>
                                <input type="hidden" name="userid" value="<?php echo $userview['id']; ?>">

                                </form>
                                <button style="float: right;margin-top: -50px"type="button" onclick="back()" class="btn btn-info">Back</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</section>
<?php
$this->load->view('footer');
?>