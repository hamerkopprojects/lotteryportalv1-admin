<?php
$this->load->view('header');
?>
<script type="text/javascript">
    function createuser() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#createuser").submit();
        }
    }
</script>
<?php 
    $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata')); 
    $time_now = $dateTime->format("h:i:s A");
    $status = $this->Lottery_model->isUploadBlocked("FILE");
?>
<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
        <?php if($status == "true"){ ?>
            <div style="color:red;font-size: 20px; font-weight: bold;padding-bottom: 20px;padding-left: 200px;">Please Contact Admin to remove the time block</div>
        <?php } ?>
        <?php if($status == "false"){ ?>
                    <section class="panel">
                        <header class="panel-heading">
                            <b style="color:#444;">  UPDATE UPLOADED FILE</b>
                        </header>
                        <div class="panel-body">
                            <div class="position-center">

                                <form role="form" id="createuser" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>user/updateuserfiles">
                                    <?php 
                                        $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata')); 
                                        $time_now = $dateTime->format("h:i:s A");
                                        $status = $this->Lottery_model->isUploadBlocked("FILE");
                                    ?>
                                    
                                    <div class="form-group">
                                        <?php if($status == "true"){ ?>
                                            <div style="color:red;font-size: 16px; font-weight: bold;padding-bottom: 20px">Please Contact Admin to remove the time block</div>
                                        <?php } ?>
                                        <label for="filename">Title</label>
                                        <input type="text" class="form-control"  style="width: 400px;" value="<?php echo $assignfiles[0]['file_name']; ?>" id="filename" name="filename" required="required" placeholder="Enter title">
                                    </div>
<!--                                <div class="form-group">
                                    <label for="exampleInputFile">User File</label>
                                    <br>
                                    <input name="file_name" type="file" />
                            
                                    <label for="filename" style="color:#444">  <?php echo $files['file_path']; ?></label>

                                
                                </div>-->

                                    <div class="form-group">
                                        <label for="exampleInputFile">Add a user file</label>
                                        <span  class="btn btn-info btn-sm" id="Add_file">
                                            <span class="glyphicon glyphicon-plus-sign"></span>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <div id="filesarea" class="row">
                                            <input type="hidden" name="existing_files_name" value="<?php echo $assignfiles[0]['file_path'];?>">
                                            <?php
                                                if (!empty($assignfiles[0]['file_path'])) {
                                                    $allfiles =  explode('@#@$@%*&', $assignfiles[0]['file_path']);
                                                    foreach ($allfiles as $key => $value) {
                                            ?>
                                            <div class="fileeach" style="float:left">
                                                <div class="form-group col-md-8">
                                                    <input type="file" name="file_name[]">
                                                    <input type="hidden" name="file_name_already_have[]" value="<?php echo $value;?>">
                                                    <?php 
                                                            $filenamelast = end(explode('/', $value));
                                                            $filenameArray = explode('random', $filenamelast);
                                                            $filenamedisplay = $filenameArray[0];
                                                            if (sizeof($filenameArray) > 1) {
                                                                $filenamedisplay = $filenameArray[1];
                                                            }
                                                    
                                                    ?>
                                                    <a target=”_blank” href="<?php echo str_replace("admin/","assets/uploads/",base_url().$value);?>"><?php echo $filenamedisplay;?></a>
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="btn btn-info btn-sm deletefile">
                                                        <span class="glyphicon glyphicon-minus-sign">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                                    }
                                                }  else {
                                            ?>
                                            <div class="fileeach" style="float:left">
                                                <div class="form-group col-md-8">
                                                    <input type="file" name="file_name[]">
                                                    <input type="hidden" name="file_name_already_have[]" value="">
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="btn btn-info btn-sm deletefile">
                                                        <span class="glyphicon glyphicon-minus-sign">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php 
                                                }
                                            ?>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <input type="hidden" name="file_id" value="<?php echo $this->uri->segment(3); ?>" />
                                    <label for="usertype">Send To</label>
                                    <br>
                                    <select class="form-control m-bot15" name="userOpt[]" multiple id ="userOpt">
<!--                                        <option value="">Select User</option>                                            -->
                                        <?php
                                        $i = 0;
                                        foreach ($username as $users) {
                                            if (in_array($users->id, $assigneduser)) {

                                                $selected = 'selected = "selected"';
                                            } else {
                                                $selected = "";
                                            }
                                            ?>
                                        <?php if($users->id != $this->session->userdata['user']['id']){ ?>
                                            <option value="<?php echo $users->id; ?>" <?php echo $selected; ?>><?php echo $users->username; ?>(<?php echo $users->user_type; ?>)</option>
                                            <?php
                                            $i++;
                                        }
                                        }
                                        ?>
                                    </select>
                                        <div class="invalid-userOpt" style="color:red;display: none">
                                                Please select shops or agents.
                                         </div>
                                </div>

                        <?php if($status == "false"){ ?>
                                <div id="buttons">
                                <button type="submit" id="fileuploadsubmit" onclick="createuser()" class="btn btn-info">Update</button>
                                <button type="submit" onclick="back()" class="btn btn-info" style="float:right">Back</button>
                                </div>
                        <?php }?>
                                </form>
                            </div>
                        </div>
                    </section>
        <?php } ?>
                </div>
            </div>
        </div>
    </section>
</section>
<script>
    $(document).ready(function () {
        $("#Add_file").on("click", function () {
            $("#filesarea").append("<div class=\"fileeach\" style=\"float:left\">\n\
                                       <div class=\"form-group col-md-8\">\n\
                                            <input type=\"file\" name=\"file_name[]\">\n\
                                            <input type=\"hidden\" name=\"file_name_already_have[]\" value=\"\">\n\
                                        </div>\n\
                                        <div class=\"col-md-4\">\n\
                                            <span class=\"btn btn-info btn-sm deletefile\">\n\
                                                <span class=\"glyphicon glyphicon-minus-sign\">\n\
                                                </span>\n\                                        \n\
                                            </span>\n\
                                        </div>\n\
                                        <div class='clear'></div>\n\
                                    </div>");
        });
        $("#filesarea").on("click", ".fileeach .deletefile", function () {
            $(this).parents('.fileeach').remove();
            var len = $(".noticeeach").length;
        });
        
        $('#userOpt').multiselect({
            nonSelectedText: 'Select User',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '400px',
            placeholder: 'Select User',
            maxHeight: 200,
            includeSelectAllOption: true,
            allSelectedText: 'All'
        });
        
        $('#fileuploadsubmit').click(function(){
            if( !$('#userOpt').val() ) {
                $(".invalid-userOpt").show();
                return false;
            }
            
        });
        
    });
    
function back(){
        var url = "<?php echo base_url(); ?>user/assigned_user_file";
        $(location).attr('href', url);
}
</script>
<?php
$this->load->view('footer');
?>

