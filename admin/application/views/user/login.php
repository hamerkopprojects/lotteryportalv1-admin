<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Sign In | <?php echo project_name; ?></title>
        <link rel="icon" href="../../favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>plugins/node-waves/waves.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>plugins/animate-css/animate.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/style2.css" rel='stylesheet' type='text/css' />
    </head>
    <body class="login-page">
        <div class="login-box">
            <br>
            <div class="logo">
                <a href="javascript:void(0);"><?php echo project_name; ?></a>
            </div>
            <div class="card">
                <div class="body">
                    <form id="sign_in"  action="<?php echo base_url(); ?>welcome/login" method="post">
                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="Password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">LOG IN</button>
                            </div>
                        </div>

                    </form>
                

                        <label><span style="color: red;text-align: center"><?php echo $this->session->flashdata('message'); ?><span></label>

                               
                                </div>
                                </div>
                                </div>

                                <!-- Jquery Core Js -->
                                <script src="<?php echo base_url(); ?>plugins/jquery/jquery.min.js"></script>

                                <!-- Bootstrap Core Js -->
                                <script src="<?php echo base_url(); ?>plugins/bootstrap/js/bootstrap.js"></script>

                                <!-- Waves Effect Plugin Js -->
                                <script src="<?php echo base_url(); ?>plugins/node-waves/waves.js"></script>

                                <!-- Validation Plugin Js -->
                                <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.js"></script>

                                <!-- Custom Js -->
                                <script src="<?php echo base_url(); ?>js/admin.js"></script>
                                <script src="<?php echo base_url(); ?>js/sign-in.js"></script>
                                </body>

                                </html>