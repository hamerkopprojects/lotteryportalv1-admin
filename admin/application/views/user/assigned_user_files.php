<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(10000);
    });
</script>
<section id="main-content">
    <section class="wrapper">
        <div class="table-agile-info">

            <!--
                        <div class="row">
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                        </div>-->



            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-success">
                        <strong><?php echo $this->session->flashdata('message'); ?></strong>
                    </div>
                <?php } ?>
                <div class="panel-heading">
                    <b style="color:#444">Uploded File Listing</b>
                    <button onclick="adduserfile()" class="btn btn-info pull-right" >Upload File</button>
                    <div class="panel-title pull-LEFT">
                        <input type="text" class="form-control" placeholder="Search" id="searchInput" style="background-color:#5bc0de" name="searchInput" onkeypress="return blockSpecialChar(event)" >

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
<!--                                <th>Assigned From</th>-->
<!--                                <th>Assigned To</th>-->
                                <th>Sent Date</th>
                                <th>Download File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            $i = 0;
                            foreach ($users_file as $userfiles):
                                $i++;
                                $originalDate = $userfiles['created_at'];
                                $newDate = date("d-m-Y g:i:s A", strtotime($originalDate));
                                $filepath = explode('@#@$@%*&', $userfiles['file_path']);
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $userfiles['file_name']; ?></td>
    <!--                            <td><?php echo $userfiles['firstname']; ?></td>-->
    <!--                                    <td><?php echo $userfiles['assigned_user']; ?></td>-->
                                    <td><?php echo $newDate; ?></td>
                                    <td>
                                        <select id="downloadfile" name="downloadfile" class="form-control select2 downloadfiles" style="width: 100%;">
                                            <option value="">Select a file</option>
                                            <?php
                                            if (!empty($userfiles['file_path'])) {
                                                foreach ($filepath as $key => $value) {
                                                    $filenamelast = end(explode('/', $value));
                                                    $filenameArray = explode('random', $filenamelast);
                                                    $filenamedisplay = $filenameArray[0];
                                                    if (sizeof($filenameArray) > 1) {
                                                        $filenamedisplay = $filenameArray[1];
                                                    }
                                                    ?>
                                                    <option value="<?php echo $value; ?>"><?php echo $filenamedisplay; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
    <!--                                    <td> <a href="<?php echo base_url() . 'user/download/' . $userfiles['user_files_id']; ?>" class="btn btn-primary">Download</a> </td>-->
                                    <td><button type="submit" class="btn btn-success"  onclick=" updatefile(<?php echo $userfiles['user_files_id']; ?>)">Update</button>
                                        <button type="submit" class="btn btn-danger"  onclick=" deletefile(<?php echo $userfiles['user_files_id']; ?>)">Delete</button></td>
    <!--                                    <td><a href="<?php echo base_url() . 'file/download' . $file->id; ?>" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt"></a></td>-->
                                <?php endforeach;
                                ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function updatefile(id)
        {
            var url = "<?php echo base_url(); ?>user/updatefiles/" + id;
            $(location).attr('href', url);
        }
        function deletefile(id)
        {
            var url = "<?php echo base_url(); ?>user/deletefiles/" + id;
            $(location).attr('href', url);
        }
        function adduserfile()
        {
            var url = "<?php echo base_url() ?>user/create_user_file";
            $(location).attr('href', url);
        }

    </script>
    <script>
        $(document).ready(function () {
            $(".downloadfiles").change(function () {
                var selectedFile = $(this).children("option:selected").val();
                if (selectedFile != '') {
                    var url = "<?php echo base_url(); ?>user/download?filepath=" + selectedFile;
                    $(location).attr('href', url);
                } else {
                    location.reload(true);
                }
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>