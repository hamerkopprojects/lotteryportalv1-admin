<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);
    });
</script>
<section id="main-content">

    <section class="wrapper">
        <div class="table-agile-info">

            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-success">
                        <strong><?php echo $this->session->flashdata('message'); ?></strong>
                    </div>
                <?php } ?>
                <div class="panel-heading" >
                    <b style="color:#444"> Users Listing</b>
                      <?php  $usertype = $this->session->userdata['user']['user_type'];?>
                    <?php if ( $usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                        <button  class="btn btn-info pull-right" onclick="adduser()">Create User</button>  <?php } ?>
                    <div class="panel-title pull-LEFT">
                        <input type="text" class="form-control" placeholder="Search" id="searchInput" style="background-color:#5bc0de" name="searchInput" onkeypress="return blockSpecialChar(event)" >
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th> No</th>
                                <th>Shop Code</th>
                                <th>Shop Name</th>
<!--                                <th>Last name</th>-->
                                <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                    <th>User name</th>
                                    <th>Password</th>
                                <?php } ?>
                                <th>User type</th>
                                <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                    <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <tr>
                                <td><?php echo 1; ?></td>
                                <td><b><?php echo $superadmins['shop_code']; ?><b></td>
                                    <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                <td><b><?php echo $superadmins['firstname'].' '.$superadmins['lastname']; ?><b></td>
<!--                                            <td><?php echo $superadmins['lastname']; ?></td>-->
                                            <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                                <td><?php echo $superadmins['username']; ?></td>

                                                <td><?php echo $superadmins['password']; ?></td>
                                            <?php } ?>
                                            <td><b><i><?php echo $superadmins['user_type']; ?></i></b></td>
                                             <?php } ?>
                                            <td>
                                                <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                                    <button type="submit" class="btn btn-success" onclick=" updateuser(<?php echo $superadmins['id']; ?>)">Update</button>
                                                <?php } ?>
                                                </tr>
                                                <?php
                                                $i = 1;
                                                foreach ($users as $user) {
                                                    $i++;
                                                    ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><b><?php echo $user->shop_code; ?><b></td>
                                                    <td><b><?php echo $user->firstname.' '.$user->lastname; ?><b></td>
<!--                                                                <td><?php echo $user->lastname; ?></td>-->
                                                                <?php if ($usertype  == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                                                    <td><?php echo $user->username; ?></td>

                                                                    <td><?php echo $user->password; ?></td>
                                                                <?php } ?>
                                                                <td><b><i><?php echo $user->user_type; ?></i></b></td>
                                                                <?php if ($usertype == "SUPERADMIN"||$usertype == "TECHNICIAN") { ?>
                                                                
                                                                    <td>
                                                                        <button type="submit" class="btn btn-success" onclick=" updateuser(<?php echo $user->id; ?>)">Update</button>
                                                                        <?php
                                                                        if ($user->user_type != 'SUPERADMIN'&&$user->user_type != 'TECHNICIAN') {
                                                                            if ($user->deleted_at == NULL) {
                                                                                ?>
                                                                                <button type="submit" class="btn btn-warning"  onclick=" deleteuser(<?php echo $user->id; ?>, 'block')">Block</button>
                                                                            </td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <button type="submit" class="btn btn"  onclick=" deleteuser(<?php echo $user->id; ?>, 'unblock')">Unblock</button>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            </tr>
                                                            </tbody>
                                                            </table>

                                                            </div>

                                                            </div>
                                                            <ul class="pagination">
                                                                <?php echo $pagination; ?>
                                                            </ul>
                                                            </div>
                                                            </section>
                                                            <!-- footer -->

                                                            <!-- / footer -->
                                                            </section>
                                                            <script type="text/javascript">
                                                                function updateuser(id)
                                                                {
                                                                    var url = "<?php echo base_url(); ?>user/edituser/" + id;
                                                                    $(location).attr('href', url);
                                                                }
                                                                function deleteuser(id, type)
                                                                {
                                                                    var url = "<?php echo base_url(); ?>user/deleteuser/" + id + "/" + type;
                                                                    $(location).attr('href', url);
                                                                }
                                                                function adduser() {
                                                                    var url = "<?php echo base_url() ?>user/create_users";
                                                                    $(location).attr('href', url);
                                                                }
                                                            </script>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("#searchInput").keyup(function () {
                                                                        var rows = $("#fbody").find("tr").hide();
                                                                        if (this.value.length) {
                                                                            var data = this.value.split(" ");
                                                                            $.each(data, function (i, v) {
                                                                                rows.filter(":contains('" + v + "')").show();
                                                                            });
                                                                        } else
                                                                            rows.show();
                                                                    });
                                                                });
                                                            </script>

                                                            <?php
                                                            $this->load->view('footer');
                                                            ?>

