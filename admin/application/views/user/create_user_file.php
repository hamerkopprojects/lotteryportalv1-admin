<?php
$this->load->view('header');


?>
<?php 
    $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata')); 
    $time_now = $dateTime->format("h:i:s A");
    $status = $this->Lottery_model->isUploadBlocked("FILE");
?>
<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
        <?php if($status == "true"){ ?>
            <div style="color:red;font-size: 20px; font-weight: bold;padding-bottom: 20px;padding-left: 200px;">Please Contact Admin to remove the time block</div>
        <?php } ?>
        <?php if($status == "false"){ ?>
                    <section class="panel">
                        <header class="panel-heading"><div class='time-frame'>
    <div id='date-part'></div>
    <div id='time-part'></div>
</div>
                            <b style="color:#444">File Upload</b>
                        </header>

                        <div class="panel-body">
                            <div class="position-center">
                                <div class='time-frame'>
    <div id='date-part'></div>
    <div id='time-part'></div>
</div>
                                <form role="form" id="userfile" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>user/user_file_upload">
                                    <div class="form-group">
<!--                                        <div style="font-size:20px;font-weight: bold;padding-bottom: 20px;float:right"><?php echo $time_now; ?></div>-->
                                        <label for="filename">Title</label>
                                        <input type="text" class="form-control" style="width: 400px;" id="filename" name="filename" required="required" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Add a user file</label>
                                        <span  class="btn btn-info btn-sm" id="Add_file">
                                            <span class="glyphicon glyphicon-plus-sign"></span>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <div id="filesarea" class="row">
                                            <div class="fileeach" style="float:left">
                                                <div class="form-group col-md-8">
                                                    <input type="file" name="file_name[]">
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="btn btn-info btn-sm deletefile">
                                                        <span class="glyphicon glyphicon-minus-sign">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="usertype">Send Files To: </label>
                                        <br>
                                        <select id="userOpt" name="userOpt[]" multiple class="form-control" required="required">


                                            <?php
                                            foreach ($username as $usernames) {
                                                ?>
                                                <option  value="<?php echo $usernames->id; ?>"><?php echo $usernames->username; ?> (<?php echo $usernames->user_type; ?>)</option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                        <div class="invalid-userOpt" style="color:red;display: none">
                                            Please select shops or agents.
                                        </div>
                                    </div>
                            <?php if($status == "false"){ ?>
                                    <div id="buttons">
                                        <button id="fileuploadsubmit" type="submit"  class="btn btn-info" style="float:left">Submit</button>
                                        <button type="submit" onclick="back()" class="btn btn-info" style="float:right">Back</button>
                                    </div>
                            <?php } ?>
                                </form>
                            </div>
                        </div>
                    </section>
            <?php } ?>
                </div>
            </div>
        </div>
       
    </section>
</section>
<script>
    $(document).ready(function () {
        $("#Add_file").on("click", function () {
            $("#filesarea").append("<div class=\"fileeach\" style=\"float:left\">\n\
                                       <div class=\"form-group col-md-8\">\n\
                                            <input type=\"file\" name=\"file_name[]\">\n\
                                        </div>\n\                                        \n\
                                        <div class=\"col-md-4\">\n\
                                            <span class=\"btn btn-info btn-sm deletefile\">\n\
                                                <span class=\"glyphicon glyphicon-minus-sign\">\n\
                                                </span>\n\                                        \n\
                                            </span>\n\
                                        </div>\n\
                                        <div class='clear'></div>\n\
                                    </div>");
        });
        $("#filesarea").on("click", ".fileeach .deletefile", function () {
            $(this).parents('.fileeach').remove();
            var len = $(".noticeeach").length;
        });
        $('#userOpt').multiselect({
            nonSelectedText: 'Select User',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '400px',
            placeholder: 'Select User',
            maxHeight: 200,
            includeSelectAllOption: true,
            allSelectedText: 'All'
        });

        $('#fileuploadsubmit').click(function () {
            if (!$('#userOpt').val()) {
                $(".invalid-userOpt").show();
                return false;
            }

        });
    });
    function back() {
        var url = "<?php echo base_url(); ?>user/assigned_user_file";
        $(location).attr('href', url);
    }
</script>
<?php $this->load->view('footer');
?>