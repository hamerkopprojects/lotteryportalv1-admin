<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);
    });
</script>

<section id="main-content">

    <section class="wrapper">
        <div class="table-agile-info">

            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-success">
                        <strong><?php echo $this->session->flashdata('message'); ?></strong>
                    </div>
                <?php } ?>
                <div class="panel-heading" >
                    <b style="color:#444"> Time Listing</b>
                    <button class="btn btn-primary" style="float: right"type="button" onclick="addtime()">Add Time</button>

                </div>
                <div>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th> No</th>
                                <th>Type</th>
                                <th>From time</th>
                                <th>To time</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            $i = 0;
                            foreach ($time as $timing) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><b><?php echo $timing->type; ?></b></td>
                                    <td><?php echo $timing->from_time; ?></td>
                                    <td><?php echo $timing->to_time; ?></td>
                                    <td><?php echo $timing->is_active; ?></td>
                                    <td><button class="btn btn-primary" style="background-color:#25A7DB;" onclick="timeedit(<?php echo $timing->id; ?>)" value="companyedit" title="Edit">Edit</button></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </section>

</section>
<script type="text/javascript">


    function timeedit(id) {
        var url = "<?php echo base_url() ?>user/edit_time/" + id;
        $(location).attr('href', url);
    }
    function addtime()
    {
        var url = "<?php echo base_url() ?>user/timesettings";
        $(location).attr('href', url);
    }

</script>
<?php
$this->load->view('footer');
?>
</html>




