<head>
    <title><?php echo project_name; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" >
    <link href="<?php echo base_url(); ?>css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet"/>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/font.css" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery.multiselect.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/morris.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/monthly.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/unsold-loader.css">
    <script src="<?php echo base_url(); ?>js/jquery2.0.3.min.js"></script>
    <script src="<?php echo base_url(); ?>js/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>js/morris.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/timepicker/bootstrap-timepicker.min.css">
    
     <link href="<?php echo base_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <style>
        .blink{
            padding-left: 200px;
            text-align: center;
        }
        .ticketconf {
            font-size: 25px;
            color: white;
            animation: blink 1s linear infinite;
        }
        @keyframes blink{
            0%{opacity: 0;}
            50%{opacity: .5;}
            100%{opacity: 1;}
        }



        @media only screen and (max-width: 800px) {
            .blink {
                padding-left: 200px;
                text-align: center;
            }

            .ticketconf {
                font-size: 25px;
                color: white;
                animation: blink 1s linear infinite;
            }

            .resultlink {
                float:left;
                color:white;
                margin-top: 27px;
                margin-left: 54px

            }
        }

        @media only screen and (max-width: 360px) {
            .blink {
                text-align: center;
            }

            .ticketconf {
                font-size: 15px;
                color: white;
                animation: blink 1s linear infinite;
            }

            .resultlink {
                float:left;
                color:white;
                margin-left: 54px

            }
        }
    </style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/admin-responsive.css" >
</head>
<body style="font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif">