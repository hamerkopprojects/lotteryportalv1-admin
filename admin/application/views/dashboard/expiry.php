<?php 
 $host = $_SERVER['HTTP_HOST'];

            if (isset($_SERVER['HTTPS']) &&
                ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
              $protocol = 'https://';
            }
            else {
              $protocol = 'http://';
            }

            if($host == 'localhost'){
                $url_starts = $protocol.$host.'/lotteryportalv1';
            }else if($host == 'hamerkopinfotech.com'){
                $url_starts = $protocol.$host.'/lotterymaster';
            } else { 
                $url_starts = $protocol.$host;
            }
            ?>

<html>
<style>
body, html {
  height: 100%;
  margin: 0;
}

.bgimg {
  background-image: url('<?php  echo $url_starts; ?>/img/expiry.jpg');
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-family: "Courier New", Courier, monospace;
  font-size: 25px;
}

.topleft {
  position: absolute;
  top: 0;
  left: 16px;
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

hr {
  margin: auto;
  width: 40%;
}
</style>
<body>

<div class="bgimg">
  <div class="topleft">
   
  </div>
  <div class="middle">
    <h1>EXPIRED</h1>
    <hr>
    <p>CONTACT WEB ADMINISTRATOR +91-9447333079</p>
  </div>
 
</div>

</body>
</html>
<?php

?>

