<?php
if ($_GET['platform'] != 'app') {
    $this->load->view('header');
} else {
    $this->load->view('header-agent');
}
?>
<style>
    @media only screen and (max-width: 600px) {
        iframe {
            width: 100%;
            height: 250%;
        }
    }
    @media only screen and (min-width: 600px) {
        iframe {
            width: 100%;
            height: 180%;
        }
    </style>
    <style>table 
        {  font-family: arial, sans-serif;  border-collapse: collapse;  width: 100%; font-weight: bolder;}
        td, th {  border: 1px solid #dddddd;    padding: 8px;}
        tr.break td {
            height: 10px;
            tr { width:100%; }
        }
        /*tr:nth-child(even) { background-color: #dddddd;}*/
    </style> 
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="panel-body">
                    <div class="">

                        <div class="agileits-box">
                            <header class="agileits-box-header clearfix"> 

                                <?php if ($_GET['platform'] == 'web') { ?>
                                    <div  class="col-md-4" ><input type="button" class="btn btn-info" style="font-weight:bolder;font-size: 20px;" value="Go Back" onclick="goback()"></div>
                                    <!--                                <div class="panel-heading">-->
                                <?php } ?>
<!--                                <table style="background:#999">
                                    <tr align="center">
                                        <td><h3><b style="color:#ffff">RESULT DETAILS</b></h3></td>
                                    </tr>
                                </table>-->
                                <!--                                </div>-->
                        </div>

                        </header><br>

                        <div>
                            <?php if ($_GET['platform'] == 'web') { ?>                       
                                <div class="row">
                                    <div class="col-md-2">
                                        <b>Select Date:</b> <input type="text" class="form-control" id="dateofunsold" name="dateofunsold"  value="<?php echo $date_of_unsold; ?>" placeholder="Select a date" onchange="dateselect();">
                                    </div>
                                    <div class="col-md-2">
                                        <b>Select Ticket:</b> 
                                        <select name="tickets_id" id="ticketname" class="form-control select2" style="width: 100%;">
                                            <?php foreach ($ticketlist as $key => $value) { ?>
                                                <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id']; ?>"><?php echo $value['ticket_name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>

                                    </div>


                                </div>
                                <br>
                            <?php } ?>
                            <?php if ($result_published == "") { ?>
                                <table>
                                    <tr align="center">
                                        <td style="background-color: black;font-size: 20px;color:#ffff;"><?php echo $drawcode; ?></td>
                                        <td style="background-color: black;font-size: 20px;color:#ffff;" colspan="2"><?php echo $ticket_name; ?></td>
                                        <td style="background-color: black;font-size: 20px;color:#ffff;"><?php echo date('d-m-Y', strtotime($_GET['date'])); ?></td>
                                    </tr>
                                </table>
                                <?php
                                $c = 1;
                                $p3 = 1;
                                $p4 = 1;
                                $p5 = 1;
                                $p6 = 1;
                                $p7 = 1;
                                $p8 = 1;
                                $p9 = 1;
                                ?>
                                <table>
                                    <?php foreach ($wontickets as $key => $wonticket) { ?>
                                        <?php foreach ($wonticket as $k => $val) { ?>
                                            <?php 
                                            
                                            if (strpos($key, '-') !== false) {
                                                $keys = array();
                                                $keys = explode('-', $key);
                                                $key = $keys[0];
                                                $amount = ' '.$keys[1].'/-';
                                            }
                                            if ($key == "1st Prize") { ?>
                                                <tr align="center">
                                                    <td style="background-color: #47d147;font-size: 25px;color:#ffff;" colspan="2"><?php echo $key.'-'.$amount; ?></td>
                                                    <td style="font-size: 25px;" colspan="2"><?php echo $val; ?></td>
                                                </tr>
                                            </table>
                                            <table>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                            <?php if ($key == "2nd Prize") { ?>

                                                <tr style="height:1em;" align="center">
                                                    <td style="background-color: #33d6ff;color:#ffff;font-size:20px;" colspan="2"><?php echo $key.'-'.$amount; ?></td>
                                                    <td style="font-size: 20px;" colspan="2"><?php echo $val; ?></td>
                                                </tr>
                                            </table>
                                            <table>
                                            <?php } ?>
                                            <?php if ($key == "Consolation Prize") { ?>

                                                <tr style="height:1em;" align="center">
                                                    <td style="background-color: #33d6ff;color:#ffff;font-size:20px;" colspan="2"><?php echo $key.'-'.$amount; ?></td>
                                                </tr>
                                                <tr align="center">
                                                    <td style="word-wrap: break-word;width:20%"><?php echo $val . '-' . $constallationcode; ?></td>
                                                </tr>
                                            </table>
                                            <table>
                                            <?php } ?>

                                            <?php
                                            if ($key == "3rd Prize") {

                                                $vallength = strlen($val);
                                                if ($vallength == 9 && $key == "3rd Prize") {
                                                    $rowcells = 2;
                                                    $colspan = 3;
                                                } else if ($vallength == 4 && $key == "3rd Prize") {
                                                    $rowcells = 5;
                                                    $colspan = 1;
                                                }
                                                // echo $p3 % 4; 
                                                if ($p3 == 1) {
                                                    ?>
                                                    <tr style="height:1em;" align="center" >
                                                        <td style="background-color:#ff4d4d;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                <?php } ?>
                <?php if ($p3 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr align="center">
                <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p3++;
                                                } ?>  
                                                <?php
                                                if ($key == "4th Prize") {
                                                    $vallength = strlen($val);
                                                    if ($vallength == 9 && $key == "4th Prize") {
                                                        $rowcells = 2;
                                                        $colspan = 3;
                                                    } else if ($vallength == 4 && $key == "4th Prize") {
                                                        $rowcells = 5;
                                                        $colspan = 1;
                                                    }
                                                    if ($p4 == 1) {
                                                        ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color:#ffc266;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if ($p4 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                                    <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p4++;
                                                } ?>             

                                                <?php
                                                if ($key == "5th Prize") {
                                                    $vallength = strlen($val);
                                                    if ($vallength == 9 && $key == "5th Prize") {
                                                        $rowcells = 2;
                                                        $colspan = 3;
                                                    } else if ($vallength == 4 && $key == "5th Prize") {
                                                        $rowcells = 5;
                                                        $colspan = 1;
                                                    }
                                                    if ($p5 == 1) {
                                                        ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color:#1a53ff;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                <?php } ?>
                                                    <?php if ($p5 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                                    <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p5++;
                                                } ?> 

                                                <?php
                                                if ($key == "6th Prize") {
                                                    $vallength = strlen($val);
                                                    if ($vallength == 9 && $key == "6th Prize") {
                                                        $rowcells = 2;
                                                        $colspan = 3;
                                                    } else if ($vallength == 4 && $key == "6th Prize") {
                                                        $rowcells = 5;
                                                        $colspan = 1;
                                                    }
                                                    if ($p6 == 1) {
                                                        ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color:#ff66b3;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if ($p6 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                                    <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p6++;
                                                } ?>            

                                                <?php
                                                if ($key == "7th Prize") {
                                                    $vallength = strlen($val);
                                                    if ($vallength == 9 && $key == "7th Prize") {
                                                        $rowcells = 2;
                                                        $colspan = 3;
                                                    } else if ($vallength == 4 && $key == "7th Prize") {
                                                        $rowcells = 5;
                                                        $colspan = 1;
                                                    }
                                                    if ($p7 == 1) {
                                                        ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color:#00cc99;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if ($p7 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                                    <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p7++;
                                                } ?>      
                                                <?php
                                                if ($key == "8th Prize") {
                                                    $vallength = strlen($val);
                                                    if ($vallength == 9 && $key == "8th Prize") {
                                                        $rowcells = 2;
                                                        $colspan = 3;
                                                    } else if ($vallength == 4 && $key == "8th Prize") {
                                                        $rowcells = 5;
                                                        $colspan = 1;
                                                    }
                                                    //echo $p3 % 4; 
                                                    if ($p8 == 1) {
                                                        ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color:#0099cc;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if ($p8 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                                    <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                                                    <?php $p8++;
                                                } ?>             
            <?php
            if ($key == "9th Prize") {
                $vallength = strlen($val);
                if ($vallength == 9 && $key == "9th Prize") {
                    $rowcells = 2;
                    $colspan = 3;
                } else if ($vallength == 4 && $key == "9th Prize") {
                    $rowcells = 5;
                    $colspan = 1;
                }
                if ($p9 == 1) {
                    ?>
                                                    <tr style="height:1em;" align="center">
                                                        <td style="background-color: #ff9933;color:#ffff;font-size:20px;" colspan="6"><?php echo $key.'-'.$amount; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($p9 % $rowcells == 1) { ?>
                                                    </tr>
                                                    <tr>
                                        <?php } ?>
                                                    <td colspan="<?php echo $colspan; ?>" align="center"><?php echo $val; ?></td>

                <?php $p9++;
            } ?>             



                                <?php } ?>
        <?php if ($c % 4 != 0) { ?>
                                            </tr>
        <?php } ?>
    <?php } ?>
                                </table>
                            </div>
<?php } else { ?>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-6" style="color:red;font-size: 25;font-weight: bolder;"><?php echo $result_published; ?></div>
                                <div class="col-md-2"></div>

                            </div>
<?php } ?>
                    </div>
                </div>
            </div>

        </section>

    </section>  
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000);
            $('.alert-warning').hide();
            $("#dateofunsold").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
        function dateselect() {
            var url = "<?php echo base_url(); ?>dashboard/results?platform=web&&ticket=" + $('#ticketname').val() + "&&date=" + $('#dateofunsold').val();
            $(location).attr('href', url);
        }
        function goback() {
            var url = "<?php echo base_url(); ?>dashboard/home";
            //  window.open(url, '_blank');
            $(location).attr('href', url);
        }
        $("#ticketname").change(function () {
            var url = "<?php echo base_url(); ?>dashboard/results?platform=web&&ticket=" + $('#ticketname').val() + "&&date=" + $('#dateofunsold').val();
            $(location).attr('href', url);
        });
    </script>

<?php
$this->load->view('footer');
?>








