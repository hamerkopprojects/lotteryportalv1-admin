<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-warning').hide();
    });
    function updatedomain() {
        var valid = true;
        var myarray = [];
        $('.form-control').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });

        if (myarray.length === 0) {
            $("#submitt").submit();
        }
    }
    function back() {
        var url = "<?php echo base_url(); ?>dashboard/clientexpiry_details";
        $(location).attr('href', url);
    }
</script>

<section id="main-content">
  <?php  $usertype = $this->session->userdata['user']['user_type'];?>
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning">
                        <strong>Warning!</strong> Please fill all the fields.
                    </div>
                    <section class="panel">
                        <header class="panel-heading">
                            <b style="color:#444">DOMAIN UPDATE</b>

                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" id="createuser" method="post" action="<?php echo base_url() ?>dashboard/update_domains">
                                    <?php
                                foreach($edit_details as $details){
//                                     echo '<pre>'; print_r($edit_details); exit;
                                    ?>
                                     <div class="form-group">
                                        <label for="protocol">Protocol</label>
                                        <input type="text" class="form-control" id="protocol" name="protocol" required="required" value="<?php echo $details['protocol']; ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label for="domainname">Domain Name</label>
                                        <input type="text" class="form-control" id="domainname" name="domain" required="required"value="<?php echo $details['domain']; ?>"  >
                                    </div>
                                    <div class="form-group">
                                        <label for="agencyname">Agency Name</label>
                                        <input type="text" class="form-control" id="agencyname" name="name"required="required" value="<?php echo $details['name']; ?>" >
                                    </div>
                                   
                                    <div class="form-group">
                                        <label for="registrationdate">Registration Date</label>
                                       <input type="text" class="form-control" id="registrationdate" name="date" value="<?php echo date("d-m-Y", strtotime($details['date'])); ?>" placeholder="Select a date">
                                    </div>
                                    <div class="form-group">
                                        <label for="expirydate">Expiry Date</label>
                                         <input type="text" class="form-control" id="expirydate" name="expiry" value="<?php echo date("d-m-Y", strtotime($details['expiry'])); ?>" placeholder="Select a date">
                                    </div>
                                       <div class="form-group">
                                        <label for="client">client</label>
                                        <input type="text" class="form-control" id="client" name="client" value="<?php echo $details['client']; ?>"  required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php echo $details['phone']; ?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="status">Status</label>
                                        <select class="form-control m-bot15" id="status" name="is_enabled"  required="required" value="<?php echo $details['is_enabled']; ?>" >
                                          <option <?php echo ($details['is_enabled'] == 1) ? 'selected="selected"' : ''; ?> value=1>Enable</option>
                                          <option <?php echo ($details['is_enabled'] == 0) ? 'selected="selected"' : ''; ?> value=0>Disable</option>
                                        </select>
                                        <span class="msg" id="msg" style="color:red"></span>
                                    </div>
                                     <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $details['address']; ?>"  >
                                    </div>
                                     <div class="form-group">
                                        <label for="comment">Comment</label>
                                        <input type="text" class="form-control" id="comment" name="comment"  value="<?php echo  $details['comment']; ?>" >
                                    </div>
                                    <div class="form-group">
                                        <span class="error" style="color:red"></span>
                                    </div>
                                <?php } ?>
                                    <div id="buttons">
<!--                                     <button type="submit" onclick="savedomain()" class="btn btn-info" id="submitt" style="float:left">Submit</button>
                                        <button type="submit" onclick="back()" class="btn btn-info" style="float:right">Back</button>-->
                                    <button type="submit" onclick="updatedomain()"id="submitt" class="btn btn-info">Update</button>
                                    <input type="hidden" name="id" value="<?php echo $details['id']; ?>">
                                    </div>
                                </form>
                             <button style="float: right;margin-top: -50px"type="button" onclick="back()" class="btn btn-info">Back</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

</section>

<!--<script type="text/javascript">
      $(document).ready(function(){
        var date_input=$('input[name="registrationdate"],input[name="expirydate"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'dd-mm-yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>-->
 <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000);
            $('.alert-warning').hide();
            $("#registrationdate").datepicker({
                dateFormat: 'dd-mm-yy'
            });
             $("#expirydate").datepicker({
                dateFormat: 'dd-mm-yy'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });

        });
    </script>
<?php
$this->load->view('footer');
?>