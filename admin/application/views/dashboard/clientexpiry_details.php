<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);
    });
</script>
<section id="main-content">

    <section class="wrapper">
        <div class="table-agile-info">

            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-success">
                        <strong><?php echo $this->session->flashdata('message'); ?></strong>
                    </div>
                <?php } ?>
                <div class="panel-heading" >
                    <b style="color:#444">Expiry Details</b>
                    <button  class="btn btn-info pull-right" onclick="adddomain()">Create domain</button>

                    <div class="panel-title pull-LEFT">
                        <input type="text" class="form-control" placeholder="Search" id="searchInput" style="background-color:#5bc0de" name="searchInput" onkeypress="return blockSpecialChar(event)" >
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th> No</th>
                                <th> Protocol</th>
                                <th>Domain Name</th>
<!--                                <th>Agency Name</th>-->
                                <th>Registration Date</th>
                                <th>Expiry Date</th>
                                <th>Days Left</th>
                                <th>client</th>
                                <th>Phone</th>
<!--                                 <th>Status</th>-->
<!--                                <th>Address</th>
                              <th>Comment</th>-->
                              <th>Updated Date</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
        <?php
        $i = 1;
        foreach ($domain_details as $details) {
            $i++;
            ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><b><?php echo $details['protocol']; ?><b></td>
<!--                    <td><?php echo $details['domain']; ?></td>-->
                    <td><?php $reg_date = date("d-m-Y", strtotime($details['date']));
                              $last_date= date("d-m-Y", strtotime($details['expiry']));
                               $registration_date=date_create($reg_date );
                               $expirydate=date_create($last_date);
                               $date_diff = date_diff($registration_date, $expirydate);
                               $difference = $date_diff->days;
                             //  echo $difference;
                               if ($difference <=10)
                                    { ?>
                                      <span style="color:red;font-weight:bold;"><?php echo  $details['name'];
                                    }  else {?>
                                      <span style="color:blue;font-weight:bold;"><?php echo $details['name'];

                                    }?></td>
                    <td><b><?php echo date("d-m-Y", strtotime($details['date']));
                            ?></b></td>
                    <td>
                        <?php  
                                if ($difference <=10)
                                    { ?>
                                      <span style="color:red;font-weight:bold;"><?php echo date("d-m-Y", strtotime($details['expiry']));
                                    }  else {?>
                                      <span style="color:blue;font-weight:bold;"><?php echo date("d-m-Y", strtotime($details['expiry']));

                                    }?>
                    </td>
                     <td><?php if ($difference <=10)
                                    { ?>
                                      <span style="color:red;font-weight:bold;"><?php echo  $difference;
                                    }  else {?>
                                      <span style="color:blue;font-weight:bold;"><?php echo $difference;
                                    }?></td>
                     <td><?php echo $details['client']; ?></td>
                     <td><?php echo $details['phone']; ?></td>
                     <td><?php echo date("d-m-Y", strtotime($details['updated_at']))?></td>
                     <td>
                    <button type="submit" class="btn btn-success" onclick=" updatedomain(<?php echo $details['id']; ?>)">Update</button>
                    <?php
                        if ($details['is_enabled'] == 0) {
                            ?>
                            <button type="submit" class="btn btn"  onclick=" deletedomain(<?php echo $details['id']; ?>, 'unblock')">Unblock</button>

                        </td>
                        <?php
                    } else {
                        ?>
                         <button type="submit" class="btn btn-warning"  onclick=" deletedomain(<?php echo $details['id']; ?>, 'block')">Block</button>
                        <?php
                    }
                
                ?>
                <?php } ?>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
                    </section>
                    </section>
                    <script type="text/javascript">
                        function updatedomain(id)
                        {
                            var url = "<?php echo base_url(); ?>dashboard/edit_domains/" + id;
                            $(location).attr('href', url);
                        }
                        function deletedomain(id, type)
                        {
                            var url = "<?php echo base_url(); ?>dashboard/delete_domains/" + id + "/" + type;
                            $(location).attr('href', url);
                        }
                        function adddomain() {
                            var url = "<?php echo base_url() ?>dashboard/create_domains";
                            $(location).attr('href', url);
                        }
                    </script>
                    <script>
                        $(document).ready(function () {
                            $("#searchInput").keyup(function () {
                                var rows = $("#fbody").find("tr").hide();
                                if (this.value.length) {
                                    var data = this.value.split(" ");
                                    $.each(data, function (i, v) {
                                        rows.filter(":contains('" + v + "')").show();
                                    });
                                } else
                                    rows.show();
                            });
                        });
                    </script>

                    <?php
                    $this->load->view('footer');
                    ?>

