<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        <?php
        $usertype = $this->session->userdata['user']['user_type'];

        if ($usertype == 'SUPERADMIN' || $usertype == 'ADMIN'|| $usertype == 'TECHNICIAN') {
            ?>       

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>
                                SHOPS
                            </h3>
                            <p>
                                Live Details
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-folder-open-o"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/shoplist" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                DC
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                DC Details of each shop<?php //echo $dctotal; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>Dcmanagement/dc_details" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>
                                WINNING
                            </h3>
                            <p>
                                Winning from unsold<?php //echo $winningtotal; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-database"></i>
                        </div>
                        <a href="<?php echo base_url() ?>unsold/listunsoldwinning/<?php echo date('Y-m-d'); ?>/RETAIL" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                UNSOLD
                            </h3>
                            <p>
                                Wholesale and Retail Unsold<?php //echo $unsoldtotal; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-money"></i>
                        </div>
                        <a href="<?php echo base_url() ?>unsold/listunsoldconsolidated/<?php echo date('Y-m-d'); ?>/RETAIL" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box" style="background-color: grey !important;color:white">
                        <div class="inner">
                            <h3>
                                FILES
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                Manage Files
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>user/received_user_file" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->                                
<!--                <div class="col-lg-3 col-xs-6">
                     small box 
                    <div class="small-box bg-aqua" style="background-color: #53d769 !important;color:white">
                        <div class="inner">
                            <h3>
                                PROFIT
                            </h3>
                            <p>
                                <?php echo $profit; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-folder-open-o"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/shoplist" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div> ./col -->
<!--                <div class="col-lg-3 col-xs-6">
                     small box 
                    <div class="small-box bg-maroon">
                        <div class="inner">
                            <h3>
                                LOSS
                            </h3>
                            <p>
                                <?php echo $loss; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-money"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/shoplist" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div> ./col        -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>
                                RESULTS
                            </h3>
                            <p>
                               Get winning results
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/results?platform=web" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->                 
            </div><!-- /.row -->        
        <?php } ?>
    
     <?php
        if ($usertype == 'SHOP') {
            ?>       

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                DC
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                <?php echo $dctotal; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>Dcmanagement/dc_details" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box" style="background-color: grey !important;color:white">
                        <div class="inner">
                            <h3>
                                FILES
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                Manage Files
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>user/received_user_file" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                    
              </div><!-- ./col --> 
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>
                                RESULTS
                            </h3>
                            <p>
                               Get winning results
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/results?platform=web" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->               
              
              
            </div><!-- /.row -->
        <?php } ?>
  <?php
        if ($usertype == 'AGENT') {
            ?>       

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                DC
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                <?php echo $dctotal; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>Dcmanagement/dc_details" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->
                                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box" style="background-color: grey !important;color:white">
                        <div class="inner">
                            <h3>
                                FILES
    <!--                                        <sup style="font-size: 20px">%</sup>-->
                            </h3>
                            <p>
                                Manage Files
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa  fa-credit-card"></i>
                        </div>
                        <a href="<?php echo base_url() ?>Dcmanagement/dc_details" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col --> 
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>
                                RESULTS
                            </h3>
                            <p>
                               Get winning results
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <a href="<?php echo base_url() ?>dashboard/results?platform=web" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div><!-- ./col -->                 
            </div><!-- /.row -->
        <?php } ?>   
   <?php if(isset($_GET ['expire_on']) && isset($_GET ['daysleft'])){ ?>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog " style="background-color: white;">
    <div class="modal-header ">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h3 ><b>Expiry Notification</b></h3>
    </div>
    <div class="modal-body">
        <div>
            <p style=" font-size: 15px"><b> Your Application will Expire on <span style="color:red;font-size:20px;"><?php echo date('d-m-Y', strtotime($_GET ['expire_on'])) ?></span>
             and <span style="color:red;font-size:20px;"><?php echo $_GET ['daysleft']?></span> Days Left.<br>
             Please Contact Admin to Renew <span style="font-size:20.5px;">(+91-9447333079).</span>
        </p>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#" class="btn" onclick = "$('#myModal').modal('hide')">OK</a>
    </div>
  </div>
   </div>
   <?php } ?>
    </section>
</section>
<script type="text/javascript">
    $(window).on('load',function(){
        var expire_on = '<?php $_GET ['expire_on']; ?>';
        var daysleft = '<?php $_GET ['daysleft']; ?>';
        if(typeof(expire_on) != "undefined" && typeof(daysleft) != "undefined")
        {
        $('#myModal').modal('show');
         }
    });
</script>
<?php
$this->load->view('footer');
?>