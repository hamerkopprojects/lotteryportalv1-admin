<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        
        <div class="table-agile-info">
            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                  <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
                <?php } ?>
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Please fill all the fields.
                </div>
                <div class="panel-heading">

                    <div class="row" >
                        <div class="col-md-8" ><b style="color:#444">Live Details - <?php echo $date; ?></b></div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-4" ><b>Select Date:</b> <input type="text" class="form-control" id="dateofdc" name="dateofdc"  value="<?php echo $date; ?>" placeholder="Select a date" onchange="dateselect();" ></div>
                </div>
<!--                     <div class="pull-LEFT" style="margin-left: 20px;margin-top: 30px;">
                         <b>Select Date:</b> <input type="text" class="form-control" id="dateofdc" name="dateofdc"  value="<?php echo $date; ?>" placeholder="Select a date" onchange="dateselect();" >
                    </div>-->
                <div class="table-responsive">
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th width="5%">No</th>
<!--                                <th width="10%">Shop Code</th>-->
                                <th width="15%">Shop Name</th>
                                <th width="10%">Shop Code</th>
<!--                                <th width="10%">Live Details</th>-->
                            </tr>
                        </thead>
                        <tbody id="fbody" >
                     
                            <?php
                            $i = 0;
                            foreach ($details as $key) {
                                $i++;
                                $date = $key['date'];
                                $newDate = $date;
                                $newDate = date("d-m-Y ", strtotime($newDate));
                                $color = ($i%2 == 0)?"#ddede0":"#ffff";
                                
                                if(isset($_GET['date'])){
                                    $date = explode('/', $_GET['date']);
                                    $date  = date('Y-m-d', strtotime(implode('-', array_reverse($date))));
                                } else {
                                    $date = date('Y-m-d');
                                }
                                $totaldc = $this->Ticket_model->get_unsold_from_dc($key['shop_code'], $date);
//                                echo '<pre>'; print_r($unsold); exit;
                                ?>
  
                                <tr style ="background-color: <?php echo $color; ?>!important;width:100%!important">
                                    <td width="5%"><?php echo $i; ?></td>
<!--                                    <td width="10%"><?php echo $key['shop_code']; ?></td>-->
                                    <td width="15%" style="font-weight:bolder;font-size: 15px"><b><?php echo $key['shopname']; ?></b></td>
                                    <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #b3b3b3!important;" value="<?php echo $key['shop_code']; ?>" onclick="livestatements(<?php echo $key['shop_code']; ?>,'LIVE')"></td>
                                    <td width="10%"></td>
                                    <td width="10%"></td>
                                </tr>
                                <tr style ="background-color: <?php echo $color; ?>!important;width:100%!important">
                                    <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #94C647!important;color:white;" value="LIVE" onclick="livestatements(<?php echo $key['shop_code']; ?>, 'LIVE')"></td>
                                    
                                        <?php if($totaldc > 0) {?>
                                        <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #FA5858!important;color:white;" value="UNSOLD" onclick="dc(<?php echo $key['shop_code']; ?>, 'UNSOLD')">
                                        <span style="color:red; font-weight:bolder;">(<?php echo $totaldc; ?>)</span>
                                        <?php } else { ?>
                                        <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #FA5858!important;color:white;" value="UNSOLD" onclick="livestatements(<?php echo $key['shop_code']; ?>, 'UNSOLD')">
                                        <?php } ?>
                                    </td>
                                    <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #EF954E!important;color:white;" value="REPORT" onclick="livestatements(<?php echo $key['shop_code']; ?>, 'REPORT')"> </td>
                                    <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #00ccff!important;color:white;" value="UNSOLD RETAIL" onclick="livestatements(<?php echo $key['shop_code']; ?>, 'UNSOLD_RETAIL')"> </td>
                                    <td width="10%"><input type="button" class="btn" style="font-weight:bolder;font-size: 15px;background-color: #00e6ac!important;color:white;" value="UNSOLD REPORT" onclick="livestatements(<?php echo $key['shop_code']; ?>, 'UNSOLD_REPORT')"> </td>
                                </tr>
                                    
                            <?php
                            }
                            ?>                          
                            
                        </tbody>
                    </table>

                 
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function dc(shopcode, type){
            var url = "<?php echo base_url(); ?>dc?s="+shopcode+"&&b=0&&user=admin&&date="+$('#dateofdc').val()+"&&type="+type;
            window.open(url, 'blank');
         //   $(location).attr('href', url);        
        }
        function livestatements(shopcode, type){
            var url = "<?php echo base_url(); ?>dashboard/livestatements?shopcode="+shopcode+"&&date="+$('#dateofdc').val()+"&&type="+type;
            window.open(url, '_self');
         //   $(location).attr('href', url);        
        }
        function dateselect(){
            var url = "<?php echo base_url(); ?>dashboard/shoplist?date="+$('#dateofdc').val();
            //var url = "<?php echo base_url(); ?>dashboard/shoplist/"+$('#dateofdc').val();
            $(location).attr('href', url);        
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(2000); 
            $('.alert-warning').hide();
            $("#dateofdc").datepicker({
                dateFormat: 'dd/mm/yy'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>