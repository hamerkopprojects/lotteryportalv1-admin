<?php
if($platform != 'app'){
 $this->load->view('header');
} else {
 $this->load->view('header-agent');  
}
?>
<style>
  @media only screen and (max-width: 600px) {
  iframe {
    width: 100%;
    height: 250%;
  }
}
  @media only screen and (min-width: 600px) {
  iframe {
    width: 100%;
    height: 180%;
  }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="panel-body">
                 <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                    <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('error')) { ?> 
                <div class="alert alert-danger">
                    <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php } ?>
                <div class="col-md-12 w3ls-graph">

                    <div class="">
                        <?php if($platform == 'web'){ ?>
                        <div class="agileits-box">
                            <header class="agileits-box-header clearfix">
                                <div class="col-md-4" ><input type="button" class="btn btn-info" style="font-weight:bolder;font-size: 20px;" value="Go Back" onclick="shoplists()"></div>
                                
                                <div class="col-md-4" >
                                <h3>Todays Details</h3>
                                </div>
                                <div class="col-md-2" ><input type="button" class="btn btn-success" style="font-weight:bolder;font-size: 20px;" value="Print" onclick="printpage()"></div>
                                </div><br>
                        <?php } ?>
                                
                                 
                            </header><br>
                          <?php 
                                 $file = $url;
                                 $file_headers = @get_headers($file);
                                 if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { ?>
                            <table align="center">       
                                <tr>
                                <td style="color:red;text-align:center;font-weight:bolder;font-size: 25px"><?php echo $message;?></td>
                                </tr>
                            </table>
                           <?php   } else { ?>
                                     <iframe src="<?php echo $url; ?>"  scrolling="no" frameBorder="0">
                            </iframe>
                            <?php   }   ?>                   
                            <div class="agileits-box-body clearfix">
                                <div id="hero-area"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

</section>
<script>
    function shoplists(){
        var url = "<?php echo $goback; ?>";
      //  window.open(url, '_blank');
        $(location).attr('href', url);        
    }
        function printpage() {
            var url = "<?php echo base_url(); ?>dashboard/printlivestatements?shopcode=<?php echo $_GET['shopcode']; ?>&&date=<?php echo $_GET['date']; ?>&&type=<?php echo $_GET['type']; ?>";
            // $(location).attr('href', url); 
            window.open(url, "_blank", "_blank", "toolbar=yes,top=500,left=500,width=400,height=400");
            return false;
        }   
setInterval(reloadIframe, 15000);
function reloadIframe(){
    $('iframe').each(function() {
      this.contentWindow.location.reload(true);
    });
}
</script>

<?php

$this->load->view('footer');
?>