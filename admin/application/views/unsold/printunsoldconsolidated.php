<?php
$this->load->view('headerprint');
?>
    <section class="wrapper">
        <div class="table-agile-info">



            <div class="panel panel-default">
                <div class="panel-heading">
                    <b style="color:#444">UNSOLD REPORT</b>
                </div>
                
                <table style="margin-bottom: 30px;margin-top: 30px;"> 
                    <tr>
                        <td><b>Date:</b></td><td> &nbsp;<?php echo date("d-m-Y", strtotime($sold_date));?></td>
<!--                    <tr></tr>-->
<!--                    <tr>-->
                        <td style="width:100px"></td>
                        <td><b>Ticket:</b></td><td>&nbsp;<?php echo $ticket_name;?></td>
<!--                    </tr>-->
<!--                    <tr></tr>-->
<!--                    <tr>-->
                        <td style="width:100px"></td>
                        <td><b>Draw Code:</b></td><td>&nbsp;<?php echo $draw_code;?></td>
<!--                    </tr>-->
                    </tr>
                </table>
                <div>

                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                       
                        <thead>
                            <tr>
                                <th>No</th>
<!--                                <th>Set</th>-->
                                <th>Name</th>
<!--                                <th>Ticket Code</th>
                                <th>From</th>
                                <th>To</th>-->
                                <th>Unsold Total</th>
<!--                                <th>Date</th>-->
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            $i = 0;
                            foreach ($unsolddata as $udkey) {
                                $i++;
                                $date = $udkey['sold_date'];
                                $newDate = $date;
                                // $newDate = date("d-m-Y", strtotime($date));
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
<!--                                    <td><?php echo $udkey['is_set']; ?></td>-->
                                    <td><?php echo $udkey['name']; ?></td>
<!--                                    <td><?php echo $udkey['ticket_code']; ?></td>
                                    <td><?php echo $udkey['ticket_from']; ?></td>
                                    <td><?php echo $udkey['ticket_to']; ?></td>-->
                                    <td><?php echo $udkey['total']; ?></td>
<!--                                    <td><?php echo $newDate; ?></td>-->
                            <?php
                            }
                            ?>
                            </tr>
                        </tbody>
                    </table>
                    <form method="post" id="listunsoldform" action="<?php echo base_url(); ?>unsold/listunsoldconsolidatedsave/<?php echo $date_of_unsold;?>">
                        <table style="float:right;" >
                            <tr style="width:300px;height: 20px;margin-bottom: 20px;">
                        <div class="panel-title">
                            <td><b>Total Unsold Tickets: </b></td><td><span style="margin-left: 10px;"><?php echo $total; ?></span></td>
                             


                        </div>
                            </tr>
                        <tr style="width:300px;height: 20px;margin-bottom: 20px;">
                            <div class="panel-title">
                                <td><b>Unsold value: </b></td><td> <span style="margin-left: 10px;"><?php echo $sold_data_val;?></b></span><b>&nbsp;<b><i>Rs</i></td>
                            </div>
                        </tr>
                        <tr style="width:300px;height: 20px;margin-bottom: 20px;">
                            <div class="panel-title">
                                <td><b>Pwt and DC : </b></td><td><span style="margin-left: 10px;"><?php echo $pwt_dc_val;?></span><b>&nbsp;<i>Rs</i></b></td>
                            </div>
                        </tr>
                        <tr style="width:300px;height: 20px;margin-bottom: 20px;">
                            <div class="panel-title">
                                <td><b>Winning : </b></td><td><span style="margin-left: 10px;"><?php echo $sold_winning;?></span><b>&nbsp;<i>Rs</i></b></td>
                            </div>
                        </tr>
                        <tr style="width:300px;height: 20px;margin-bottom: 20px;">
                            <div class="panel-title">
                                <td><b>Profit / Loss : </b></td><td><span style="margin-left: 10px;font-weight: bolder"><?php echo $sold_profit_loss;?></span><b>&nbsp;<i>Rs</i></b></td>
                            </div>
                        </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    <script type="text/javascript">
        function dateselect(){
            var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated/"+$('#dateofunsold').val();
            $(location).attr('href', url);        
        }
        
        function createsold() {
            var valid = true;
            var myarray = [];
            $('.pwt_dc,.sold_total').each(function () {
                if ($(this).val() === '') {
                    valid = false;
                    window.scrollTo(0, 0);
                    myarray.push(valid);
                    $('.alert-warning').show().fadeOut(4000);
                    return false;
                }
            });
            if (myarray.length === 0) {
                $("#listunsoldform").submit();
            } else {
                return false;
            }
        }
        
        function printpage() {
            var url = "<?php echo base_url(); ?>unsold/printunsoldconsolidated/<?php echo $date_of_unsold;?>";
            // $(location).attr('href', url); 
            window.open(url, '_blank');
        }

    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(2000); 
            $('.alert-warning').hide();
            $("#dateofunsold").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            window.print();
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>