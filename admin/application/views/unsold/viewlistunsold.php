<?php
$this->load->view('headerprint');
?>
<?php 
$url_date = $this->uri->segment(3);
$url_ticket = $this->uri->segment(4);
$url_shopid = $this->uri->segment(5);
$url_ticket_type = $this->uri->segment(6);
?>
    <section class="wrapper">
        <div class="table-agile-info">

            <!--
                        <div class="row">
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                        </div>-->


                <div class="row">
                    <div class="col-md-2">
                        <input type="button" class="btn btn-info" value="Go Back" onclick="goback();">
                    </div>
                    <div class="col-md-2">
                        <input type="button" class="btn btn-warning" value="Print" onclick="print();">
                    </div>
                </div>
            <div class="panel panel-default table-responsive">
                <div class="panel-heading" style="font-weight: bolder">
                    <b style="color:#444">Today's Retail Unsold</b>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>Shop:</b> &nbsp;<i><?php echo $unsolddata[0]['name'];?></i>
                    </div>
                    <div class="col-md-2">
                        <b>Date:</b> &nbsp;<?php echo date("d-m-Y", strtotime($ticket_details['date']));?></b>
                    </div>
                    <div class="col-md-2">
                        <b>Ticket:</b> &nbsp;<?php echo $ticket_details['ticket_name'];?></b>
                    </div>
                    <div class="col-md-2">
                        <b>Draw Code:</b>&nbsp;<?php echo $ticket_details['draw_code'];?></b>
                    </div>
                </div>
                <div>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Serial Code</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Qty</th>
                                <th>Count</th>
                                <th>Total</th>
<!--                                <th>Date</th>-->
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <tr>
                            <?php
                            $i = 0;
                            
                           // echo '<pre>'; print_r($unsolddata); exit;
                            foreach ($unsolddata as $udkey) {
                                $i++;
                                $date = $udkey['sold_date'];
                                //$newDate = $date;
                                 $newDate = date("d-m-Y g:i:s A", strtotime($date));
                                 $type = '-';
                                 if($udkey['is_set'] == 'YES'){
                                     $type = 'SET';
                                 }
                                ?>
                                    <td><?php echo $i; ?></td>
                                    <td><b><?php echo $type; ?></b></td>
                                    <td><?php echo $udkey['ticket_code']; ?></td>
                                    <td><?php echo $udkey['ticket_from']; ?></td>
                                    <td><?php echo $udkey['ticket_to']; ?></td>
                                    <td><?php echo $udkey['ticket_quantity']; ?></td>
                                    <td><?php echo $udkey['ticket_count']; ?></td>
                                    <td><?php echo $udkey['total']; ?></td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                    <td style="font-size: 16px;font-weight: bolder;font-style: italic">Total Unsold: </td>
                                    <td style="color:red;font-weight: bolder;font-size: 16px"><?php echo $total; ?></td>
                                    <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
                            <div class="row">
                   
                </div>
        </div>
    <script>
        $(document).ready(function () {
           // window.print();
        });
                function goback(){
                    var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated?date=<?php echo $url_date; ?>&&ticket=<?php echo $url_ticket; ?>&&type=<?php echo $url_ticket_type; ?>";
               //   alert(url);
                    $(location).attr('href', url);
                }
                function print(){
                    var url = "<?php echo base_url(); ?>unsold/printlistunsold/<?php echo $url_date; ?>/<?php echo $url_ticket; ?>/<?php echo $url_shopid; ?>/<?php echo $url_ticket_type; ?>";
                    window.open(url, "_blank", "_blank", "toolbar=yes,top=500,left=500,width=400,height=400");
                    return false;
                }
    </script>
    
    <?php $this->load->view('footer'); ?>