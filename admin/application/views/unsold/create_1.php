<?php
$this->load->view('header');
?>
<style>
    .button_text{
        font-family:Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;
        color:#fff;
        padding:3px;
        font-weight: bolder;
    }
</style>
<script type="text/javascript">
    function createsold() {
        var valid = true;
        var myarray = [];
        $('.sold_code,.sold_from,.sold_total').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().fadeOut(4000);
                return false;
            }
        });
        $('.sold_to').each(function () {
            if ($(this).parents('.soldeach').find('.sold_set').prop('checked') != true) {
                if ($(this).val() === '') {
                    valid = false;
                    window.scrollTo(0, 0);
                    myarray.push(valid);
                    $('.alert-warning').show().fadeOut(4000);
                    return false;
                }
            }
        });

        if (myarray.length === 0) {
            $("#createsold").submit();
        } else {
            return false;
        }
    }
    
    function back(){
        var url = "<?php echo base_url(); ?>unsold/listunsold";
        $(location).attr('href', url);
    }
</script>

<section id="main-content">
    <section class="wrapper">
        <div class="form-w3layouts">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <?php if ($this->session->flashdata('message')) { ?> 
                        <div class="alert alert-success">
                          <strong><?php echo $this->session->flashdata('message'); ?></strong>
                        </div>
                        <?php } ?>
                        <div class="alert alert-warning">
                          <strong>Warning!</strong> Please fill all the fields.
                        </div>
                        <header class="panel-heading">
                            <b style="color:#444">TODAY'S UNSOLD DETAILS</b>

                        </header>
                        <div class="panel-body">
                            <div class="position-left" style="width:100%">
                                <form role="form" id="createsold" method="post" action="<?php echo base_url() ?>unsold/savesold" accept-charset="utf-8">
                                    <div class="form-group">
                                        <label for="ticketname">Ticket name : <?php echo $lotteryame; ?></label>
                                        <div class="box-header with-border" style="float:right;">
                                                <span  class="btn btn-success btn-sm" id="Add_notice">
                                                    <span class="glyphicon button_text" >ADD UNSOLD</span>
                                                </span>
                                        </div>
                                    </div>
                                    <div class="box  box-danger">
                                        <div class="box-body">
                                            <div class="row" id="notedissueheading">
                                                <div class="col-md-1 form-group">
                                                        <label>Set</label>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                        <label>Ticket code</label>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                        <label>From</label>
                                                 </div>
                                                <div class="col-md-3 form-group">
                                                        <label>To</label>
                                                </div>
                                                <div class="col-md-1 form-group">
                                                        <label>Total</label>
                                                </div>
                                                <div class="col-md-1 form-group"
                                                        <label></label>
                                                </div>
                                            </div>
                                            <div id="solddata">
                                                    <?php
                                                        $total = 0;
                                                        if (empty($unsold)) {
                                                    ?>
                                                        <div class="soldeach row">
                                                            <div class="form-group col-md-1">
                                                                 <input type="checkbox" value="" id="sold_set_1" class="sold_set" name="sold[set][1]" value="1">
                                                             </div>                                        
                                                             <div class="form-group col-md-3">
                                                                 <input type="text" class="sold_code" name="sold[code][1]">
                                                             </div>
                                                             <div class="form-group col-md-3">
                                                                 <input type="number" class="sold_from" name="sold[from][1]">
                                                             </div>                                        
                                                             <div class="form-group col-md-3">
                                                                 <input type="number" class="sold_to" name="sold[to][1]">
                                                             </div>
                                                             <div class="col-md-1">
                                                                 <input style="width:63px;" type="number" class="sold_total" name="sold[total][1]" readonly>
                                                             </div>
                                                             <div class="col-md-1">
                                                                     <span class="btn btn-danger btn-sm deletenotice">
                                                                              <span class="glyphicon button_text">Delete
                                                                              </span>
                                                                     </span>
                                                             </div>
                                                        </div>
                                                    <?php
                                                        } else {
                                                            $inc = 1;
                                                            foreach ($unsold as $ukey => $uvalue) {
                                                                $checked = '';
                                                                $disabled = '';
                                                                $readonly = 'readonly';
                                                                $total += $uvalue['total'];
                                                                if ($uvalue['is_set'] == 'YES') {
                                                                    $checked = 'checked = "checked"';
                                                                    $disabled = 'disabled = "disabled"';
                                                                    $readonly = '';
                                                                    $uvalue['ticket_to'] = '';
                                                                }
                                                    ?>
                                                        <div class="soldeach row">
                                                            <div class="form-group col-md-1">
                                                                 <input type="checkbox" value="" id="sold_set_<?php echo $inc;?>" class="sold_set" name="sold[set][<?php echo $inc;?>]" value="1" <?php echo $checked;?>>
                                                             </div>                                        
                                                             <div class="form-group col-md-3">
                                                                 <input type="text" class="sold_code" name="sold[code][<?php echo $inc;?>]"  value="<?php echo $uvalue['ticket_code'];?>">
                                                             </div>
                                                             <div class="form-group col-md-3">
                                                                 <input type="number" class="sold_from" name="sold[from][<?php echo $inc;?>]"  value="<?php echo $uvalue['ticket_from'];?>">
                                                             </div>                                        
                                                             <div class="form-group col-md-3">
                                                                 <input type="number" class="sold_to" name="sold[to][<?php echo $inc;?>]"  value="<?php echo $uvalue['ticket_to'];?>"  <?php echo $disabled;?>>
                                                             </div>
                                                             <div class="col-md-1">
                                                                 <input style="width:63px;" type="number" class="sold_total" name="sold[total][<?php echo $inc;?>]"  value="<?php echo $uvalue['total'];?>" <?php echo $readonly;?>>
                                                             </div>
                                                             <div class="col-md-1">
                                                                     <span class="btn btn-danger btn-sm deletenotice">
                                                                              <span class="glyphicon button_text">Delete
                                                                              </span>
                                                                     </span>
                                                             </div>
                                                        </div>
                                                    <?php 
                                                            $inc++;
                                                            }
                                                        }
                                                    ?>
                                            </div>
                                        </div>
                                        <div class="row" style="font-size: large;font-weight: bolder">
                                            <div class="form-group col-md-1">
                                             </div>                                        
                                             <div class="form-group col-md-3">
                                             </div>
                                             <div class="form-group col-md-3">
                                              </div>                                        
                                             <div class="form-group col-md-3">
                                             </div>
                                             <div class="col-md-1">
                                                <span style="color:red;" id="sold_full_total"><?php echo $total; ?></span>
                                             </div>
                                             <div class="col-md-1">
                                             </div>
                                        </div>

                                        <div class="row">
                                            <button type="button" onclick="createsold()" class="btn btn-info" style="float:left">Submit</button>
                                            <button type="submit" onclick="back()" class="btn btn-info" style="float:right">Back</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);  
        $('.alert-warning').hide();
        $("#addremarks").on("click", function () {
            $("#addremarksdiv").append("<div class='remarkeach'><div class='col-md-9' style='margin-bottom: 10px;'><input type='text' name='remarks[]' class='form-control'/></div><div class='col-md-2'><span  class='btn btn-info btn-sm deleteremarks'><span class='glyphicon glyphicon-minus-sign'></span></span><br></div></div>");
        });        
        

        $( "#addremarksdiv" ).on( "click", ".remarkeach .deleteremarks", function() {
            $(this).parents('.remarkeach').remove();
        });

        $("#Add_notice").on("click", function () {
            var id = 1;
            if ( $('.sold_set').length > 0) {
                var lastid = $('.sold_set:last').attr('id');
                var idarray = lastid.split('_');
                id = parseInt(idarray[2]) + 1;
            }
            
            $("#solddata").append("<div class=\"soldeach row\">\n\
                                       <div class=\"form-group col-md-1\">\n\
                                            <input type=\"checkbox\" value=\"\" id=\"sold_set_"+id+"\" class=\"sold_set\" name=\"sold[set]["+id+"]\" value=\"1\">\n\
                                        </div>\n\                                        \n\
                                        <div class=\"form-group col-md-3\">\n\
                                            <input type=\"text\" class=\"sold_code\" name=\"sold[code]["+id+"]\">\n\
                                        </div>\n\
                                        <div class=\"form-group col-md-3\">\n\
                                            <input type=\"number\" class=\"sold_from\" name=\"sold[from]["+id+"]\">\n\
                                        </div>\n\                                        \n\
                                        <div class=\"form-group col-md-3\">\n\
                                            <input type=\"number\" class=\"sold_to\" name=\"sold[to]["+id+"]\">\n\
                                        </div>\n\
                                        \n\<div class=\"col-md-1\">\n\
                                            <input style=\"width:63px;\" type=\"number\" class=\"sold_total\" name=\"sold[total]["+id+"]\" readonly>\n\
                                            </div>\n\
                                            <div class=\"col-md-1\">\n\
                                                <span class=\"btn btn-danger btn-sm deletenotice\">\n\
                                                     <span class=\"glyphicon button_text\">Delete\n\
                                                     </span>\n\
                                                </span>\n\
                                            </div\n\
                                            \n\
                                    </div>");
            var len = $(".soldeach").length;
            if (len == 0) {
                $('#notedissueheading').hide();
            } else {
                $('#notedissueheading').show();
            }

        });
        $( "#solddata" ).on( "click", ".soldeach .deletenotice", function() {
            $(this).parents('.soldeach').remove();
            var len = $(".soldeach").length;
            if (len == 0) {
                $('#notedissueheading').hide();
            } else {
                $('#notedissueheading').show();
            }
            var sum = 0;
            $('.sold_total').each(function(){
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            $('#sold_full_total').text(sum);
        });
        
        $( "#solddata" ).on( "click", ".soldeach .sold_set", function() {
            if($(this).prop('checked') == true){
                $(this).parents('.soldeach').find('.sold_to').prop('disabled','disabled');
                $(this).parents('.soldeach').find('.sold_to').val('');
                $(this).parents('.soldeach').find('.sold_total').removeAttr('readonly');
            } else {
                $(this).parents('.soldeach').find('.sold_total').prop('readonly', true);
                $(this).parents('.soldeach').find('.sold_to').removeAttr('disabled');
            }
            $(this).parents('.soldeach').find('.sold_total').val('');
            var sum = 0;
            $('.sold_total').each(function(){
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            $('#sold_full_total').text(sum);
        });
        
//        $("#solddata").on('keyup', ".soldeach .sold_code" , function(e) {
//            var val = $(this).val();
//            // alert(val);
//           if (val.match(/[^a-zA-Z]/g)) {
//               $(this).val(val.replace(/[^a-zA-Z]/g, ''));
//           }
//        });

        $("#solddata").on('keyup', ".soldeach .sold_from, .soldeach .sold_to" , function(e) {
            var from = $(this).parents('.soldeach').find('.sold_from').val();
            var to = $(this).parents('.soldeach').find('.sold_to').val();
            var total = parseInt(to) - parseInt(from) + 1;
            $(this).parents('.soldeach').find('.sold_total').val(total);
            var sum = 0;
            $('.sold_total').each(function(){
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            $('#sold_full_total').text(sum);
        });
        
        $("#solddata").on('keyup', ".soldeach .sold_total" , function(e) {
            var sum = 0;
            $('.sold_total').each(function(){
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            $('#sold_full_total').text(sum);
        });
        

    });
</script>

</script>
<?php
$this->load->view('footer');
?>