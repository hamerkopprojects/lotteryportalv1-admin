
<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        
        <div class="table-agile-info">

            <!--
                        <div class="row">
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                        </div>-->



            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                  <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
                <?php } ?>
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Please fill all the fields.
                </div>
                <div class="panel-heading">
                    <b style="color:#444">UNSOLD REPORT</b>
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-2">
                            <b>Select Date:</b> <input type="text" class="form-control" id="dateofunsold" name="dateofunsold"  value="<?php echo $date_of_unsold;?>" placeholder="Select a date" onchange="dateselect();">
                        </div>
                        <form method="post" id="listunsoldform" action="<?php echo base_url(); ?>unsold/listunsoldconsolidatedsave/<?php echo $date_of_unsold;?>">
                        <div class="col-md-2">
                            <b>Select Ticket:</b> 
                            <select name="tickets_id" id="ticketnames" class="form-control select2" style="width: 100%;">
                            <?php 
                                foreach ($ticketlist as $key => $value) { ?>
                                <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id'];?>"><?php echo $value['ticket_name'];?></option>
                            <?php 
                                }
                            ?>
                            </select>
                        </div>
                            <div class="col-md-2">
                                <b>Select Ticket Type:</b> 
                                <select name="type" id="tickettypename" class="form-control select2" style="width: 100%;">
                                      <option <?php if (isset($unsold_type) && $unsold_type  == "RETAIL") { ?> selected="selected" <?php } ?>  value="RETAIL">RETAIL</option>
                                      <option <?php if (isset($unsold_type) && $unsold_type  == "WHOLESALE") { ?> selected="selected" <?php } ?>  value="WHOLESALE">WHOLESALE</option>
                                 </select>
                            </div>
                                         
                        
                        <?php 
//                        $unsold_type = end($this->uri->segment_array()); 
//                        $unsold_type_text = ucfirst(strtolower($unsold_type));
                        
                        
                        ?>
                        <?php //if($date_of_unsold == date('Y-m-d')) { ?>
                        
                        <?php if(!empty($unsolddata)) { ?>
                            <div class="col-md-2" style="padding-top: 20px">
                                    <input type="button" class="btn btn-secondary" value="Sync <?php //echo $unsold_type_text; ?> Unsold" onclick="alert('Please Clear <?php echo $unsold_type_text; ?> Unsold Data to Sync Again');">
                            </div>

 
                        <?php } else { ?>
                        <div class="col-md-2" style="padding-top: 20px">
                                     <input type="button" class="btn btn-info" value="Sync <?php //echo $unsold_type_text; ?> Unsold" onclick="syncretailunsold();"> 
                            </div>
                        <?php } ?>
                            <div class="col-md-2" style="padding-top: 20px">
                                     <input type="button" class="btn btn-info" value="Clear <?php //echo $unsold_type_text; ?> Unsold Data" onclick="clearunsolddata();"> 
                            </div>
                        <?php //} ?>

                    <?php if(!empty($unsolddata)) { ?>
                    <div class="col-md-1" style="padding-top: 20px">
                             <input type="button" class="btn btn-success" value="Total Print" onclick="printpage();"> 
                    </div>
                    <?php } ?>
                    </div>  
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Shop Code</th>
                                <th>Unsold Total</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            $i = 0;
                            //echo '<pre>'; print_r($unsolddata); exit;
                            foreach ($unsolddata as $udkey) {
                                $i++;
                                $date = $udkey['sold_date'];
                                $newDate = $date;
                                // $newDate = date("d-m-Y", strtotime($date));
                                $newDate = date("d-m-Y g:i:s A", strtotime($newDate));
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $udkey['name']; ?></td>
                                    <td><b><?php echo $udkey['shop_code']; ?></td>
                                    <td><?php echo $udkey['total']; ?></td>
                                    <td><?php echo $newDate; ?></td>
                                    <td><input type="button" class="btn btn-success" value="View" onclick="viewunsolddetails(<?php echo $udkey['user_id']; ?>)"> </td>
                                    <td><input type="button" class="btn btn-warning" value="Print" onclick="printunsolddetails(<?php echo $udkey['user_id']; ?>)"> </td>   
                                    <?php if($udkey['is_draft'] == 0) { ?>
<!--                                        <td><input type="button" class="btn btn-info" value="Unlock" onclick="unlockunsoldedit(<?php echo $udkey['user_id']; ?>)"> </td> -->
                                    <?php } ?>
                                    </tr>
                            <?php
                            }
                            ?>
                            
                            
                                <?php if($_GET['data'] == 'nil'){ ?>
                                        
                                <tr>
                                    <td></td>
                                    <td></td>
                                   <td style="color:red;font-weight: bolder;font-size: 15px">NO DATA TO SYNC</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php } ?>  
                                 
                            <tr>
                                <td></td>
                                <td style="color:blue;">Total Unsold Tickets</td>
                                <td><span style="font-weight: bolder;"><?php echo $total; ?></span></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="color:blue;">Unsold Value</td>
                                <td><input style="width:112px;" type="number" class="sold_total" name="sold_total" value="<?php echo ($sold_data_val != 0)?$sold_data_val:$unsoldtotal;?>"><b>&nbsp;<i>Rs</i></b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="color:blue;">Winning</td>
                                <td><input style="width:112px;" type="number" class="sold_winning" name="winning" value="<?php echo ($sold_winning !=0)?$sold_winning:$winningtotal;?>"><b>&nbsp;<i>Rs</i></b></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="color:blue;">Pwt and AP</td>
                                <td><input style="width:112px;" type="number" class="pwt_dc" name="pwt_dc" value="<?php echo ($pwt_dc_val !=0)?$pwt_dc_val:$dctotal;?>"><b>&nbsp;<i>Rs</i></b></td>
                            </tr>
                            <tr>
                                <td><input type="hidden" name="profit_loss" id="profit_loss_save" value="<?php echo $sold_profit_loss;?>"></td>
                                <td style="color:blue;">Profit/Loss</td>
                                <div id="hide_profit_loss">
                                    <?php if($sold_profit_loss > 0){ ?>
                                        <td><span id="unsold_profit" style='color:green;font-weight:bolder;'><?php echo $sold_profit_loss; ?></span></td>
                                    <?php } else if($sold_profit_loss < 0) { ?>
                                        <td><span id="unsold_profit" style='color:red;font-weight:bolder;'><?php echo $sold_profit_loss; ?></span></td>
                                    <?php } else if($sold_profit_loss == '') {?>
                                        <td><span id="profit_loss"></span></td>
                                    <?php } ?>
                                </div>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><input type="button" class="btn btn-info" value="Save" onclick="createsold();"></td>
                            </tr>
                        </tbody>
                    </table>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function dateselect(){
            var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated?date="+$('#dateofunsold').val()+"&&ticket="+$('#ticketnames').val()+"&&type="+$('#tickettypename').val();
            $(location).attr('href', url);        
        }
        
        function unsolddata(){
            var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated?date="+$('#dateofunsold').val()+"&&ticket="+$('#ticketnames').val()+"&&type="+$('#tickettypename').val();
            $(location).attr('href', url); 
        }
        
        $("#ticketnames").change(function () {
            var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated?date="+$('#dateofunsold').val()+"&&ticket="+$('#ticketnames').val()+"&&type="+$('#tickettypename').val();
            $(location).attr('href', url); 
        });
        
//        function createsold() {
//            var valid = true;
//            var myarray = [];
//            if(!$("#ticketnames option:selected").length) {
//                $('.alert-warning').show().fadeOut(10000);
//                return false;
//            }
//            
//            $('.pwt_dc,.sold_total').each(function () {
//                if ($(this).val() === '') {
//                    valid = false;
//                    window.scrollTo(0, 0);
//                    myarray.push(valid);
//                    $('.alert-warning').show().fadeOut(4000);
//                    return false;
//                }
//            });
//            if (myarray.length === 0) {
//                $("#listunsoldform").submit();
//            } else {
//                return false;
//            }
//        }
        
        function printpage() {
            var url = "<?php echo base_url(); ?>unsold/printunsoldconsolidated/<?php echo $date_of_unsold;?>/"+$('#ticketnames').val()+"/"+$('#tickettypename').val();
            // $(location).attr('href', url); 
            window.open(url, "_blank", "_blank", "toolbar=yes,top=500,left=500,width=400,height=400");
            return false;
        }
        
        function printunsolddetails(user_id) {
            var url = "<?php echo base_url(); ?>unsold/printlistunsold/<?php echo $date_of_unsold;?>/"+$('#ticketnames').val()+"/"+user_id+"/"+$('#tickettypename').val();
            // $(location).attr('href', url); 
            window.open(url, "_blank", "_blank", "toolbar=yes,top=500,left=500,width=400,height=400");
            return false;
        }
        function viewunsolddetails(user_id) {
            var url = "<?php echo base_url(); ?>unsold/viewlistunsold/<?php echo $date_of_unsold;?>/"+$('#ticketnames').val()+"/"+user_id+"/"+$('#tickettypename').val();
            // $(location).attr('href', url); 
           // window.open(url, "_blank", "_blank", "toolbar=yes,top=500,left=500,width=400,height=400");
           // return false;
           $(location).attr('href', url);  
        }
        
        function unlockunsoldedit(user_id){
            
            var url = "<?php echo base_url(); ?>unsold/unlockunsoldedit/<?php echo $date_of_unsold;?>/"+$('#ticketnames').val()+"/"+user_id+"/"+$('#tickettypename').val();
            $(location).attr('href', url);  
            
        }
        
        function syncretailunsold(){
        jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
             
     
        var url = "<?php echo base_url(); ?>unsold/syncretailunsold/"+$('#dateofunsold').val()+"/"+$('#ticketnames').val()+"/<?php echo $unsold_type; ?>"
        $(location).attr('href', url); 
            
        }
        
        function clearunsolddata(){
            var url = "<?php echo base_url(); ?>unsold/clearunsolddata/<?php echo $date_of_unsold;?>/"+$('#ticketnames').val()+"/<?php echo $unsold_type; ?>"
            $(location).attr('href', url); 
        }
        $("#tickettypename").change(function () {
                var url = "<?php echo base_url(); ?>unsold/listunsoldconsolidated?date=<?php echo $date_of_unsold;?>&&ticket="+$('#ticketnames').val()+"&&type="+$('#tickettypename').val();
                $(location).attr('href', url); 
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(10000); 
            $('.alert-warning').hide();
            $("#dateofunsold").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
            
//            $("#fbody").on('keyup', ".sold_total, .sold_winning, .pwt_dc", function (e) {
//                var total = 0;
//                var sold_total = $('.sold_total').val();
//                var sold_winning = $('.sold_winning').val();
//                var pwt_dc = $('.pwt_dc').val();
//
//                if(sold_total !='' && sold_winning =='' && pwt_dc ==''){
//                    total = parseInt('0');
//                } else if(sold_total !='' && sold_winning !='' && pwt_dc ==''){
//                    total = parseInt('0');
//                } else if(sold_total !='' && sold_winning !='' && pwt_dc !=''){
//                    total = parseInt(sold_total)-(parseInt(sold_winning)+parseInt(pwt_dc));
//                }
//                if(total > 0){
//                    
//                    $('#profit_loss_save').val('-'+total)
////                    $("#profit_loss").html("<div style='color:red;font-weight:bolder;'>-"+Math.abs(total)+"</div>");
//                } else if(total < 0){
//                    $('#profit_loss_save').val(Math.abs(total))
////                    $("#profit_loss").html("<div style='color:green;font-weight:bolder;'>"+Math.abs(total)+"</div>");
//                }
//            });
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>