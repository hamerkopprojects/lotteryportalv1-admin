<?php
$this->load->view('header');
?>

<section id="main-content">
    <section class="wrapper">
        
        <div class="table-agile-info">
            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?> 
                <div class="alert alert-success">
                  <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
                <?php } ?>
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Please fill all the fields.
                </div>
                <div class="panel-heading">
                    <div class="row" >
                        <div class="col-md-8" ><b style="color:#444">Unsold Details - <?php echo $date_of_unsold; ?></b></div>
                    </div>
                </div>        

                
    <table class="table table-borderless">
    <thead>
      <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Select Date</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><select name="tickets_id" id="ticketname" class="form-control select2">
            <?php 
               foreach ($ticketlist as $key => $value) { ?>
            <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id'];?>"><?php echo $value['ticket_name'];?></option>
            <?php 
               }
               ?>
         </select></td>
        <td><select name="type" id="tickettypename" class="form-control select2" style="width: 100%;">
                          <option <?php if (isset($selectedtickettype) && $selectedtickettype == "RETAIL") { ?> selected="selected" <?php } ?>  value="RETAIL">RETAIL</option>
                          <option <?php if (isset($selectedtickettype) && $selectedtickettype == "WHOLESALE") { ?> selected="selected" <?php } ?>  value="WHOLESALE">WHOLESALE</option>
                     </select></td>
        <td><input type="text" class="form-control" id="dateofunsold" name="dateof_unsold"   value="<?php echo $date_of_unsold; ?>" placeholder="Select a date" onchange="dateselect();" ></td>
      </tr>
    </tbody>
  </table>
                 
    <div class="table-responsive">
        <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Shop Name</th>
        <th>Live Details</th>
      </tr>
    </thead>
    <tbody>
             <?php
                            $i = 0;
                          //echo '<pre>'; print_r($unsolddata ); exit;
                            foreach ($unsolddata as $key) {
                                $i++;
                                $date = $key['sold_date'];
                                $ticketid=$key['tickets_id'];
                                $tickettype=$key['type'];
                                $color = ($i%2 == 0)?"#ddede0":"#ffff";
                                ?>
                                <tr style ="background-color: <?php echo $color; ?>!important;">
                                    <td ><?php echo $i; ?></td>
                                    <td ><b><?php echo $key['name']; ?></b></td>
                                    <td ><input type="button" class="btn" style="font-weight:bolder;font-size: 30px;background-color: #b3b3b3!important;" value="<?php echo $key['shop_code']; ?>" onclick="viewunsolddetails(<?php echo $key['user_id']; ?>)"> </td>
                                    <td > <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="View Details" onclick="viewunsolddetails(<?php echo $key['user_id']; ?>)"></td>
                                </tr>
                            <?php
                            }
                            ?>                          
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
         function viewunsolddetails(user_id) {
            var url = "<?php echo base_url(); ?>unsold/list_shop_unsolddetails/"+$('#dateofunsold').val()+"/"+$('#ticketname').val()+"/"+user_id+"/"+$('#tickettypename').val();
             $(location).attr('href', url); 
            return false;
        }
        function dateselect(){
            var url = "<?php echo base_url(); ?>unsold/unsold_details/"+$('#dateofunsold').val()+"/"+$('#ticketname').val()+"/"+$('#tickettypename').val();
            $(location).attr('href', url);  
        }
         $("#ticketname").change(function () {
            var url = "<?php echo base_url(); ?>unsold/unsold_details/"+$('#dateofunsold').val()+"/"+$('#ticketname').val()+"/"+$('#tickettypename').val();
            $(location).attr('href', url); 
        });
         $("#tickettypename").change(function () {
            var url = "<?php echo base_url(); ?>unsold/unsold_details/"+$('#dateofunsold').val()+"/"+$('#ticketname').val()+"/"+$('#tickettypename').val();
            $(location).attr('href', url); 
        });
         
    </script>
    <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(2000); 
            $('.alert-warning').hide();
            $("#dateofunsold").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
        });
    </script>
    <?php
    $this->load->view('footer');
    ?>