<?php

$this->load->view('header');
?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="panel-body">
                <div class="col-md-12 w3ls-graph">
                        <div class="agileits-box">
                            <header class="agileits-box-header clearfix">
                                <div class="col-md-4" ></div>

                                 <div class="panel-heading" style="font-weight: bolder">
                                               <b style="color:#444">Today's Retail Unsold</b>
                                           </div>

                                <div class="col-md-4" ><input type="button" class="btn btn-info" style="font-weight:bolder;font-size: 20px;" value="Go Back" onclick="goback()"></div>
                            </header>
                           <table style="margin-bottom: 30px;margin-top: 30px;"> 
                    <tr>
                        <td><b>Shop:</b></td><td> &nbsp;<b><?php echo $unsolddata[0]['name'];?></b></td>
<!--                    <tr></tr>-->
<!--                    <tr>-->
                        <td style="width:100px"></td>
                        <td><b>Date:</b></td><td> &nbsp;<?php echo date("d-m-Y", strtotime($ticket_details['date']));?></td>
<!--                    <tr></tr>-->
<!--                    <tr>-->
                        <td style="width:100px"></td>
                        <td><b>Ticket:</b></td><td>&nbsp;<?php echo $ticket_details['ticket_name'];?></td>
<!--                    </tr>-->
<!--                    <tr></tr>-->
<!--                    <tr>-->
                        <td style="width:100px"></td>
                        <td><b>Draw Code:</b></td><td>&nbsp;<?php echo $ticket_details['draw_code'];?></td>
<!--                    </tr>-->
                    </tr>
                </table>
                <div>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Serial Code</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Qty</th>
                                <th>Count</th>
                                <th>Total</th>
<!--                                <th>Date</th>-->
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <tr>
                            <?php
                            $i = 0;
                           // echo '<pre>'; print_r($unsolddata); exit;
                            foreach ($unsolddata as $udkey) {
                                $i++;
                                $date = $udkey['sold_date'];
                                //$newDate = $date;
                                 $newDate = date("d-m-Y g:i:s A", strtotime($date));
                                 $type = '-';
                                 if($udkey['is_set'] == 'YES'){
                                     $type = 'SET';
                                 }
                                ?>
                                    <td><?php echo $i; ?></td>
                                    <td><b><?php echo $type; ?></b></td>
                                    <td><?php echo $udkey['ticket_code']; ?></td>
                                    <td><?php echo $udkey['ticket_from']; ?></td>
                                    <td><?php echo $udkey['ticket_to']; ?></td>
                                    <td><?php echo $udkey['ticket_quantity']; ?></td>
                                    <td><?php echo $udkey['ticket_count']; ?></td>
                                    <td><?php echo $udkey['total']; ?></td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                    <td style="font-size: 16px;font-weight: bolder;font-style: italic">Total Unsold: </td>
                                    <td style="color:red;font-weight: bolder;font-size: 16px"><?php echo $total; ?></td>
                                    <td></td>
                            </tr>
                        </tbody>
                    </table>
                        </div>
                  
                </div>
            </div>
        </div>

    </section>

</section>
<script>
    function goback(){
       var url = "<?php echo $goback; ?>";
        $(location).attr('href', url);        
    }
setInterval(reloadIframe, 15000);
function reloadIframe(){
    $('iframe').each(function() {
      this.contentWindow.location.reload(true);
    });
}
</script>
<?php

$this->load->view('footer');
?>