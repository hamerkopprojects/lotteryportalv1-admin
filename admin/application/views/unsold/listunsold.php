<?php
$this->load->view('header');
?>
<section id="main-content">
    <section class="wrapper">
        <div class="table-agile-info">

            <!--
                        <div class="row">
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                            <div class="col-md-4">.col-sm-4</div>
                        </div>-->



            <div class="panel panel-default">
                <div class="panel-heading" style="font-weight: bolder">
                    <b style="color:#444">Unsold data list</b>
<!--                    <button onclick="adduserfile()" class="btn btn-info pull-right" >Upload File</button>-->
<!--                    <div class="panel-title pull-LEFT" style="color:green">
                        Total number of unsold tickets : <?php echo $total; ?> 

                    </div>-->
                </div>
                <div>
                <form method="post" id="listunsoldform" action="<?php echo base_url(); ?>unsold/listunsold">
                    <div class="pull-LEFT" style="margin-left: 20px;">
<!--                        <input type="date" id="dateofunsold" name="dateofunsold" value="<?php echo $date_of_unsold;?>" min="1998-01-01" onchange="dateselect();">-->
                            <input type="text" class="form-control" id="dateofunsold" name="dateofunsold"  value="<?php echo $date_of_unsold;?>" placeholder="Select a date" onchange="dateselect();">

                    </div>
                   
                    <div class="pull-LEFT" style="margin-left: 20px;margin-right: 20px;">
                            <select name="ticketname" id="ticketname" class="form-control select2" style="width: 100%;">
                          <?php if (!empty($ticketlist)) { ?> 
                            <?php 
                                foreach ($ticketlist as $key => $value) { ?>
                                <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id'];?>"><?php echo $value['ticket_name'];?></option>
                            <?php 
                                }
                            ?>
                            <?php } else { ?>
                                <option value="">Admin did not set tickets for today</option>
                            <?php } ?>
                            </select>
                    </div>
                    <?php 
                        if ($user_type != 'SUPERADMIN' && $user_type != 'ADMIN') {
                    ?>
                        <div class="pull-LEFT">
                                <input type="button" class="btn btn-info" value="Go" onclick="listUnsoldBySelect()">
                        </div>
                        <?php 
                            }
                        ?>
                        <?php 
                            if ($user_type == 'SUPERADMIN' || $user_type == 'ADMIN') {
                        ?>
                        <div class="pull-LEFT" style="margin-left: 20px;">
                                <select class="form-control m-bot15" name="userid" id="assign_to">
                                    <option value=""><b>All Users</b></option>
                                    <?php
                                    foreach ($userlist as $usernames) {
                                        $selected = '';
                                        if ($selecteduserid == $usernames->id) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option  value="<?php echo $usernames->id; ?>" <?php echo $selected;?>><?php echo $usernames->firstname.' '.$usernames->middlename.' '.$usernames->lastname; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>   
                        </div>
                        <div class="pull-LEFT" style="margin-left: 20px;">
                            <input type="button" class="btn btn-info" value="Go" onclick="$('#listunsoldform').submit();">
                        </div>
                        <?php 
                            }
                        ?>
                </form>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Total</th>
<!--                                <th>Date</th>-->
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <tr>
                            <?php
                            $i = 0;
                            foreach ($unsolddata as $udkey) {
                                $i++;
                                $date = $udkey['sold_date'];
                                //$newDate = $date;
                                 $newDate = date("d-m-Y g:i:s A", strtotime($date));
                                 $type = '-';
                                 if($udkey['is_set'] == 'YES'){
                                     $type = 'SET';
                                 }
                                ?>
                                    <td><?php echo $i; ?></td>
                                    <td><b><?php echo $type; ?></b></td>
                                    <td><?php echo $udkey['name']; ?></td>
                                    <td><?php echo $udkey['ticket_from']; ?></td>
                                    <td><?php echo $udkey['ticket_to']; ?></td>
                                    <td><?php echo $udkey['total']; ?></td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="font-size: 18px;font-weight: bolder;font-style: italic">Total Unsold: </td>
                                    <td style="color:red;font-weight: bolder;font-size: 18px"><?php echo $total; ?></td>
                                    <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        function dateselect(){
            var url = "<?php echo base_url(); ?>unsold/listunsold/"+$('#dateofunsold').val();
            $(location).attr('href', url);        
        }
        
        function listUnsoldBySelect() {  
            var url = "<?php echo base_url(); ?>unsold/listunsold/"+$('#dateofunsold').val()+"/"+$('#ticketname').val();
            $(location).attr('href', url);  
        }
    </script>

    <script>
        $(document).ready(function () {
            $("#dateofunsold").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
        });
    </script>
    
    <?php $this->load->view('footer'); ?>