<?php
$this->load->view('header');
?>
<script type="text/javascript">
   
    $(document).ready(function () {
        $('.alert-success').show().fadeOut(2000);
    });
 </script>
<section id="main-content">

    <section class="wrapper">
        <div class="table-agile-info">

            <div class="panel panel-default">
                <div class="panel-heading" >
                    <b style="color:#444"> Winning Shops List</b>
<!--                    <div class="panel-title pull-LEFT">
                        <input type="text" class="form-control" placeholder="Search" id="searchInput" style="background-color:#5bc0de" name="searchInput" onkeypress="return blockSpecialChar(event)" >
                    </div>-->
                </div>
            </div>
                <div class="row">
                    
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="dateofwining" name="dateofwining"  value="<?php echo $date_of_unsold;?>" placeholder="Select a date" onchange="dateselect();">
                    </div>
                    <div class="col-md-2">
                        <select name="tickets_id" id="ticketnames" class="form-control select2" style="width: 100%;">
                          <?php 
                                foreach ($ticketlist as $key => $value) { ?>
                                <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id'];?>"><?php echo $value['ticket_name'];?></option>
                            <?php 
                                }
                            ?>
                     </select>
                    </div>
                    <div class="col-md-2">
<!--                        <b>Select Ticket Type:</b> -->
                        <select name="type" id="tickettypename" class="form-control select2" style="width: 100%;">
                              <option <?php if (isset($unsold_type) && $unsold_type  == "RETAIL") { ?> selected="selected" <?php } ?>  value="RETAIL">RETAIL</option>
                              <option <?php if (isset($unsold_type) && $unsold_type  == "WHOLESALE") { ?> selected="selected" <?php } ?>  value="WHOLESALE">WHOLESALE</option>
                         </select>
                    </div>
                 
                
                    <?php //if($date_of_unsold == date('Y-m-d')) { ?>    
                        <?php if(!empty($details) || $status == "NODATA") { ?>
                     
                         <div class="col-md-3">
                                     <input type="button" class="btn btn-secondary" value="SyncUnsoldwining" onclick="alert('Please Clear Unsold Winning Data to Sync Again for the Ticket');"> 
                         </div>
                        <?php } else { ?>
                         <div class="col-md-3">
                            <input type="button" class="btn btn-info" value="SyncUnsoldwining" onclick="syncretailunsoldwinningticket();">

                         </div>
                          <?php } ?>
                      

                        <div class="col-md-3">
                                 <input type="button" class="btn btn-info" value="Clear Unsoldwining Data" onclick="clearsyncretailunsoldwinningticket();"> 
                        </div>
                    <?php //} ?>
                          <?php //} ?>

                        <!--<div class="pull-RIGHT" style="margin-right: 20px;margin-top: 10px;">-->
                                 <!--<input type="button" class="btn btn-success" value="Total Print" onclick="printpage();">--> 
                        <!--</div>-->
                      </div> 
                <div>
                    <table class="table" ui-jq="footable" ui-options='{
                           "paging": {
                           "enabled": true
                           },
                           "filtering": {
                           "enabled": true
                           },
                           "sorting": {
                           "enabled": true
                           }}'>
                        <thead>
                            <tr>
                                <th> No</th>
                                <th>Prize</th>
                                <th>Prize Amount</th>
                                <th>Ticket</th>
                                <th>Winning Ticket</th>
                                <th>Ticket Number</th>
                                <th>Shop Name</th>
                                <th>Unsold</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">               

                            <?php
                            //echo '<pre>'; print_r($details); 
                            $i = 1;
                            $j = 0;
                            $k = 0;
                            
                            foreach ($details as $detail) {
                                $k++;
                                if($details[$j]['prize'] == "1st Prize"){
                                    
                                    $color = "red";
                                }
                                if($details[$j]['prize'] == "2nd Prize"){
                                    
                                    $color = "green";
                                }
                                if($details[$j]['prize'] == "3rd Prize"){
                                    
                                    $color = "yellow";
                                }
                                if($details[$j]['prize'] == "4th Prize"){
                                    
                                    $color = "purple";
                                }
                                if($details[$j]['prize'] == "5th Prize"){
                                    
                                    $color = "violet";
                                }
                                if($details[$j]['prize'] == "6th Prize"){
                                    
                                    $color = "blue";
                                }
                                
                                if($details[$j]['prize'] == "7th Prize"){
                                    
                                    $color = "grey";
                                }
                                
                                if($details[$j]['prize'] == "Consolation Prize"){
                                    
                                    $color = "Orange";
                                }
                                ?>
                                <tr>
                                    <td><?php echo $k; ?></td>
                                    <td style="font-weight:bolder; font-size:15px; color:<?php echo $color; ?>"><?php echo $details[$j]['prize']; ?></td>
                                    <td><?php echo $details[$j]['prize_amount']; ?></td>
                                    <td><b><?php echo $details[$j]['ticket_name']; ?><b></td>
                                    <td><b><i><?php echo $details[$j]['won_ticket']; ?></i></b></td>
                                    <td><b><i><?php echo $details[$j]['ticket_number']; ?></i></b></td>
                                    <td><?php echo $details[$j]['shop_name']; ?></td>
                                   <td><?php echo $details[$j]['unsold_type']; ?></td>
                                </tr>
                            <?php 
                            $j++;
                            } ?>
                                <?php if(empty($details) && $status == "NODATA") { ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="color:red;font-weight: bolder;">No Winning from Synced Unsold Data</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                            </table>
                                            </div>

                                            </div>

                                            </div>
                                            </div>
                                            </section>
                                            <!-- footer -->

                                            <!-- / footer -->
                                            </section>
          <script type="text/javascript">
    function dateselect(){
      var url = "<?php echo base_url(); ?>unsold/listunsoldwinning?date="+$('#dateofwining').val()+"&&ticket="+$('#ticketnames').val()+"&&type=<?php echo $unsold_type; ?>";
      $(location).attr('href', url);        
    }

        function syncretailunsoldwinningticket()
    {
        $("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');
        var url = "<?php echo base_url(); ?>unsold/syncretailunsoldwinningticket?date=" + $('#dateofwining').val() + "&&ticket=" + $('#ticketnames').val() + "&&type=<?php echo $unsold_type; ?>"
        $(location).attr('href', url);
    }
       function clearsyncretailunsoldwinningticket() {
        var url = "<?php echo base_url(); ?>unsold/clearsyncretailunsoldwinningticket?date=<?php echo $date_of_unsold  ?>&&ticket=" + $('#ticketnames').val() + "&&type=<?php echo $unsold_type; ?>"
        $(location).attr('href', url);
    }
    $("#ticketnames").change(function () {
            var url = "<?php echo base_url(); ?>unsold/listunsoldwinning?date="+$('#dateofwining').val()+"&&ticket="+$('#ticketnames').val()+"&&type=<?php echo $unsold_type; ?>";
            $(location).attr('href', url); 
    });
    $("#tickettypename").change(function () {
            var url = "<?php echo base_url(); ?>unsold/listunsoldwinning?date=<?php echo $date_of_unsold;?>&&ticket="+$('#ticketnames').val()+"&&type="+$('#tickettypename').val();
            $(location).attr('href', url); 
    });
        </script>
        
        <script>
        $(document).ready(function () {
            $('.alert-success').show().fadeOut(2000); 
            $('.alert-warning').hide();
            $("#dateofwining").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#searchInput").keyup(function () {
                var rows = $("#fbody").find("tr").hide();
                if (this.value.length) {
                    var data = this.value.split(" ");
                    $.each(data, function (i, v) {
                        rows.filter(":contains('" + v + "')").show();
                    });
                } else
                    rows.show();
            });
            
            
            $("#fbody").on('keyup', ".sold_total, .sold_winning, .pwt_dc", function (e) {
                var total = 0;
                var sold_total = $('.sold_total').val();
                var sold_winning = $('.sold_winning').val();
                var pwt_dc = $('.pwt_dc').val();

                if(sold_total !='' && sold_winning =='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc ==''){
                    total = parseInt('0');
                } else if(sold_total !='' && sold_winning !='' && pwt_dc !=''){
                    total = parseInt(sold_total)-(parseInt(sold_winning)+parseInt(pwt_dc));
                }
                if(total > 0){
                    
                    $('#profit_loss_save').val('-'+total)
//                    $("#profit_loss").html("<div style='color:red;font-weight:bolder;'>-"+Math.abs(total)+"</div>");
                } else if(total < 0){
                    $('#profit_loss_save').val(Math.abs(total))
//                    $("#profit_loss").html("<div style='color:green;font-weight:bolder;'>"+Math.abs(total)+"</div>");
                }
            });
        });
    </script>

           <?php
           $this->load->view('footer');
             ?>

