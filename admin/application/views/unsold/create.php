<?php
$this->load->view('header');
?>
<style>
    .button_text{
        font-family:Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;
        color:#fff;
        padding:3px;
        font-weight: bolder;
    }
    .headtitle{
        color:#444;font-weight: bold;
    }
/*    .sold_set,.sold_code,.sold_from,.sold_to,.sold_total,.sold_set_head,.sold_code_head,.sold_from_head,.sold_to_head,.sold_total_head{
        width:150px;
    }
    .sold_set_head{
        width:150px;
        text-align: center;
    }
    .sold_set_td{
        text-align: center;
    }*/
/*    @media only screen and (max-width: 800px) {
        .sold_set,.sold_code,.sold_from,.sold_to,.sold_total,.sold_set_head,.sold_code_head,.sold_from_head,.sold_to_head,.sold_total_head{
            width:35px;
        }
    }

    @media only screen and (max-width: 330px) {
        .sold_set,.sold_code,.sold_from,.sold_to,.sold_total,.sold_set_head,.sold_code_head,.sold_from_head,.sold_to_head,.sold_total_head,.seriescode{
            width:25px;
            font-size: 8px;
        }
        .headtitle{
            color:#444;font-weight: bold;
            font-size: 13px;
        }
    }*/
</style>
<script>
    $(document).ready(function () {
        $('.seriescode').multiselect({
            nonSelectedText: 'Select Code',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '175px',
            placeholder: 'Select Code',
            maxHeight: 300,
            includeSelectAllOption: true,
            allSelectedText: 'All'
        });
    });

    function triggerMultiselectDesign() {
        $('.seriescode').multiselect({
            nonSelectedText: 'Select Code',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '175px',
            placeholder: 'Select Code',
            maxHeight: 300,
            includeSelectAllOption: true,
            allSelectedText: 'All'
        });
        
    }
</script>
<script type="text/javascript">
    function createsold(type) {
        $('.sold_from').css('border-color','');
        $('.sold_to').css('border-color','');
        var valid = true;
        var myarray = [];
        $('.sold_code,.sold_from,.sold_total').each(function () {
            if ($(this).val() === '') {
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().html('<strong>Warning!</strong> Found duplicate entries.').fadeOut(6000);
                return false;
            }
        });
        var sold_from_array = [];
        $(".sold_from").each(function() {
            if (sold_from_array.indexOf($(this).val().trim()) != -1 ) {
                $(this).css('border-color','red');
                valid = false;
                window.scrollTo(0, 0);
                myarray.push(valid);
                $('.alert-warning').show().html('<strong>Warning!</strong> Found duplicate entries.').fadeOut(6000);
            }
            sold_from_array.push($(this).val().trim());
        });
        
        var sold_to_array = [];
        $(".sold_to").each(function() {
            if ($(this).val().trim() != '') {
                if (sold_to_array.indexOf($(this).val().trim()) != -1 ) {
                    $(this).css('border-color','red');
                    valid = false;
                    window.scrollTo(0, 0);
                    myarray.push(valid);
                    $('.alert-warning').show().html('<strong>Warning!</strong> Found duplicate entries.').fadeOut(6000);
                }
                sold_to_array.push($(this).val().trim());
            }
        });
        
        if (myarray.length === 0) {
            var input = $("<input>")
                           .attr("type", "hidden")
                           .attr("name", "type").val("draft"); 
            if (type == 'final') {
                input = $("<input>")
                           .attr("type", "hidden")
                           .attr("name", "type").val("final");
                            $.confirm({
                                title: 'Are you sure?',
                                content: 'If you confirm data will be submitted to admin.',
                                buttons: {
                                    confirm: function () {
                                        $("#createsold").append(input);
                                        $("#createsold").submit();
                                    },
                                    cancel: function () {
//                                        $.alert('Canceled!');
                                    }
                                }
                            });
            } else {
                $("#createsold").append(input);
                $("#createsold").submit();
            }
        } else {
            return false;
        }
    }

    function back() {
        var url = "<?php echo base_url(); ?>unsold/listunsold";
        $(location).attr('href', url);
    }

</script>
<?php $status = $this->Lottery_model->isUploadBlocked("UNSOLD"); ?>


<section id="main-content">


    <section class="wrapper">

        <div class="table-agile-info">
            <input type="hidden" name="set_count" id="set_count" value="<?php echo $ticketcodes[0]['set_count']; ?>" />
            <form role="form" id="createsold" method="post" action="<?php echo base_url() ?>unsold/savesold" accept-charset="utf-8">
                <div class="panel panel-default">
                    <?php if ($status == "true") { ?>
                        <div style="color:red;font-size: 20px; font-weight: bold;padding-bottom: 20px;padding-left: 200px;">Please Contact Admin to remove the time block</div>
                    <?php } ?>
                    <?php if ($status == "false") { ?>
                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success">
                                <strong><?php echo $this->session->flashdata('message'); ?></strong>
                            </div>
                        <?php } ?>
                        <div class="alert alert-warning">
                            <strong>Warning!</strong> Please fill all the fields.
                        </div>
                        <?php if (empty($ticketlist)) { ?>
                            <div style="color:red;font-size: 18px;font-weight: bold;padding-bottom: 30px;">Admin did not set tickets for today</div>
                        <?php } ?>
                        <div class="panel-heading">
                            <div class="headtitle">TODAY'S UNSOLD DETAILS</div>
                            <span  class="btn btn-success btn-sm pull-RIGHT" id="Add_notice">
                                <span class="glyphicon button_text" >ADD</span>
                            </span>
                            <div style="float:left;">
                                Select Ticket: 
                                <select name="ticketname" id="ticketname" class="form-control select2" style="width: 200px;">
                                    <?php
                                    if (!empty($ticketlist)) {
                                        foreach ($ticketlist as $key => $value) {
                                            ?>
                                            <option <?php if (isset($selectedticket) && $selectedticket == $value['id']) { ?> selected="selected" <?php } ?>  value="<?php echo $value['id']; ?>"><?php echo $value['ticket_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div>

                            <table class="table" ui-jq="footable" ui-options='{
                                   "paging": {
                                   "enabled": true
                                   },
                                   "filtering": {
                                   "enabled": true
                                   },
                                   "sorting": {
                                   "enabled": true
                                   }}'>
                                <thead>
                                    <tr> 
<!--                                        <th class="sold_code_head">Code</th>-->
                                        <th class="sold_set_head">Set</th>
                                        <th class="sold_from_head">From</th>
                                        <th class="sold_to_head">To</th>
                                        <th class="sold_total_head">Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="solddata">
                                    <?php
                                    $total = 0;
                                    if (empty($unsold)) {
                                        ?>
                                        <tr class="soldeach">
                                            
<!--                                            <td><select class="form-control m-bot15 seriescode" name="sold[codeoptions][codeOpt1][]" multiple id ="codeOpt">
                                                    <?php
//                                                      echo
                                                    foreach ($singleticket as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value; ?>"><?php echo $value; ?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select></td>-->
                                            <td class="sold_set_td"><input type="checkbox" value="" id="sold_set_1" class="sold_set" name="sold[set][1]" value="1"></td>
                                                <td><input type="text" class="sold_from form-control" name="sold[from][1]"></td>
                                            <td><input type="text" class="sold_to form-control" name="sold[to][1]"></td>
                                            
                                            <td><input type="number" class="sold_total form-control" name="sold[total][1]" readonly></td>
                                            <td>
                                                <span class="btn btn-danger btn-sm deletenotice">
                                                    <span class="glyphicon button_text">Delete
                                                    </span>
                                                </span>
                                            </td>
                                        </tr>

                                        <?php
                                    } else {
                                        $inc = 1;
                                        
                                        foreach ($unsold as $ukey => $uvalue) {
                                            $checked = '';
                                            $disabled = '';
                                            $readonly = 'readonly';
                                            $total += $uvalue['total'];
                                            if ($uvalue['is_set'] == 'YES') {
                                                $checked = 'checked = "checked"';
                                                //$disabled = 'disabled = "disabled"';
                                                //$readonly = 'readonly';
                                                //$uvalue['ticket_to'] = '';
                                            }
//                                            echo '<pre>';
//                                            print_r($uvalue['ticket_code'])
                                            ?>

                                            <tr class="soldeach"> 
<!--                                                <td>
                                                    <select class="form-control m-bot15 seriescode" name="sold[codeoptions][codeOpt<?php echo $inc; ?>][]" multiple id ="codeOpt">
                                                        <?php
                                                        foreach ($singleticket as $key => $value) {
                                                            $datas['ticket_code'] = explode(',', $uvalue['ticket_code']);
//                                                            print_r($datas['ticket_code']);

                                                            $selected = in_array($value, $datas['ticket_code']) ? ' selected="selected" ' : '';
                                                            ?>
                                                            <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $value ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>-->
                                                <?php
                                                    $ticket_to = $uvalue['ticket_to'];
//                                                    if ($uvalue['ticket_to'] != '') {
//                                                        $ticket_to = $uvalue['ticket_code'].'-'.$uvalue['ticket_to'];
//                                                    }
                                                    $ticket_from = $uvalue['ticket_from'];
//                                                    if ($uvalue['ticket_from'] != '') {
//                                                        $ticket_from = $uvalue['ticket_code'].'-'.$uvalue['ticket_from'];
//                                                    }
                                                ?>
                                                <td class="sold_set_td"><input type="checkbox" value="" id="sold_set_<?php echo $inc; ?>" class="sold_set" name="sold[set][<?php echo $inc; ?>]" value="1" <?php echo $checked; ?>></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--                                                <td><input type="text" class="sold_code" name="sold[code][<?php echo $inc; ?>]"  value="<?php echo $uvalue['ticket_code']; ?>"></td>-->
                                                <td><input type="text" class="sold_from form-control" name="sold[from][<?php echo $inc; ?>]"  value="<?php echo $ticket_from; ?>"></td>
                                                <td><input type="text" class="sold_to form-control" name="sold[to][<?php echo $inc; ?>]"  value="<?php echo $ticket_to; ?>"  <?php echo $disabled; ?>></td>
                                                
                                                <td><input  type="number" class="sold_total form-control" name="sold[total][<?php echo $inc; ?>]"  value="<?php echo $uvalue['total']; ?>" <?php echo $readonly; ?>></td>
                                                <td>
                                                    <span class="btn btn-danger btn-sm deletenotice">
                                                        <span class="glyphicon button_text">Delete
                                                        </span>
                                                    </span>
                                                </td>
                                            </tr>
                                            <?php
                                            $inc++;
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        
                                        <td style="font-weight: bolder;font-size: 20px;"><b><i>Total Unsold: </i></b></td>
                                        <td style="color:red;font-weight: bolder;font-size: 20px;" id="sold_full_total"><?php echo $total; ?></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if (!empty($ticketlist)) { ?>
                                <div class="row">
                                    <?php if(isset($unsold[0]['is_draft']) && !$unsold[0]['is_draft'] == 0 || empty($unsold)){ ?>
                                    <button type="button" onclick="createsold('draft')" class="btn btn-warning" style="float:left;margin-left:20px">Draft Unsold</button>
                                        <?php if(!empty($unsold)){ ?>
                                        <button type="button" onclick="createsold('final')" class="btn btn-info" style="float:left;margin-left:100px">Submit Unsold</button>
                                        <?php } ?>
                                        
                                    <?php } else if(isset($unsold[0]['is_draft'])) { ?>
                                        <div style="color:red;font-weight: bolder;margin-left: 200px;font-size: 18px">Please Contact Admin to Edit Unsold</div>
                                    <?php } ?>
                                    <button type="button" onclick="back()" class="btn btn-info" style="float:right">Back</button>
                                </div>
                            <?php } ?>
                            
                            <!--                            </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </section>-->

                        </div>
                    <?php } ?>
                </div>
            </form>
            <form id="createsoldfinal" method="post" action="<?php echo base_url() ?>unsold/savesoldfinal" accept-charset="utf-8">
                <input type="hidden" name="selected_ticket" value="<?php echo $selectedticket; ?>" />
            </form>
        </div>
    </section>

</section>


<script type="text/javascript">
    $(document).ready(function () {
        
        $("#solddata").on("click", ".multiselect", function () {
                    
                    if($(this).parents('.soldeach').find('.sold_set').is(":checked")){
                        $(this).parents('.soldeach').find('.sold_set').prop("checked", false);
                        $(this).parents('.soldeach').find('.sold_total').val('');
                        $('#sold_full_total').text('');
                    }
        });
        
        
        sessionStorage.clear();
        $('.alert-success').show().fadeOut(2000);
        $('.alert-warning').hide();
//        $('input').keypress(function (event) {
        $("#solddata").on("keypress", "input", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
//            if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
//                if ($(this).hasClass('sold_to')) {
//                    if (keycode == '13') {
//                        $('#Add_notice').click();
//                        $('.sold_from:last').focus();
//                        return false; 
//                    }
//                } else if($(this).hasClass('sold_from')) {
//                    if (keycode == '13') {
//                        $(this).parents('.soldeach').find('.sold_to').focus();
//                        return false;
//                    }
//                }
//            } else {
//                if (keycode == '13') {
//                    $('#Add_notice').click();
//                    $('.sold_from:last').focus();
//                    return false;
//                }
//            }
//==========================================================
                if ($(this).hasClass('sold_to')) {
                    if (keycode == '13') {
                        $('#Add_notice').click();
                        $('.sold_from:last').focus();
                        return false; 
                    }
                } else if($(this).hasClass('sold_from')) {
                    if (keycode == '13') {
                        $(this).parents('.soldeach').find('.sold_to').focus();
                        return false;
                    }
                }
//====================================================================                
            if (keycode == '43') {
                $('#Add_notice').click();
                $('.sold_from:last').focus();
                return false;
            }
        });
        $("#addremarks").on("click", function () {
            $("#addremarksdiv").append("<div class='remarkeach'><div class='col-md-9' style='margin-bottom: 10px;'><input type='text' name='remarks[]' class='form-control'/></div><div class='col-md-2'><span  class='btn btn-info btn-sm deleteremarks'><span class='glyphicon glyphicon-minus-sign'></span></span><br></div></div>");
        });
        $("#addremarksdiv").on("click", ".remarkeach .deleteremarks", function () {
            $(this).parents('.remarkeach').remove();
        });
        $("#Add_notice").on("click", function () {

            var id = 1;
            if ($('.sold_set').length > 0) {
                var lastid = $('.sold_set:last').attr('id');
                var idarray = lastid.split('_');
                id = parseInt(idarray[2]) + 1;
            }


            $("#solddata").append("<tr class=\"soldeach\">\n\
                                                <td class=\"sold_set_td\">\n\
                                                     <input type=\"checkbox\" value=\"\" id=\"sold_set_" + id + "\" class=\"sold_set sold_set_td\" name=\"sold[set][" + id + "]\" value=\"1\">\n\
                                                 </td>\n\
                                                <td>\n\
                                                    <input type=\"text\" class=\"sold_from form-control\" name=\"sold[from][" + id + "]\">\n\
                                                </td>\n\                                        \n\
                                                <td>\n\
                                                    <input type=\"text\" class=\"sold_to form-control\" name=\"sold[to][" + id + "]\">\n\
                                                </td>\n\
                                                <td>\n\
                                                    <input type=\"number\" class=\"sold_total form-control\" name=\"sold[total][" + id + "]\" readonly>\n\
                                                </td>\n\
                                                    <td>\n\
                                                        <span class=\"btn btn-danger btn-sm deletenotice\">\n\
                                                             <span class=\"glyphicon button_text\">Delete\n\
                                                             </span>\n\
                                                        </span>\n\
                                                    </td>\n\
                                                    \n\
                                            </tr>");
            $("")
            var len = $(".soldeach").length;
            if (len == 0) {
                $('#notedissueheading').hide();
            } else {
                $('#notedissueheading').show();
            }
            triggerMultiselectDesign();
        });
        $("#solddata").on("click", ".soldeach .deletenotice", function () {
            $(this).parents('.soldeach').remove();
            var len = $(".soldeach").length;
            if (len == 0) {
                $('#notedissueheading').hide();
            } else {
                $('#notedissueheading').show();
            }
            var sum = 0;
            $('.sold_total').each(function () {
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            $('#sold_full_total').text(sum);
        });
        $("#solddata").on("click", ".soldeach .sold_set", function () {
            if ($(this).prop('checked') == true) {
                // var selectedcount = $(this).parents('.soldeach').find('.checkbox :checked').length;
                //alert(selectedcount);
                sessionStorage.setItem("last_sold_to", $(this).parents('.soldeach').find('.sold_to').val());
                //$(this).parents('.soldeach').find('.sold_to').prop('disabled', 'disabled');
                //$(this).parents('.soldeach').find('.sold_to').val('');
                // $(this).parents('.soldeach').find('.sold_total').removeAttr('readonly');
                $(this).parents('.soldeach').find('.sold_total').prop('readonly', true);
                //$(this).parents('.soldeach').find('.sold_total').val($('#set_count').val()*selectedcount);
                
                //changed due to latest change
                var toval = sessionStorage.getItem("last_sold_to").split("-").pop();
                var fromval = $(this).parents('.soldeach').find('.sold_from').val().split("-").pop();
                var totaldiffcount;

                if(toval == ""){
                        totaldiffcount = $('#set_count').val();
                        
                } else {
                        var uncheck_total = parseInt(toval)-parseInt(fromval);
                        totaldiffcount = parseInt((uncheck_total+1))*parseInt($('#set_count').val());
                }
                
                
             //  if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                $(this).parents('.soldeach').find('.sold_total').val(totaldiffcount);
                
                
                
                //$(this).parents('.soldeach').find('.sold_total').val($('#set_count').val());
            } else {
//                if($(this).parents('.soldeach').find('.sold_to').val() == ''){
//                    $(this).parents('.soldeach').find('.sold_total').val('1');
//                }
                //$(this).parents('.soldeach').find('.sold_to').removeAttr('disabled');
                //$(this).parents('.soldeach').find('.sold_to').val(sessionStorage.getItem("last_sold_to"));
                //var fromval = sessionStorage.getItem("last_sold_to").split("-").pop();
                
                var fromval = $(this).parents('.soldeach').find('.sold_from').val().split("-").pop();
                var toval = $(this).parents('.soldeach').find('.sold_to').val().split("-").pop();
                $(this).parents('.soldeach').find('.sold_total').prop('readonly', true);
                
                var uncheck_total = parseInt(toval)-parseInt(fromval);

                if($(this).parents('.soldeach').find('.sold_to').val() == ''){
                    $(this).parents('.soldeach').find('.sold_total').val('1');
                } else {
                    $(this).parents('.soldeach').find('.sold_total').val(uncheck_total+1);
                }

        }

            
            var sum = 0;
            $('.sold_total').each(function () {
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
                $('#sold_full_total').text(sum);
        });
        $("#solddata").on('keyup', ".soldeach .sold_from, .soldeach .sold_to", function (e) {
//            var from = $(this).parents('.soldeach').find('.sold_from').val();
//            var to = $(this).parents('.soldeach').find('.sold_to').val();
            var from = $(this).parents('.soldeach').find('.sold_from').val().split("-").pop();
            var to = $(this).parents('.soldeach').find('.sold_to').val().split("-").pop();
            var total = parseInt(to) - parseInt(from) + 1;
            
            if(from !='' && to ==''){
                total = parseInt('1');
            } else if(from =='' && to !=''){
                total = parseInt('1');
            }
            if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                $(this).parents('.soldeach').find('.sold_total').val(total);
            }
  //=======================================================================================================          
            if($(this).parents('.soldeach').find('.sold_set').is(":checked")){
                //changed due to latest change
                var fromval = $(this).parents('.soldeach').find('.sold_from').val().split("-").pop();;
                var toval = $(this).parents('.soldeach').find('.sold_to').val().split("-").pop();
                var totaldiffcount;

                if(toval == ""){
                        totaldiffcount = $('#set_count').val();
                        
                } else {
                        var uncheck_total = parseInt(toval)-parseInt(fromval);
                        totaldiffcount = parseInt((uncheck_total+1))*parseInt($('#set_count').val());
                }
                
                
             //  if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                $(this).parents('.soldeach').find('.sold_total').val(totaldiffcount);
            }
//===========================================================END ===============================================

            var sum = 0;
            $('.sold_total').each(function () {
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
//            if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
//                $('#sold_full_total').text(sum);
//            }
            
         //   if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                $('#sold_full_total').text(sum);
          //  }
        });
        $("#solddata").on('change', ".soldeach .sold_from, .soldeach .sold_to", function (e) {
            var from = $(this).parents('.soldeach').find('.sold_from').val().split("-").pop();
            if ($(this).parents('.soldeach').find('.sold_to').val().trim() != '') {
                var to = $(this).parents('.soldeach').find('.sold_to').val().split("-").pop();
                var total = parseInt(to) - parseInt(from) + 1;
                if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                    $(this).parents('.soldeach').find('.sold_total').val(total);
                }
            } else {
                if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                    $(this).parents('.soldeach').find('.sold_total').val('1');
                }
            }
            var sum = 0;
            $('.sold_total').each(function () {
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                $('#sold_full_total').text(sum);
            }
        });
        $("#solddata").on('keyup', ".soldeach .sold_total", function (e) {
            var sum = 0;
            $('.sold_total').each(function () {
                if (this.value != '') {
                    sum += parseInt(this.value);
                }
            });
            
//            if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
//                    $('#sold_full_total').text(sum);
//            }
          //  if(!$(this).parents('.soldeach').find('.sold_set').is(":checked")){
                    $('#sold_full_total').text(sum);
          //  }
        });
        $("#ticketname").on('change', function (e) {
            var url = "<?php echo base_url(); ?>unsold/create/" + $(this).val();
            $(location).attr('href', url);
        });
    });</script>


<?php
$this->load->view('footer');
?>