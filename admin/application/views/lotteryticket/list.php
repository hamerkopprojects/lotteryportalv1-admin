<?php
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.alert-info').show().fadeOut(2000);
    });
</script>
<script>
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        //$('#datepicker').datepicker().datepicker('setDate', 'today');
    });


</script>
<section id="main-content">

    <section class="wrapper">
        <div class="table-agile-info">

            <div class="panel panel-default">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-info">
                        <strong><?php echo $this->session->flashdata('message'); ?></strong>
                    </div>
                <?php } ?>
                <div class="panel-heading" >
                    <b  style="color:#444"> Ticket Listing</b></div>


                <div class="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Ticket name</th>
                                <th>Draw code</th>
                                <th>Day</th>
                                <th>Date</th>
                                <th>Set count</th>
                                <th>Price</th>
                                <th>Ticket code</th>
                            </tr>
                        </thead>
                        <tbody id="fbody">
                            <?php
                            $i = 0;
                            foreach ($ticketdetails as $k => $v) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                            <form id="tickets_update_<?php echo $ticketdetails[$k]['id'] ?>" action="<?php echo base_url(); ?>ticket/updateticket_details/<?php echo $ticketdetails[$k]['id'] ?>" method="post">

                                <div class="form-group col-md-3">
                                    <!--  <label for="description">Description</label-->

                                    <td>

                                        <input name="ticket_name" type="text" class="form-control" id="ticket_name_<?php echo $i; ?>" value="<?php echo $ticketdetails[$k]['ticket_name'] ?>" >
                                    </td>
                                </div>

                                <td><input name="draw_code" type="text" style="width: 100px;" class="form-control" id="draw_code" value="<?php echo $ticketdetails[$k]['draw_code'] ?>">
                                </td>
                                <td><select name="day" type="text" class="form-control" id="day" disabled="disabled">
                                        <option value="SUN" <?php echo ($ticketdetails[$k]['day'] == "SUN") ? 'selected=selected' : ''; ?>>SUNDAY</option>
                                        <option value="MON" <?php echo ($ticketdetails[$k]['day'] == "MON") ? 'selected=selected' : ''; ?>>MONDAY</option>
                                        <option value="TUE" <?php echo ($ticketdetails[$k]['day'] == "TUE") ? 'selected=selected' : ''; ?>>TUESDAY</option>
                                        <option value="WED" <?php echo ($ticketdetails[$k]['day'] == "WED") ? 'selected=selected' : ''; ?>>WEDNESDAY</option>
                                        <option value="THU" <?php echo ($ticketdetails[$k]['day'] == "THU") ? 'selected=selected' : ''; ?>>THURSDAY</option>
                                        <option value="FRI" <?php echo ($ticketdetails[$k]['day'] == "FRI") ? 'selected=selected' : ''; ?>>FRIDAY</option>
                                        <option value=SAT" <?php echo ($ticketdetails[$k]['day'] == "SAT") ? 'selected=selected' : ''; ?>>SATURDAY</option>
                                        <option value=OTHERS" <?php echo ($ticketdetails[$k]['day'] == "OTHERS") ? 'selected=selected' : ''; ?>>OTHERS</option>
                                    </select>

                                </td>
                                <td> <input name="date" type="text" style="width: 110px;" class="form-control datepicker" value="<?php echo date('d-m-Y', strtotime($ticketdetails[$k]['date'])) ?>">
                                </td>
                                <td> <input name="set_count" type="number" style="width: 70px;" class="form-control" value="<?php echo $ticketdetails[$k]['set_count'] ?>">
                                     <td> <input name="cost" type="number" style="width: 70px;" class="form-control" value="<?php echo $ticketdetails[$k]['cost'] ?>">
                                </td>
    <!--                               <td> <textarea class="form-control" id="ticket_code" name="ticket_code" rows="1"><?php echo $ticketdetails[$k]['ticket_code'] ?></textarea>-->
                                </td>
                                <td><button type="submit" class="btn btn-success" onclick=" updateticket(<?php echo $ticketdetails[$k]['id'] ?>)">Update</button>
                                </td
                                </tr>
                            </form>
                        <?php } ?>
                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>


        </div>
    </section>
    <!-- footer -->

    <!-- / footer -->
</section>

<script type="text/javascript">
    $(function () {
        $('textarea').on('keypress', function (e) {
            if (e.which == 32)
                return false;
        });
    });
    function updateticket(id)
    {
        $('#tickets_update_' + id).submit();
    }

</script>

<?php
$this->load->view('footer');
?>

