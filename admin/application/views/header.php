<head>
    <title><?php echo project_name; ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" >
    <link href="<?php echo base_url(); ?>css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet"/>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/font.css" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery.multiselect.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/morris.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/monthly.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/unsold-loader.css">
    <script src="<?php echo base_url(); ?>js/jquery2.0.3.min.js"></script>
    <script src="<?php echo base_url(); ?>js/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>js/morris.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/timepicker/bootstrap-timepicker.min.css">

    <link href="<?php echo base_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<!--    
 Bootstrap Date-Picker Plugin -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>-->
    <style>
        .blink{
            padding-left: 200px;
            text-align: center;
        }
        .ticketconf {
            font-size: 25px;
            color: white;
            animation: blink 1s linear infinite;
        }
        @keyframes blink{
            0%{opacity: 0;}
            50%{opacity: .5;}
            100%{opacity: 1;}
        }



        @media only screen and (max-width: 800px) {
            .blink {
                padding-left: 200px;
                text-align: center;
            }

            .ticketconf {
                font-size: 25px;
                color: white;
                animation: blink 1s linear infinite;
            }

            .resultlink {
                float:left;
                color:white;
                margin-top: 27px;
                margin-left: 54px

            }
        }

        @media only screen and (max-width: 360px) {
            .blink {
                text-align: center;
            }

            .ticketconf {
                font-size: 15px;
                color: white;
                animation: blink 1s linear infinite;
            }

            .resultlink {
                float:left;
                color:white;
                margin-left: 54px

            }
        }
    </style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/admin-responsive.css" >
</head>
<body style="font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif">

    <?php
    $username = $this->session->userdata['user']['username'];
    $firstname = $this->session->userdata['user']['firstname'];
    $usertype = $this->session->userdata['user']['user_type'];
    ?>
    <section id="container">
        <header class="header fixed-top clearfix">
            <div class="brand" style="background:#3c8dbc!important;">
                <?php
                if ($usertype == 'SUPERADMIN' || $usertype == 'ADMIN' || $usertype == 'SHOP' || $usertype == 'TECHNICIAN') {
                    $url = base_url() . 'dashboard/home';
                } else {
                    $url = base_url() . 'user/received_user_file';
                }
                ?>
                <a href="<?php echo $url; ?>" class="logo">
                    <?php echo project_short_name; ?>
                </a>

                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
            <?php if (!isset($_GET['platform'])) { ?>
                <div class="top-nav clearfix">

                    <ul class="nav pull-right top-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                                <span class="username" style="font-size: 15px; font-weight: bold"><?php echo $firstname; ?></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <li><a href="<?php echo base_url(); ?>user/logout"><i class="fa fa-key" style="color: #444"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            <?php } ?>
        </header>

        <?php
        if ($usertype == 'SUPERADMIN' || $usertype == 'TECHNICIAN') {
            ?>

            <aside>
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-user"></i>
                                    <span>User management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>user/create_users">Create User</a></li>
                                    <li><a href="<?php echo base_url() ?>user/view_users">Users List</a></li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>File management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>user/create_user_file">Upload File</a></li>
                                    <li><a href="<?php echo base_url() ?>user/assigned_user_file">Files Sent</a></li>
                                    <li><a href="<?php echo base_url() ?>user/received_user_file">Files Received</a></li>
                                </ul>
                            </li>

                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-table"></i>
                                    <span>Unsold management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldconsolidated?date=<?php echo date('Y-m-d'); ?>&&type=RETAIL">Daily Unsold</a></li>
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldconsolidated/<?php echo date('Y-m-d'); ?>/WHOLESALE">Daily Wholesale Unsold</a></li>-->
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsold/<?php echo date('Y-m-d'); ?>">Agency/Shop Unsold</a></li>-->
                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldwinning/<?php echo date('Y-m-d'); ?>/RETAIL">Daily Winning</a></li>
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldwinning/<?php echo date('Y-m-d'); ?>/WHOLESALE">Daily Wholesale Unsold Winning</a></li>-->
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/unsold_details/<?php echo date('Y-m-d'); ?>/RETAIL">Unsold Details</a></li>-->

                                </ul>

                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>DC Management</span>
                                </a>
                                <ul class="sub">
    <!--                                    <li><a href="<?php echo base_url() ?>Dcmanagement/list_dailydc/<?php echo date('Y-m-d'); ?>"> Daily DC</a></li>-->
                                    <li><a href="<?php echo base_url() ?>Dcmanagement/dc_details?date=<?php echo date('Y-m-d'); ?>"> DC Details</a></li>

                                </ul>
                            </li>

                            <?php
                            if ($usertype == 'SUPERADMIN' || $usertype == 'ADMIN' || $usertype == 'TECHNICIAN') {
                                ?>                            
                                <li class="sub-menu">
                                    <a href="javascript:;">
                                        <i class="glyphicon glyphicon-home"></i>
                                        <span>Shops Management</span>
                                    </a>
                                    <ul class="sub">
                                        <li><a href="<?php echo base_url() ?>dashboard/shoplist">Live Statements</a></li>

                                    </ul>
                                </li>
                                <li class="sub-menu">
                                    <a href="javascript:;">
                                        <i class="fa fa-table"></i>
                                        <span>Result Management</span>
                                    </a>                                    
                                    <ul class="sub">
                                        <li><a href="<?php echo base_url() ?>result/searchresult">Check Ticket Winning</a></li>
                                        <li><a href="<?php echo base_url() ?>dashboard/results?platform=web">Results</a></li>

                                    </ul>
                                </li>

                                <li class="sub-menu">
                                    <a href="javascript:;">
                                        <i class="fa fa-cogs"></i>
                                        <span>Configuration</span>
                                    </a>
                                    <ul class="sub">
                                        <li><a href="<?php echo base_url() ?>ticket/index">Set Daily Tickets</a></li>
                                        <li><a href="<?php echo base_url() ?>user/timesettings">Set Upload Time</a></li>
                                    </ul>
                                </li>
                                <?php
                            } if ($usertype == 'TECHNICIAN') {
                                ?>
                                <li class="sub-menu">
                                    <a href="javascript:;">
                                        <i class="fa fa-cogs"></i>
                                        <span>Expiry management</span>
                                    </a>
                                    <ul class="sub">
                                        <li><a href="<?php echo base_url() ?>Dashboard/create_domains">Create Domain</a>
                                        <li><a href="<?php echo base_url() ?>Dashboard/clientexpiry_details">Expiry Details</a>
                                    </ul>

                                </li>
                                <?php }
                            ?>             
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>user/logout">
                                    <i class="glyphicon glyphicon-off"></i>
                                    <span>&nbspLogout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
            <?php
        } else if ($usertype == 'ADMIN') {
            ?>

            <aside>
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-user"></i>
                                    <span>User management</span>
                                </a>
                                <ul class="sub">

                                    <li><a href="<?php echo base_url() ?>user/view_users">Users List</a></li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>File management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>user/create_user_file">Upload File</a></li>
                                    <li><a href="<?php echo base_url() ?>user/assigned_user_file">Files Sent</a></li>
                                    <li><a href="<?php echo base_url() ?>user/received_user_file">Files Received</a></li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-table"></i>
                                    <span>Unsold management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldconsolidated/<?php echo date('Y-m-d'); ?>/RETAIL">Daily Unsold</a></li>
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldconsolidated/<?php echo date('Y-m-d'); ?>/WHOLESALE">Daily Wholesale Unsold</a></li>-->
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsold/<?php echo date('Y-m-d'); ?>">Agency/Shop Unsold</a></li>-->
                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldwinning/<?php echo date('Y-m-d'); ?>/RETAIL">Daily Winning</a></li>
    <!--                                    <li><a href="<?php echo base_url() ?>unsold/listunsoldwinning/<?php echo date('Y-m-d'); ?>/WHOLESALE">Daily Wholesale Unsold Winning</a></li>-->
<!--                                    <li><a href="<?php echo base_url() ?>unsold/unsold_details/<?php echo date('Y-m-d'); ?>/RETAIL">Unsold Details</a></li>-->

                                </ul>

                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>DC Management</span>
                                </a>
                                <ul class="sub">
    <!--                                    <li><a href="<?php echo base_url() ?>Dcmanagement/list_dailydc/<?php echo date('Y-m-d'); ?>"> Daily DC</a></li>-->
                                    <li><a href="<?php echo base_url() ?>Dcmanagement/dc_details?date=<?php echo date('Y-m-d'); ?>"> DC Details</a></li>

                                </ul>
                            </li>                            
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="glyphicon glyphicon-home"></i>
                                    <span>Shops Management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>dashboard/shoplist">Live Statements</a></li>

                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-cogs"></i>
                                    <span>Configuration</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>ticket/index">Set Daily Tickets</a></li>
                                    <li><a href="<?php echo base_url() ?>user/timesettings">Set Upload Time</a></li>

                                </ul>
                            </li>
                             <li class="sub-menu">
                                <a href="<?php echo base_url() ?>user/logout">
                                    <i class="glyphicon glyphicon-off"></i>
                                    <span>&nbspLogout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>


            <?php
        } else if ($usertype == 'SHOP') {
            ?>
            <aside>
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>User File management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>user/create_user_file">Upload File</a></li>
                                    <li><a href="<?php echo base_url() ?>user/assigned_user_file">Files Sent</a></li>
                                    <li><a href="<?php echo base_url() ?>user/received_user_file">Files Received</a></li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>DC Management</span>
                                </a>
                                <ul class="sub">
    <!--                                    <li><a href="<?php echo base_url() ?>Dcmanagement/list_dailydc/<?php echo date('Y-m-d'); ?>"> Daily DC</a></li>-->
                                    <li><a href="<?php echo base_url() ?>Dcmanagement/dc_details?date=<?php echo date('Y-m-d'); ?>"> DC Details</a></li>

                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>dashboard/results?platform=web">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Results</span>
                                </a>
                            </li>
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>user/logout">
                                    <i class="glyphicon glyphicon-off"></i>
                                    <span>&nbspLogout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
            <?php
        } else if ($usertype == 'AGENT') {
            ?>
            <aside>
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>User File management</span>
                                </a>
                                <ul class="sub">
                                    <li><a href="<?php echo base_url() ?>user/create_user_file">Upload File</a></li>
                                    <li><a href="<?php echo base_url() ?>user/assigned_user_file">Files Sent</a></li>
                                    <li><a href="<?php echo base_url() ?>user/received_user_file">Files Received</a></li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>dashboard/results?platform=web">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Results</span>
                                </a>
                            </li>
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>user/logout">
                                    <i class="glyphicon glyphicon-off"></i>
                                    <span>&nbspLogout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>

            <?php
        } else if ($usertype == 'OTHERS') {
            ?>
            <aside>
                <div id="sidebar" class="nav-collapse">
                    <div class="leftside-navigation">
                        <ul class="sidebar-menu" id="nav-accordion">
                            <li class="sub-menu">
                                <a href="javascript:;">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>User File management</span>
                                </a>
                                <ul class="sub">

                                    <li><a href="<?php echo base_url() ?>user/received_user_file">Files Received</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>dashboard/results?platform=web">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Results</span>
                                </a>
                            </li>

                            <li class="sub-menu">
                                <a href="<?php echo base_url() ?>user/logout">
                                    <i class="glyphicon glyphicon-off"></i>
                                    <span>&nbspLogout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
            <?php }
        ?>
    </section>