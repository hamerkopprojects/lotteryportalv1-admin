<?php
if ($_GET['platform'] != 'app') {
    $this->load->view('header');
} else {
    $this->load->view('header-agent');
}
?>
<style>
    @media only screen and (max-width: 600px) {
        iframe {
            width: 100%;
            height: 250%;
        }
    }
    @media only screen and (min-width: 600px) {
        iframe {
            width: 100%;
            height: 180%;
        }
    </style>
    <style>table 
        {  font-family: arial, sans-serif;  border-collapse: collapse;  width: 100%; font-weight: bolder;}
        td, th {  border: 1px solid #dddddd;    padding: 8px;}
        tr.break td {
            height: 10px;
            tr { width:100%; }
        }
        /*tr:nth-child(even) { background-color: #dddddd;}*/
    </style> 
<section id="main-content">
    <section class="wrapper">


        <div class="panel panel-default">
<!--            <div class="alert alert-warning">
                <strong>Warning!</strong> Please fill all the fields.
            </div>-->
        <?php 
  if ($_GET['platform'] != 'app') {
  ?>
                <div class="panel-heading">
                <div class="row" >
                    <div class="col-md-8" ><b style="color:#444">Search Winning By Ticket</b></div>
                </div>
            </div>  
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php $ticketnumber = (isset($_GET['ticketnumber']))?$_GET['ticketnumber']:''?>
                <b>Search by Ticket: </b><input type="text" class="form-control" id="ticketnumber" name="ticketnumber"  value="<?php echo $ticketnumber; ?>" placeholder="Search by Ticket">
            </div>
            <div class="col-md-3">
                 <?php $tickettype = (isset($_GET['tickettype']))?$_GET['tickettype']:'WEEKLY'?>
                <b>Ticket Type: </b>
                <select id="tickettype" id="tickettype" class="form-control select2">
                    <option value="WEEKLY" <?php ($tickettype == 'WEEKLY')?'selected=seleted':''; ?> >WEEKLY</option>
                    <option value="BUMPER" <?php ($tickettype == 'BUMPER')?'selected=seleted':''; ?> >BUMPER</option>
                    
                </select>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top: 25px;">
                    <label for="lastname"></label>
                    <input type="button" class="btn btn-info" style="font-weight:bolder!important;" value="Search" onclick="search();">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group" style="padding-top: 25px;">
                    <label for="lastname"></label>
                    <input type="button" class="btn btn-warning" style="font-weight:bolder!important;" value="Reset" onclick="reset();">
                </div>
            </div>
        </div>
  <?php } ?>
            <div class="table-responsive">
                <table class="table" ui-jq="footable" ui-options='{
                       "paging": {
                       "enabled": true
                       },
                       "filtering": {
                       "enabled": true
                       },
                       "sorting": {
                       "enabled": true
                       }}'>

                    <tbody id="fbody">


                        <?php
                        //echo '<pre>'; print_r($details); exit;
                        $i = 0;
                        $j = 0;
                       
                        foreach ($details as $key) {
                            $j++;
                            $newDate = $details[$i]['date'];
                            $newDate = date("d-m-Y ", strtotime($newDate));
                            $color = ($j % 2 == 0) ? "#ddede0" : "#ffff";
                            ?>
                        <?php if($j == 1){ ?>
<!--                        <tr>
                            <td width="10%" style="font-size:30px;background-color: black;color:white;"></td>
                            <td width="10%" style="font-size:30px;background-color: black;color:white;"></td>
                            <td width="10%" style="font-size:30px;background-color: black;color:white;"><?php echo $details[$i]['ticket_name']; ?></td>
                            <td width="10%" style="font-size:30px;background-color: black;color:white;"></td>
                            <td width="10%" style="font-size:30px;background-color: black;color:white;"></td>                            
                        </tr>
                        <thead>
                        <tr>
                            <th width="10%">Draw Date</th>
                            <th width="10%">Draw Code</th>
                            <th width="10%">Ticket Name</th>
                            <th width="10%">Ticket Number</th>
                            <th width="10%">Won Ticket</th>
                            <th width="10%">Prize Rank</th>
                            <th width="10%">Prize Amount</th>
                            
                        </tr>
                    </thead>-->
                    
                        <table>
                            <tr align="center">
                                <td style="background-color: black;font-size: 20px;color:#ffff;">&nbsp;</td>
                                <td style="background-color: black;font-size: 20px;color:#ffff;" colspan="2"><?php echo $details[$i]['ticket_name']; ?></td>
                                <td style="background-color: black;font-size: 20px;color:#ffff;">&nbsp;</td>
                            </tr>
                        </table>
                                           <table>
                                                <tr align="center">
                                                    <td style="font-size: 25px;color:#1F4071;" colspan="2"><?php echo $newDate; ?></td>
                                                    <td style="font-size: 25px;color:#ff6600;" colspan="1"><?php echo $details[$i]['won_ticket']; ?></td>
                                                    <td style="background-color: #47d147;font-size: 25px;color:#ffff;" colspan="2"><?php echo $details[$i]['prize'].'-'.$details[$i]['prize_amount']; ?></td>
                                                    <td style="font-size: 25px;color:#CF57E7;" colspan="2"><?php echo $details[$i]['drawcode']; ?></td>
                                                </tr>
                                           </table>                    
                        <?php } else { ?>
                    
                                          <table>
                                                <tr align="center">
                                                    <td style="font-size: 20px;color:#1F4071;" colspan="2"><?php echo $newDate; ?></td>
                                                    <td style="font-size: 20px;color:#ff6600;" colspan="1"><?php echo $details[$i]['won_ticket']; ?></td>
                                                    <td style="background-color: #3AE6E1;font-size: 20px;color:#ffff;" colspan="2"><?php echo $details[$i]['prize'].'-'.$details[$i]['prize_amount']; ?></td>
                                                    <td style="font-size: 20px;color:#CF57E7;" colspan="2"><?php echo $details[$i]['drawcode']; ?></td>
                                                </tr>
                                           </table>
                        <?php } ?>
<!--                            <tr style ="background-color: <?php echo $color; ?>!important;">
                                <td width="25%" style="font-weight:bolder;font-size: 25px;color:yellow;"><b><?php echo $newDate; ?></b></td>
                                <td width="25%" style="font-weight:bolder;font-size: 25px;color:violet;"><b><?php echo $details[$i]['drawcode']; ?></b></td>
                                <td width="10%" style="font-weight:bolder;font-size: 15px"><b><?php echo $details[$i]['ticket_name']; ?></b></td>  
                                <td width="10%" style="font-weight:bolder;font-size: 15px"><b><?php echo $details[$i]['ticket_number']; ?></b></td>
                                <td width="25%" style="font-weight:bolder;font-size: 25px;color:#3AE6E1;"><b><?php echo $details[$i]['prize']; ?></b></td>
                                <td width="25%" style="font-weight:bolder;font-size: 25px;color:red;"><b><?php echo $details[$i]['prize_amount']; ?></b></td>

                            </tr>-->
                        <?php 
                        $i++;
                        } 
                        ?>
                    </tbody>
                </table>


            </div>
    </section>
    <script type="text/javascript">
        function search() {
            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');


            var url = "<?php echo base_url(); ?>result/searchresult?ticketnumber=" + $('#ticketnumber').val()+ "&&tickettype=" + $('#tickettype').val()+"&&platform=web";
            $(location).attr('href', url);

        }
        function reset() {
            jQuery("body").prepend('<div class="loading">Loading&#8230;dgfgdfgdfgdfgf</div>');


            var url = "<?php echo base_url(); ?>result/searchresult";
            $(location).attr('href', url);

        }
    </script>
    <?php
    $this->load->view('footer');
    ?>
