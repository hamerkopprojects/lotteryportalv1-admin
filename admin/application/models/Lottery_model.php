<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lottery_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $lastQuery = '';

    public function login($username, $password) {
        $query = $this->db->get_where('user', array('username' => $username, 'password' => $password, 'deleted_at' => NULL));
        return $query->row_array();
    }

    public function insert_users($data) {
        $insert = $this->db->insert("user", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function get_users_list($limit, $start) {
        //$this->db->limit($limit, $start);
        $this->db->where("user_type !=", "SUPERADMIN");
        $this->db->where("user_type !=", "TECHNICIAN");
        $this->db->order_by("user.shop_code", "asc");
        $query = $this->db->get("user");
        $this->lastQuery = $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function get_super_user_list() {
        $this->db->where("user_type =", "SUPERADMIN");
        $query = $this->db->get("user");
        $this->lastQuery = $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }

        print_t($query->row_array());
    }

    public function get_user($userid = array()) {
        $this->db->select('id,username,firstname,lastname,password,user_type,shop_code');
        $this->db->where('id', $userid['userid']);
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }

    public function update_user($condition = array(), $datapassed) {
        $date = date('Y-m-d H:i:s');
        //   $usertypearr = array();
        $data = array(
            'username' => trim($datapassed['username']),
            'firstname' => $datapassed['firstname'],
            'lastname' => $datapassed['lastname'],
            'password' => trim($datapassed['password']),
            //'user_type' => $datapassed['usertype'],
            //'shop_code' => $datapassed['shop_code'],
            'created_at' => $date,
            'updated_at' => $date
        );
        if ($datapassed['usertype'] != "") {

            $usertypearr = array(
                'user_type' => $datapassed['usertype']
            );
            $data = array_merge($data, $usertypearr);
            
            if($datapassed['usertype'] == 'AGENT'){
              $shopcodearr = array('shop_code' => $datapassed['shop_id']);
            } else {
              $shopcodearr = array('shop_code' => $datapassed['shop_code']); 
            }
            $data = array_merge($data, $shopcodearr);
        }
        
        //echo '<pre>'; print_r($data); exit;

        $this->db->where('id', $condition['id']);
        $this->db->update('user', $data);
    }

    public function delete_user($userid, $type) {
        $this->db->where('id', $userid);
        $data['deleted_at'] = NULL;
        if ($type == 'block') {
            $data['deleted_at'] = date('Y-m-d H:i:s');
        }

        $this->db->update('user', $data);
        // $this->db->delete('user');
    }

    public function username_exists($username) {
        $this->db->select('username');
        $this->db->from('user');
        $this->db->where('username', $username);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function selectuser() {

        $this->db->select('id,username,user_type');
        $this->db->order_by("user.user_type", "asc");
        $this->db->from('user');
        //$this->db->where("user.id!=", $id);
        $this->db->where("user.user_type!=", 'TECHNICIAN');
        $this->db->where("user.deleted_at =", NULL);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function selectusers($id) {
//        echo $id;
        $this->db->select('id,username,user_type');
        $this->db->order_by("user.user_type", "asc");
        $this->db->from('user');
        $this->db->where("user.id!=", $id);
        $this->db->where("user.user_type!=", 'TECHNICIAN');
        $this->db->where("user.deleted_at =", NULL);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function user_file_upload($data) {
        $insert = $this->db->insert("user_files", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function user_update_file_upload($data, $fileid) {
        $this->db->where('id', $fileid);
        $this->db->update('user_files', $data);
    }

    public function insert_userviewable($filedetails) {
        $this->db->insert("user_files_viewable", $filedetails);
    }

    public function assigned_files_view($id, $user_type) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('user_files', 'user.id = user_files.user_id');
        $this->db->join('user_files_viewable', 'user_files_viewable.user_files_id = user_files.id');
        $this->db->where("user.id", $id);
        $this->db->group_by('user_files.id');
        $this->db->where("user.user_type", $user_type)->order_by('user_files.created_at', 'desc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function received_files_view($id, $user_type, $details) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('user_files', 'user.id = user_files.user_id');
        $this->db->join('user_files_viewable', 'user_files_viewable.user_files_id = user_files.id', 'left');



        if ($user_type != "ADMIN" && $user_type != "SUPERADMIN") {
            $this->db->where("user_files_viewable.user_id =", $id);
        }

        if (isset($details['assign_from']) && $details['assign_from'] != "") {
            $this->db->where("user_files.user_id =", $details['assign_from']);
        }

        if (isset($details['assign_to']) && $details['assign_to'] != "") {
            $this->db->where("user_files_viewable.user_id =", $details['assign_to']);
        }

        if (isset($details['file_search']) && $details['file_search'] != "") {
            $this->db->where("user_files.file_name =", $details['assign_to']);
        }

        if (isset($details['date']) && $details['date'] != "") {
            $fromdate = $details['date'] . ' ' . '00:00:00';
            $todate = $details['date'] . ' ' . '23:59:59';
            $this->db->where('user_files.created_at BETWEEN "' . $fromdate . '" and "' . $todate . '"');
        } else {
            // echo 'dfsdf'; exit;
            $fromdate = date('Y-m-d 00:00:00');
            $todate = date('Y-m-d  23:59:59');

            $this->db->where('user_files.created_at BETWEEN "' . $fromdate . '" and "' . $todate . '"');
        }


        $this->db->order_by('user_files.created_at', 'desc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function user_details($id) {

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user.id", $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function received_user_details($id) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user.id", $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function assigneduser($uid) {
        $this->db->select('user_id');
        $this->db->from('user_files_viewable');
        $this->db->where('user_files_viewable.user_files_id', $uid);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function assignedfiles($fid) {
        $this->db->select('file_name,file_path', 'id');
        $this->db->from('user_files');
        $this->db->where('user_files.id', $fid);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function delete_user_without_file($id) {

        $this->db->where('user_files_id', $id);
        $this->db->delete('user_files_viewable');
    }

    public function delete_user_with_files($fid) {

        $this->db->where('user_files_id', $fid);
        $this->db->delete('user_files_viewable');
        $this->db->where('id', $fid);
        $this->db->delete('user_files');
    }

    public function delete_user_with_update_files($fid) {

        $this->db->where('user_files_id', $fid);
        $this->db->delete('user_files_viewable');
    }

    public function received_userdata($Details = array(), $id, $user_type) {

        $from = $Details['assign_from'];
        $to = $Details['assign_to'];
        $date = $Details['date'];

        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('user_files', 'user.id = user_files.user_id');
        $this->db->join('user_files_viewable', 'user_files_viewable.user_files_id = user_files.id', 'left');

        if ($user_type != "ADMIN") {

            $this->db->where("user_files_viewable.user_id =", $id);
        }

        $this->db->order_by('user_files.created_at', 'desc');

        if ($from != '') {
            $this->db->where("user_files.user_id", $from);
        }

        if ($to != '') {
            $this->db->where("user_files_viewable.user_id", $to);
        }


        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getTotalRow() {

        $sql = explode('LIMIT', $this->lastQuery);
        $query = $this->db->query($sql[0]);
        $result = $query->result();
        return count($result);
    }

    public function download($id) {

        $this->db->select('*');
        $this->db->from('user_files_viewable');
        $this->db->join('user_files', 'user_files.id = user_files_viewable.user_files_id');
        $this->db->where("user_files.id", $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_lottery_name() {
        $this->db->select('lotteries.name');
        $this->db->from('lotteries');
        $this->db->where('lotteries.day', date('l'));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['name'];
    }

    public function get_lottery_id() {
        $this->db->select('lotteries.id');
        $this->db->from('lotteries');
        $this->db->where('lotteries.day', date('l'));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['id'];
    }

    /*
     * Insert unsold information
     */

    public function insert_unsold_data($data = array()) {

        $this->db->insert_batch('unsold_data', $data);
    }

    public function getUnsoldByUserForTheDay($date = '', $id = '', $ticketid = '', $unsold_type = '') {
        $this->db->select('*');
        $this->db->from('unsold_data');
        $this->db->join('user', 'user.id = unsold_data.user_id');
        if ($id != '') {
            $this->db->where("unsold_data.user_id", $id);
        }

        if ($date != '') {
            // $this->db->where("unsold_data.sold_date", $date);
            $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        }

        if ($ticketid != '') {
            $this->db->where("unsold_data.tickets_id", $ticketid);
        }
        $this->db->where("unsold_data.type =", $unsold_type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteUnsoldByUserForTheDay($date, $id, $ticketid) {
        $this->db->where("unsold_data.user_id", $id);
        $this->db->where("unsold_data.tickets_id", $ticketid);
        //$this->db->where("unsold_data.sold_date", $date);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->delete('unsold_data');
    }

    public function get_unsold_list($date, $id) {
        $this->db->select('*');
        $this->db->from('unsold_data');
        $this->db->join('user', 'user.id = unsold_data.user_id');
        $this->db->where("user.id", $id);
        $this->db->where("unsold_data.type =", 'RETAIL');
        //$this->db->where("unsold_data.sold_date", $date);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function listUserByFullName() {

        $this->db->select('id,firstname,middlename,lastname,user_type');
        $this->db->order_by("user.user_type", "asc");
        $this->db->from('user');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUnsoldConsolidatedForTheDay($date = '', $ticketid = '', $unsold_type = '') {
        $this->db->select('*,sum(unsold_data.total) as total');
        $this->db->from('unsold_data');
        $this->db->join('user', 'user.id = unsold_data.user_id');
        $this->db->group_by('unsold_data.user_id');
        if ($date != '') {
            //$this->db->where("unsold_data.sold_date", $date);
            $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        }
        if ($ticketid != '') {
            $this->db->where("unsold_data.tickets_id", $ticketid);
        }
        $this->db->where("unsold_data.is_draft !=", 1);
        $this->db->where("unsold_data.type =", $unsold_type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insert_consolidated_sold_data($filedetails) {
        $this->db->insert("consolidated_sold_data", $filedetails);
    }

    public function deleteConsolidatedUnsold($date, $ticketid) {
        $this->db->where("consolidated_sold_data.tickets_id", $ticketid);
        //$this->db->where("consolidated_sold_data.sold_date", $date);
        $this->db->where('consolidated_sold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->delete('consolidated_sold_data');
    }

    public function consolidatedDailyData($date, $ticketid, $unsold_type) {
        $this->db->select('*');
        $this->db->where("consolidated_sold_data.tickets_id", $ticketid);
        $this->db->where("consolidated_sold_data.type =", $unsold_type);
        //$this->db->where("consolidated_sold_data.sold_date", $date);
        $this->db->where('consolidated_sold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $this->db->from('consolidated_sold_data');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function insert_timeDetails($timeDetails) {
        $this->db->insert("time_settings", $timeDetails);
    }

    public function truncate_timeDetails() {
        $this->db->empty_table("time_settings");
    }

    public function get_timeDetails() {
        $this->db->order_by("time_settings.from_time", "asc");
        $query = $this->db->get("time_settings");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function get_time($timeid = array()) {
        $this->db->select('id,from_time,to_time,type,is_active');
        $this->db->where('id', $timeid['timeid']);
        $query = $this->db->get("time_settings");
        $result = $query->result_array();
        return $result;
    }

    public function update_time($condition = array(), $datapassed) {
        $data = array(
            'from_time' => $datapassed['from_time'],
            'to_time' => $datapassed['to_time'],
        );
        $this->db->where('id', $condition['id']);
        $this->db->update('time_settings', $data);
    }

    public function selecttimesettings() {

        $this->db->select('*');
        $this->db->from('time_settings');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function isUploadBlocked($type) {

        $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $time_now = strtotime($dateTime->format("h:i A"));
        //$time_now = strtotime("04:27 AM");
        $this->db->select('*');
        $this->db->from('time_settings');
        $this->db->where('type', $type);
        $query = $this->db->get();
        $result = $query->row_array();

        $from_time = strtotime($result['from_time']);
        $to_time = strtotime($result['to_time']);
        $is_active = $result['is_active'];

        $user_type = $this->session->userdata['user']['user_type'];



        if ($from_time <= $time_now &&
                $time_now <= $to_time &&
                $is_active == "YES" &&
                $user_type != "SUPERADMIN" &&
                $user_type != "ADMIN") {

            $status = "false";
        } else if ($is_active == "NO" &&
                $user_type != "SUPERADMIN" &&
                $user_type != "ADMIN") {
            $status = "false";
        } else if ($user_type == "SUPERADMIN" ||
                $user_type == "ADMIN") {

            $status = "false";
        } else {
            $status = "true";
        }

        return $status;
    }

    public function update_unsold_data_final($selected_ticket) {

        $date = date('Y-m-d');
        $id = $this->session->userdata['user']['id'];


        $this->db->where('user_id', $id);
        $this->db->where('tickets_id', $selected_ticket);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $data['is_draft'] = 0;
        // $data['updated_at'] = date('Y-m-d H:i:s');
        $this->db->update('unsold_data', $data);
    }

    public function unlockunsoldedit($datas) {

        $date = date('Y-m-d');
        $id = $datas['user_id'];
        $selected_ticket = $datas['selectedticket'];

        $this->db->where('user_id', $id);
        $this->db->where('tickets_id', $selected_ticket);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $data['is_draft'] = 1;
        // $data['updated_at'] = date('Y-m-d H:i:s');
        $this->db->update('unsold_data', $data);
    }

    public function getShopCodes() {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }

    public function getShopCodeUser($shopcode) {
        $this->db->select('id as userId');
        $this->db->where('user_type', 'SHOP');
        $this->db->where('shop_code', $shopcode);
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }

    public function getTicketId($ticketname) {
        $this->db->select('id as ticketId');
        $this->db->where('ticket_name', $ticketname);
        $query = $this->db->get("tickets");
        $result = $query->result_array();
        return $result;
    }

    public function sync_unsold_data($data) {

        $insert = $this->db->insert("unsold_data", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function sync_consolidated_sold_data($data) {

        $insert = $this->db->insert("consolidated_sold_data", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
function checkPrizeIsWon($parameters = array(), $digitcondition) {

        $date = $parameters['date'];

        $this->db->select('unsold_data.ticket_code as ticket_code,unsold_data.type as unsold_type , unsold_data.ticket_from as ticket_from, '
                . 'unsold_data.ticket_to as ticket_to, user.firstname as firstname, tickets.ticket_name as ticket_name, '
                . 'user.lastname as lastname, unsold_data.user_id as userid, unsold_data.id as unsoldid, user.shop_code as shop_code');
        $this->db->from('unsold_data');
        $this->db->join('user', 'user.id = unsold_data.user_id');
        $this->db->join('tickets', 'tickets.id = unsold_data.tickets_id');
        $this->db->where('tickets.ticket_name', $parameters['ticket_name']);
        $this->db->where('unsold_data.type', $parameters['type']);
        $this->db->where('unsold_data.sold_date BETWEEN "' . "$date 00:00:00" . '" and "' . "$date  23:59:59" . '"');
        $query = $this->db->get();
        $unsolddetails = $query->result_array();
        
//        echo '<pre>';
//        print_r($unsolddetails);
//        exit;
        $ticketcodearrs = array();
        $winningshops = array();
        //echo $digitcondition;
        foreach ($unsolddetails as $key => $val) {
            $ticketcode = trim($unsolddetails[$key]['ticket_code']);
            $ticketfrom = trim($unsolddetails[$key]['ticket_from']);
            $ticketto = trim($unsolddetails[$key]['ticket_to']);
            $ticket_name = trim($unsolddetails[$key]['ticket_name']);
            $unsold_type = trim($unsolddetails[$key]['unsold_type']);
            $shopname = $unsolddetails[$key]['firstname'] . ' ' . $unsolddetails[$key]['lastname'];
            $shop_code = trim($unsolddetails[$key]['shop_code']);

            if ($digitcondition == "fulldigits") {
                $ticketcodearrs = explode(",", $ticketcode);
                $won_serialarr = explode(" ", $parameters['serial_number']);
                $serial_code = trim($won_serialarr[0]);
                $serial_number = trim($won_serialarr[1]);
                if (in_array($serial_code, $ticketcodearrs)) {


                    if (($serial_number >= $ticketfrom) && ($serial_number <= $ticketto)) {
                        $winningshops[] = array(
                            'won_ticket' => $serial_code . ' ' . $serial_number,
                            'ticket_number' => $serial_code . ' ' . $serial_number,
                            'prize' => $parameters['price_type'],
                            'shop_name' => $shopname,
                            'prize_amount' => $parameters['price_amount'],
                            'ticket_name' => $ticket_name,
                            'unsold_type'=> $unsold_type,
                            'shop_code'=> $shop_code
                        );
                    }
                }
            }

            if ($digitcondition == "lastfourdigits") {
                $won_serialarr = explode(" ", $parameters['serial_number']);
                $display_serial_number = trim($won_serialarr[1]);
                
                $serial_number = $parameters['serial_number'];
                $fromfirsttwo = substr($ticketfrom, 0, 2);
                $tofirsttwo = substr($ticketto, 0, 2);
                $ticketfrom = substr($ticketfrom, 2);
                $ticketto = substr($ticketto, 2);

                //exit;
                if (($serial_number >= (int) $ticketfrom) && ($serial_number <= (int) $ticketto)) {
                    $winningshops[] = array(
                        'won_ticket' => $parameters['serial_number'],
                        'ticket_number' => $ticketcode . ' ' . $fromfirsttwo.$parameters['serial_number'], 
                        'prize' => $parameters['price_type'],
                        'shop_name' => $shopname,
                        'prize_amount' => $parameters['price_amount'],
                        'ticket_name' => $ticket_name,
                        'unsold_type' => $unsold_type,
                        'shop_code' => $shop_code
                    );
                }
            }
        }
//echo '<pre>'; print_r($winningshops); exit;
        return $winningshops;
    }
     


    public function updateTodaysTicket() {
        $day = strtoupper(date('D'));
        $date = date('Y-m-d');

        $this->db->select('*');
        $this->db->where('day', $day);
        $this->db->where('date', $date);
        $query = $this->db->get("tickets");
        $result = $query->result_array();

        if (count($result) < 1) {

            $this->db->select('*');
            $this->db->where('day', $day);
            $query = $this->db->get("tickets");
            $result = $query->result_array();

            $drawcode = explode("-", $result[0]['draw_code']);
            $drawcode_updated = $drawcode[1] + 1;
            $drawcode_changed = $drawcode[0] . '-' . $drawcode_updated;


            $this->db->where('day', $day);
            $data['date'] = date('Y-m-d');
            $data['draw_code'] = $drawcode_changed;
            $this->db->update('tickets', $data);
        }
    }

    public function save_dc_data($data) {
        $insert = $this->db->insert("daily_dc", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function get_dc_list($date, $ticket, $shopcode) {
        $this->db->select('*');
        $this->db->from('daily_dc');
        $this->db->where('date', $date);
        $this->db->where('ticket_id', $ticket);
        $this->db->where('shop_code', $shopcode);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function delete_dc_list($date, $ticketid, $shopcode) {

        $this->db->where("daily_dc.ticket_id", $ticketid);
        $this->db->where('daily_dc.shop_code', $shopcode);
        $this->db->where('daily_dc.date', $date);
        $this->db->order_by("daily_dc.bill_no", "asc");
        $this->db->delete('daily_dc');
    }

    public function getShopName($shopcode) {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $this->db->where('shop_code', $shopcode);
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }

    public function getShops() {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $this->db->order_by("shop_code", "asc");
        $this->db->where("shop_code !=", "");
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }

    public function getShopswithDCdataCount($shopcode, $date, $ticket) {
        $this->db->select('daily_dc.ticket_id as ticket');
        $this->db->where("shop_code =", $shopcode);
        $this->db->where("date =", $date);
        $this->db->where("ticket_id =", $ticket);
        $query = $this->db->get("daily_dc");
        $results = $query->result_array();

        $this->db->select('COUNT(daily_dc.id) as count, daily_dc.ticket_id as ticket');
        $this->db->where("shop_code =", $shopcode);
        $this->db->where("date =", $date);
        $this->db->where("ticket_id =", $results[0]['ticket']);
        $query = $this->db->get("daily_dc");
        $result = $query->result_array();    
        
        
        
        return $result;
    }

    public function get_dc_list_by_bill($params) {
        $this->db->select('*');
        $this->db->from('daily_dc');
        $this->db->where('date', $params['date_of_dc']);
        $this->db->where('ticket_id', $params['selectedticket']);
        $this->db->where('shop_code', $params['selectedshopcode']);
        if ($params['search'] != '') {
            $this->db->where('bill_no', $params['search']);
        }
        $this->db->group_by('bill_no');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getByShopCode($shopcode) {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $this->db->order_by("shop_code", "asc");
        if ($shopcode != "") {
            $this->db->where("shop_code =", $shopcode);
        } else {
            $this->db->where("shop_code !=", "");
        }
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }
     public function getByShopid($id,$shopcode ) {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $this->db->where('id', $id);
        $this->db->order_by("shop_code", "asc");
        if ($shopcode != "") {
            $this->db->where("shop_code =", $shopcode);
        } else {
            $this->db->where("shop_code !=", "");
        }
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }
    
        public function updateTodaysTicketwithSync($ticketname, $drawcode, $date) {
//            echo $ticketname;
//            echo $drawcode;
//            echo $date;
//            
//            exit;
            
       // $day = strtoupper(date('D'));
        $date = date('Y-m-d', strtotime($date));

        $this->db->select('*');
        $this->db->where('ticket_name', $ticketname);
        $this->db->where('draw_code', $drawcode);
        $this->db->where('date', $date);
        $query = $this->db->get("tickets");
        $result = $query->result_array();

        if (count($result) < 1) {

//            $this->db->select('*');
//            $this->db->where('ticket_name', $ticketname);
//            $query = $this->db->get("tickets");
//            $result = $query->result_array();

//            $drawcode = explode("-", $result[0]['draw_code']);
//            $drawcode_updated = $drawcode[1] + 1;
//            $drawcode_changed = $drawcode[0] . '-' . $drawcode_updated;


            $this->db->where('ticket_name', $ticketname);
            $data['date'] = $date;
            $data['draw_code'] = $drawcode;
            $this->db->update('tickets', $data);
        }
    }
        public function insert_domains($data) {
            // echo '<pre>'; print_r($data);exit;
        $insert = $this->db->insert("domains", $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
        
    }
    
      public function get_domaindetails() {
        $this->db->select('id,protocol,domain,name,is_enabled,date,expiry,client,phone,updated_at');
        $query = $this->db->get("domains");
        $result = $query->result_array();
        return $result;
    }
      public function get_editdomaindetails($userid = array()) {
        $this->db->select('id,protocol,domain,name,is_enabled,date,expiry,client,phone,updated_at,address,comment');
        $this->db->where('id', $userid['userid']);
        $query = $this->db->get("domains");
        $result = $query->result_array();
        return $result;
    }
    
        function checkbarcodewinning($parameters = array(), $digitcondition){
   $date = $parameters['date'];
 //echo '<pre>'; print_r($parameters ); exit;
        $ticketcodearrs = array();
        $winningshops = array();
        $scannedticket = array();
        //echo $digitcondition;
            $scannedticket = explode("-", $parameters['scannedticket']);
            $scannedticketcode = trim($scannedticket[0]);
            $scannedticketnumber = trim($scannedticket[1]);
           // echo '<pre>'; print_r($scannedticketnumber); exit;
            if ($digitcondition == "fulldigits") {
                $won_serialarr = explode(" ", $parameters['serial_number']);
                $serial_code = trim($won_serialarr[0]);
                //echo '<pre>'; print_r($serial_code); exit;
                $serial_number = trim($won_serialarr[1]);
                 //echo '<pre>'; print_r($serial_number); exit;
                if ($serial_code == $scannedticketcode) {
                    if ($serial_number == $scannedticketnumber) {
                        $winningshops[] = array(
                            'won_ticket' => $serial_code . ' ' . $serial_number,
                            'ticket_number' => $scannedticketcode . ' ' . $scannedticketnumber,
                            'prize' => $parameters['price_type'],
                            'prize_amount' => $parameters['price_amount'],
                            'ticket_name' => $parameters['ticket_name'],
                            'drawcode' => $parameters['drawcode'],
                            'date' => $date,
                        );
                    }
                }
            }

            if ($digitcondition == "lastfourdigits") {
                $won_serialarr = explode(" ", $parameters['serial_number']);
                //$display_serial_number = trim($won_serialarr[1]);
                
                $serial_number = $parameters['serial_number'];
                $lastfour = substr($scannedticketnumber, 2);

                //exit;
                if ($serial_number == (int) $lastfour) {
                    $winningshops[] = array(
                        'won_ticket' => $parameters['serial_number'],
                        'ticket_number' => $scannedticketcode . ' ' . $scannedticketnumber, 
                        'prize' => $parameters['price_type'],
                        'prize_amount' => $parameters['price_amount'],
                        'ticket_name' => $parameters['ticket_name'],
                        'drawcode' => $parameters['drawcode'],
                        'date' => $date,
                    );
                }
            }
//echo '<pre>'; print_r($winningshops); exit;
        return $winningshops;
    }
    
      public function update_domains($condition = array(), $data) {
        //echo '<pre>'; print_r($data);exit;
        $this->db->where('id', $condition['id']);
        $this->db->update('domains', $data);
    }

     public function delete_domains($userid,$type) {
         $this->db->where('id', $userid);
          $data['is_enabled'] = 1;
        if ($type == 'block') {
          $data['is_enabled'] = 0;
        }
        //echo '<pre>'; print_r($data['is_enabled']);exit;
        $this->db->update('domains', $data);
        // $this->db->delete('user');
    }

    public function getAgentShops() {
        $this->db->select('shop_code, CONCAT(firstname, " ", lastname) AS shopname');
        $this->db->where('user_type', 'SHOP');
        $this->db->order_by("shop_code", "asc");
        $this->db->where("shop_code !=", "");
        $this->db->where("shop_code !=", 0);
        $query = $this->db->get("user");
        $result = $query->result_array();
        return $result;
    }    
    
}