<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('directory');
        $this->load->helper('form');
        //error_reporting(1);

    }

    public function home() {
        //error_reporting(1);
        $data = array();
        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year-$month-$day"; 

        if(isset($_GET['date'])){

            $date = $_GET['date'];
        } else {
            $date = $curdate; 
        }
        
        $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
          //echo '<pre>'; print_r($data); exit;
         if (empty($data['selectedticket'])) {
            if (!empty($data["ticketlist"])) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
                $data['selectedticket_cost'] = $data["ticketlist"][0]['cost'];
            }
        }
        
        $unsoldtotalarr = $this->Ticket_model->reportTotalUnsold($date, $data['selectedticket']);
        $dctotalarr = $this->Ticket_model->reportTotalDc($date, $data['selectedticket']);
        $winningtotalarr = $this->Ticket_model->reportTotalWinning($date, $data['selectedticket']);
        
        $winningtotalarr = unserialize($winningtotalarr[0]['winning_details']);
       //echo '<pre>'; print_r($winningtotalarr['details']); exit;
        
        $i = 0;
        foreach($winningtotalarr['details'] as $details){
           $winningtotal += str_replace(',','',$winningtotalarr['details'][$i]['prize_amount']);
            $i++;
            
        }
            $data['unsoldtotal'] = $unsoldtotalarr[0]['totalunsolds']*$data['selectedticket_cost'];
            $data['unsoldcount'] = ($unsoldtotalarr[0]['totalunsolds'] !=0)?$unsoldtotalarr[0]['totalunsolds']:0;
            $data['dctotal'] = ($dctotalarr[0]['totaldc'] !=0)?$dctotalarr[0]['totaldc']:0;
            $data['winningtotal'] = ($winningtotal !=0)?$winningtotal:0;
            $profitstatus = $data['winningtotal']+$data['dctotal']-$data['unsoldtotal'];
            if($profitstatus >= 0){
                $data['profit'] = $profitstatus;
                $data['loss'] = 0;
            } else if($profitstatus <= 0){
                $data['profit'] = 0;
                $data['loss'] = $profitstatus;                
            }
                 //echo $data;exit;
        $this->load->view('dashboard/dashboard_home', $data);
    }

    public function shoplist() {
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
        
        if($this->session->userdata['user']['user_type'] != 'SUPERADMIN' && $this->session->userdata['user']['user_type'] != 'ADMIN'&& $this->session->userdata['user']['user_type'] != 'TECHNICIAN'){
            redirect(base_url('/'));
        }
        
        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$day/$month/$year"; 
        
        if(isset($_GET['date'])){
            
            $date = $_GET['date'];
        } else {
            $date = $curdate; 
        }
       $data['date'] = $date;

        $data["details"] = $this->Lottery_model->getShops();
        
       // $date_of_details['']
        $this->load->view('dashboard/shoplist', $data);
    }
    public function livestatements() {
        
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
        
        if($this->session->userdata['user']['user_type'] != 'SUPERADMIN' && $this->session->userdata['user']['user_type'] != 'ADMIN'&& $this->session->userdata['user']['user_type'] != 'TECHNICIAN'){
            redirect(base_url('/'));
        }        
        
        //$data["details"] = $this->Lottery_model->getShops();
        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";
        
        if(isset($_GET['date'])){
            $dates = explode("/",$_GET['date']);
            $getdate = $_GET['date'];
            $date = $dates[2].'/'.$dates[1].'/'.$dates[0];
        } else {
            $date = $curdate; 
        }
        //echo $date; exit;
        $host = $_SERVER['HTTP_HOST'];
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
          $protocol = 'https://';
        }
        else {
          $protocol = 'http://';
        }        
        $subfolder = ($host == 'hamerkopinfotech.com')?'lotterymaster':'lotteryportalv1';
        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {                
            if($_GET['type'] == 'UNSOLD'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
              $url = "http://".$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
              $url = $protocol.$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }  else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            } else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/$subfolder/admin/dashboard/shoplist?date=$getdate";
        } else {
            if($_GET['type'] == 'UNSOLD'){
                $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }   else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            }  else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/admin/dashboard/shoplist?date=$getdate";
          
        }
          
            $file =  $url;
           // echo '<pre>'; print_r($file);exit;
            $file_headers = @get_headers($file);
            //echo '<pre>'; print_r($file_headers);
            //if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { 
            if(!$file_headers || ($file_headers[7] == 'Content-Type: text/html; charset=UTF-8') || ($file_headers[7] == 'Content-Type: text/html;charset=utf-8')) {     
                
              $data['message'] = " No Data to display ";
            }
            else {
                  $data['url'] = $url;
            }
//echo $url;
            $data['platform'] = 'web';
                
       
        $data['goback'] = $goback;
        $this->load->view('dashboard/livestatements', $data);
    }
    
    public function printlivestatements(){
              //$data["details"] = $this->Lottery_model->getShops();
        
         if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
        
        if($this->session->userdata['user']['user_type'] != 'SUPERADMIN' && $this->session->userdata['user']['user_type'] != 'ADMIN'){
            redirect(base_url('/'));
        }         

        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";
        
        if(isset($_GET['date'])){
            $dates = explode("/",$_GET['date']);
            $getdate = $_GET['date'];
            $date = $dates[2].'/'.$dates[1].'/'.$dates[0];
        } else {
            $date = $curdate; 
        }
        //echo $date; exit;
        $host = $_SERVER['HTTP_HOST'];
      
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
          $protocol = 'https://';
        }
        else {
          $protocol = 'http://';
        }        
        $subfolder = ($host == 'hamerkopinfotech.com')?'lotterymaster':'lotteryportalv1';
if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {                
            if($_GET['type'] == 'UNSOLD'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
              $url = "http://".$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
              $url = $protocol.$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }  else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            } else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/$subfolder/admin/dashboard/shoplist?date=$getdate";
        } else {
            if($_GET['type'] == 'UNSOLD'){
                $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }   else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            }  else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/admin/dashboard/shoplist?date=$getdate";
          
        }      
            $file =  $url;
            $file_headers = @get_headers($file);
            //echo '<pre>'; print_r($file_headers);
            //if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { 
            if(!$file_headers || ($file_headers[7] == 'Content-Type: text/html; charset=UTF-8') || ($file_headers[7] == 'Content-Type: text/html;charset=utf-8')) {     
                
              $data['message'] = " No Data to display ";
            }
            else {
                  $data['url'] = $url;
            }
//echo $url;
                
       
        //$data['goback'] = $goback;
        
        $this->load->view('dashboard/printlivestatements', $data);
        
    }
 
        public function livestatementsapp() {   
        //$data["details"] = $this->Lottery_model->getShops();
        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";
        
        if(isset($_GET['date'])){
            $dates = explode("/",$_GET['date']);
            $getdate = $_GET['date'];
            $date = $dates[2].'/'.$dates[1].'/'.$dates[0];
        } else {
            $date = $curdate; 
        }
        //echo $date; exit;
        $host = $_SERVER['HTTP_HOST'];
        
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
          $protocol = 'https://';
        }
        else {
          $protocol = 'http://';
        }
        
        $subfolder = ($host == 'hamerkopinfotech.com')?'lotterymaster':'lotteryportalv1';
if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {                
            if($_GET['type'] == 'UNSOLD'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
              $url = "http://".$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
              $url = $protocol.$host."/$subfolder/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }  else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            } else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/$subfolder/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/$subfolder/admin/dashboard/shoplist?date=$getdate";
        } else {
            if($_GET['type'] == 'UNSOLD'){
                $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode']."_Unsold.html";
            } else if($_GET['type'] == 'REPORT') {
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_".$_GET['type']."_ShopeDetails.html";
            } else if($_GET['type'] == 'LIVE'){
                $url = $protocol.$host."/data_shopdetail/".$date."/".$_GET['shopcode']."/".$_GET['shopcode']."_ShopeDetails.html";
            }   else if($_GET['type'] == 'UNSOLD_RETAIL'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Unsold.html";
            }  else if($_GET['type'] == 'UNSOLD_REPORT'){
              $url = $protocol.$host."/data_unsoldretail/".$date."/".$_GET['shopcode'].'200_RETAIL'."_Report.html";
            }
          $goback = $protocol.$host."/admin/dashboard/shoplist?date=$getdate";
          
        }       
        
            $file =  $url;
            $file_headers = @get_headers($file);
            //echo '<pre>'; print_r($file_headers);
            //if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { 
            if(!$file_headers || ($file_headers[7] == 'Content-Type: text/html; charset=UTF-8') || ($file_headers[7] == 'Content-Type: text/html;charset=utf-8')) {     
                
              $data['message'] = " No Data to display ";
            }
            else {
                  $data['url'] = $url;
            }
//echo $url;
               
            $data['platform'] = 'app';
       
        $data['goback'] = $goback;
        $this->load->view('dashboard/livestatements', $data);
    }
        
        public function results() {
//        $year = (int) date("Y");
//        $month = (int) date("m");
//        $month = sprintf("%02d", $month);
//        $day = (int) date("d");
//        $day = sprintf("%02d", $day);
//        $dayname = strtoupper(date("D") . 'day');
//        $curdate = "$year/$month/$day";
//        $sold_date = date("Y-m-d");
//        $winningfiledate = $year . $month . $day;
           
        $data['date_of_unsold'] = (isset($_GET['date']))?$_GET['date']:date('Y-m-d');  
        $dateofunsold = explode('-',$data['date_of_unsold']);
        $sold_date = $dateofunsold[0] .'-'. $dateofunsold[1] . '-'. $dateofunsold[2];
        //$curdate = $dateofunsold[0] .'/'. $dateofunsold[1] . '/'. $dateofunsold[2];
        $curdate = $dateofunsold[0].$dateofunsold[1].$dateofunsold[2];
        $dcdate = $dateofunsold[2].'-'.$dateofunsold[1].'-'.$dateofunsold[0];
        $winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];            
        $host = $_SERVER['HTTP_HOST'];
       // $winningfiledate = "20190523";
        $data['result_published'] = '';
        //$data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();  
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);   
        $data['selectedticket'] = (isset($_GET['ticket']))?$_GET['ticket']:$data["todaysticket"][0]['id'];
        $ticketdetails = $this->Ticket_model->getTicketDetails($data['selectedticket']);
        $data['ticket_name'] = $ticketdetails['ticket_name'];
        $finalfilename = $winningfiledate.'_'.$data['ticket_name'].'.txt'; 
            
            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/winning/$curdate/$finalfilename";
            } else {
                $file_path = winning_file_url .$data['ticket_name']."/".$curdate."_".$data['ticket_name'].'.txt';
            }       
//            echo $file_path;
//            exit;
            $winning_content = file_get_contents($file_path);
            $winning_arrs = array();
            $winnig_detail = array();
            $wonshopsarr = array();
            
            $winning_arrs = explode($winningfiledate, $winning_content);
           // echo '<pre>'; print_r($winning_arrs); exit;
            $data['result_published'] = "";
           if(!isset($winning_arrs[1])){
               $data['result_published'] = 'Result Not Uploded Contact Admin';
           }
           // echo '<pre>'; print_r($data); exit;
            foreach ($winning_arrs as $key => $val) {
                if (!empty($val)) {
                    //echo $val;
                    $winnig_detail = explode('$', $val);
                    $ticket_name = strtoupper($winnig_detail[1]);
                    $drawcode = $winnig_detail[5].'-'.$winnig_detail[4];
                    $price_type = $winnig_detail[2];
                    $price_amount = $winnig_detail[3];
                    $won_serial = $winnig_detail[6];
                    $conwonarr = explode(' ', $won_serial);
                    if($price_type == "Consolation Prize"){
                        $data['constallationcode'] = $conwonarr[1];
                        $data['ticket_name'] = $ticket_name;
                        $data['drawcode'] = $drawcode;
                        $data['date'] = $ticketdate[0];
                         $data['wontickets'][$price_type.'-'.$price_amount][0] .= $conwonarr[0].',';
                    } else {
                        $data['wontickets'][$price_type.'-'.$price_amount][] = $won_serial;
                        //$data['wontickets'][$price_type]['amount'] = $price_amount;
                    }

                }
            }
            //echo '<pre>'; print_r($data); exit;
            $this->load->view('dashboard/dailyresults', $data);
        }
        
        public function expiry(){
            $this->load->view('dashboard/expiry');
        }
       public function clientexpiry_details(){
        $data["domain_details"] = $this->Lottery_model->get_domaindetails();
        //echo '<pre>'; print_r($data); exit;
        $this->load->view('dashboard/clientexpiry_details', $data);
        }
       public function create_domains() {
        $this->load->view('dashboard/create_domains');
        }
       public function save_domains() {
        $data = $this->input->post();
        $domainDetails = array(
            'protocol' => $data['protocol'],
            'domain' => $data['domainname'],
            'name' => $data['agencyname'],
            'date' => $data['registrationdate'],
            'expiry' => $data['expirydate'],
            'client' => $data['client'],
            'phone' => $data['phone'],
            'is_enabled' => $data['status'],
            'address' => $data['address'],
            'comment' => $data['comment'],
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $user_id = $this->Lottery_model->insert_domains($domainDetails);
        $this->session->set_flashdata('message', 'Saved domain Successfully');
        redirect("dashboard/clientexpiry_details");
    }
     public function edit_domains() {
        $userid = $this->uri->segment(3);
        $condition = array('userid' => $userid);
        $data['edit_details'] = $this->Lottery_model->get_editdomaindetails($condition);
         //echo '<pre>'; print_r($data); exit;
        $this->load->view('dashboard/edit_domains', $data);
    }

    public function update_domains() {
        $data = $this->input->post();
     // echo '<pre>'; print_r($data); 
        $details = array(
            'protocol' => $data['protocol'],
            'domain' => $data['domain'],
            'name' => $data['name'],
            'date' => date("Y-m-d", strtotime($data['date'])),
            'expiry' => date("Y-m-d", strtotime( $data['expiry'])),
            'client' => $data['client'],
            'phone' => $data['phone'],
            'is_enabled' => $data['is_enabled'],
            'address' => $data['address'],
            'comment' => $data['comment'],
            'updated_at' => date('Y-m-d H:i:s'),
        );
       // echo '<pre>'; print_r($details ); exit;
        $condition = array('id' => $data['id']);
        $this->Lottery_model->update_domains($condition,  $details);
        redirect("dashboard/clientexpiry_details");
    }
     public function delete_domains() {
        $userid = $this->uri->segment(3);
        $this->Lottery_model->delete_domains($userid, $this->uri->segment(4));
        redirect("dashboard/clientexpiry_details");
    }

     
}
