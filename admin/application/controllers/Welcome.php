<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('directory');
    }

    public function index() {
        $this->load->view('user/login');
    }

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('Password');
        if (!isset($username) || !isset($password)) {
            $this->load->library('session');
            $this->session->unset_userdata('user');
            redirect(base_url('user/'));
        }

        $data = $this->Lottery_model->login($username, $password);
        if ($data) {
            $this->session->set_userdata('user', $data);
            $id = $this->session->userdata['user']['id'];
            $user_type = $this->session->userdata['user']['user_type'];
            $this->Lottery_model->updateTodaysTicket();
            $this->checkExpiry();
           // exit;
            if ($user_type == SUPERADMIN || $user_type == ADMIN || $user_type == SHOP||$user_type == TECHNICIAN) {
                redirect(base_url('dashboard/home'));
            } elseif ($user_type == AGENT || $user_type == OTHERS) {
                redirect(base_url('user/received_user_file'));
            }
        } else {
            $this->session->set_flashdata('message', 'Wrong username or password');
            redirect(base_url('/'));
        }
    }

    public function dc() {
        //http://kairaliagencies.com/admin/qrcodewondcprice?shopid=714&&billid=2345
        
        if(isset($_GET['lc']) && $_GET['lc'] !=''){ 
            $curdate = $_GET['lc'];
            $purchasecode = $_GET['b'];
            $host = $_SERVER['HTTP_HOST'];
            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if (isset($_SERVER['HTTPS']) &&
                ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
              $protocol = 'https://';
            }
            else {
              $protocol = 'http://';
            }
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/$curdate/");
            } else {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$curdate/");
            }
          //  echo '<pre>'; print_r($files); exit;
            foreach ($files as $key => $filenames) {

                $filename = explode("_", $filenames);
//                $shopcode = $filename[0];
//                $file_type = $filename[1];
                  $purchasefilecode = $filename[2];
               // $ticketnamefinal = explode(".", $ticketname);
                
                if ($purchasecode == $purchasefilecode) {
                        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/$curdate/$filenames";
                            $data['url'] = $protocol.$host."/$subfolder/$curdate/$filenames";
                        } else {
                            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$curdate/$filenames";
                            $data['url'] = $protocol.$host."/$curdate/$filenames";
                        }
                        $this->load->view('dashboard/purchasefileview', $data);
                }    
            }
            
            
        } else {
            $shopcode = (isset($_GET['s']))?$_GET['s']:0;
            $billno = (isset($_GET['b']))?$_GET['b']:0;
            $data['date'] = (isset($_GET['date']))?$_GET['date']:date('Y-m-d');
            $params = array('shopid' => $shopcode,
                         'billno'=>$billno,
                         'date' => $data['date']);

            $data['details'] = $this->Ticket_model->getDcDetails($params);
            $data['totaldc'] = $this->Ticket_model->getTotalDc($params);
            $data['user'] = (isset($_GET['user']))?$_GET['user']:"agent";
            $data['ticket'] = (isset($_GET['ticket']))?$_GET['ticket']:'';
            $data['billno'] = (isset($_GET['billno']))?$_GET['billno']:'';

            $data['shopcode'] = $_GET['s'];
            //  echo '<pre>'; print_r($data['shopcode']); exit;
            if(isset($_GET['type'])){
                $data['url'] = base_url().'dashboard/shoplist?date='.$data['date'];
            } else {
                $data['url'] = base_url().'Dcmanagement/list_dailydc_details?date='.$data['date'].'&&ticket='.$data['ticket'].'&&shopcode='.$data['shopcode'].'&&billno='.$data['billno'];
            }
            $this->load->view('user/qrcodedc', $data);
        }
    }
    
    public function dcsave(){
       // echo 'dfdsfd'; exit;
        //http://localhost/lotteryportalv1/admin/Dcmanagement/sync_dcdetails_save?date=2019-10-06&&ticket=7&&shopcode=103
            
        //To get date
        $this->clear_dc_details_list();
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
        $data['ticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : '';

        $data["tickets"] = $this->Ticket_model->getTicketwithName($data['ticket']);      
        //echo '<pre>'; print_r($data["tickets"]['id']); exit;

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $data["tickets"]['id'] : 1;

        $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);
        //echo '<pre>'; print_r($data); exit;
        $dateofunsold = explode('-',$data['date_of_unsold']);
        $sold_date = $dateofunsold[0] .'-'. $dateofunsold[1] . '-'. $dateofunsold[2];
        $curdate = $dateofunsold[0] .'/'. $dateofunsold[1] . '/'. $dateofunsold[2];
        $dcdate = $dateofunsold[2].'-'.$dateofunsold[1].'-'.$dateofunsold[0];
        //$winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];

        //$winningfiledate = "20190531";
        $ticketdetails = $this->Ticket_model->getTicketDetails($data['selectedticket']);
        $data['ticket_name'] = $ticketdetails['ticket_name'];

        $finalfilename = $data['shopcode'] . '_' . 'DC' . '_' . $data['ticket_name'] . '_' . $dcdate . '.txt';
        $selectedshopcode = $data['shopcode'];

        $host = $_SERVER['HTTP_HOST'];

        if ($host == 'localhost' || $host == 'localhost:8080') {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/lotteryportalv1/data_dc/$curdate/$selectedshopcode/$finalfilename";
        } else {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_dc/$curdate/$selectedshopcode/$finalfilename";
        }
//echo $file_path; exit;
        $dc_content = file_get_contents($file_path);

        $dc_arrs = array();
        $dc_detail = array();
        $dc_arrs = explode("=", $dc_content);
        $j = 0;
//              echo '<pre>';
//              print_r($dc_arrs);exit;
        foreach ($dc_arrs as $key => $val) {
            if (!empty($val)) {
                //echo $val;
                $dc_detail = explode('$', $val);
                //echo '<pre>'; print_r($dc_detail); exit;
                $dcarray['details'][$j]['price_type'] = $dc_detail[1];
                $dcarray['details'][$j]['ticket_id'] = $data['selectedticket'];
                $dcarray['details'][$j]['agent_name'] = $dc_detail[2];
                $dcarray['details'][$j]['shop_code'] = $data['shopcode'];
                $dcarray['details'][$j]['serial_code'] = $dc_detail[3];
                $dcarray['details'][$j]['ticket_number'] = $dc_detail[4];
                $dcarray['details'][$j]['bill_no'] = $dc_detail[5];
                $dcarray['details'][$j]['total_dc'] = $dc_detail[6];
                $dcarray['details'][$j]['date'] = $data['date_of_unsold'];
                $j++;
            }
        }

        foreach ($dcarray['details'] as $details) {

            $dc_insert_array = array(
                'price_type' => $details['price_type'],
                'ticket_id' => $details['ticket_id'],
                'agent_name' => $details['agent_name'],
                'shop_code' => $details['shop_code'],
                'serial_code' => $details['serial_code'],
                'ticket_number' => $details['ticket_number'],
                'bill_no' => $details['bill_no'],
                'total_dc' => $details['total_dc'],
                'payment' => "UNPAID",
                'date' => $sold_date
            );

            $this->Lottery_model->save_dc_data($dc_insert_array);
        }
        //echo $j; exit;
        if($j > 0){
            $message = "DC Synced Successfully";
        } else {
            $message = "No Data to Sync or failed";
        }
        $this->session->set_flashdata('message', $message);
        $this->load->view('dashboard/savedc', $data);
        //redirect('/Dcmanagement/dc_details?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&shopcode=' . $data['shopcode'], 'refresh');            
                
    }
        public function clear_dc_details_list() {
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
        $data['ticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : '';

        $data["tickets"] = $this->Ticket_model->getTicketwithName($data['ticket']);      
        //echo '<pre>'; print_r($data["tickets"]['id']); exit;

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $data["tickets"]['id'] : 1;

        //To fetch the file 
        $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);

        //echo '<pre>'; print_r($data); exit;

        $this->Lottery_model->delete_dc_list($data['date_of_unsold'], $data['selectedticket'], $data['shopcode']);

       // $this->session->set_flashdata('message', "DC Cleared Successfully");
      //  redirect('/Dcmanagement/dc_details?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&shopcode=' . $data['shopcode'], 'refresh');
    }
    
    public function checkExpiry(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://hamerkopinfotech.com/lotterymaster/api/user/domains");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        curl_close($ch);
        $responses = json_decode($response);
        // echo '<pre>'; print_r($responses); exit;
        $host= $_SERVER['HTTP_HOST'];
      //$key = array_search('kairaliagencies.com', array_column($responses->domains, 'domain'));
        $key = array_search($host, array_column($responses->domains, 'domain'));
        $responses = $responses->domains;
        $last_date = $responses[$key]->expiry;
       // $last_date = '2019-09-26';
        $todays_date=date_create(date('Y-m-d'));
        $expirydate=date_create($last_date);
       // return $days;
        $date_diff = date_diff($todays_date, $expirydate);
      //echo '<pre>'; print_r($date_diff);exit;
         $difference = $date_diff->days;
        $user_type = $this->session->userdata['user']['user_type'];
       // $daysleft=365-$difference;
       // echo '<pre>'; print_r($daysleft);exit;
      //  $expdate   = strtotime(date("Y-m-d", strtotime($installdate)) . " +1 year");
      // $expire_on= date("Y-m-d", $expdate);
        //echo '<pre>'; print_r($expire_on);exit;
 if ($difference == 0)
    {
       redirect(base_url('dashboard/expiry'));
    }
 else if ($difference <= 10) 
    {
        if ($user_type == SUPERADMIN || $user_type == ADMIN ||$user_type ==TECHNICIAN)
       {

          redirect(base_url('dashboard/home?daysleft='.$difference.'&&expire_on='.$last_date));
        }
}
}
}
