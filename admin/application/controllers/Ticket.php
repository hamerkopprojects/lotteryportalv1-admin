<?php


error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
    }

    public function index() {
        
        $data['ticketdetails'] = $this->Ticket_model->ticket_list();
        $this->load->view('lotteryticket/list',$data);
         //echo '<pre>'; print_r($data);exit;
    }
    
      public function updateticket_details() {
       
       $id = $this->uri->segment(3);
       $details = array(
        //'ticket_name' => $this->input->post('ticket_name'),
        'draw_code' =>$this->input->post('draw_code'),
        'set_count' =>$this->input->post('set_count'),
           'cost' =>$this->input->post('cost'),
//        'ticket_code' =>$this->input->post('ticket_code'),
        'day' =>$this->input->post('day'),
        'date'=> date('Y-m-d H:i:s', strtotime($this->input->post('date')))
       );
        $this->Ticket_model->update($details, $id);
        //  echo '<pre>'; print_r($ticketdetails); exit;
          $this->session->set_flashdata('message', 'Update Successfully');
         redirect(base_url('ticket/'));
      }
    
    

    
    
    
    
    
    
    

}