<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('directory');
        $this->load->helper('form');
        //error_reporting(1);

    }
    
    public function searchresult(){
        $data = array();
        if(isset($_GET['ticketnumber']) && $_GET['ticketnumber'] !=''){
            $tickettype = ($_GET['tickettype']) ? $_GET['tickettype'] : 'WEEKLY';
            $data = $this->barcodewinning($_GET['ticketnumber'], $tickettype);
            
        }
        //echo '<pre>'; print_r($data); exit;
        $this->load->view('result/searchticketwinning', $data);
        
    }

    function barcodewinning($ticketnumber, $tickettype) {
            $scannedticket = ($ticketnumber) ? $ticketnumber : 0;
            

            $identifyticket = substr($scannedticket, 0, 1);
            if ($identifyticket == 'A') {
                $ticketname = 'AKSHAYA';
            } else if ($identifyticket == 'K') {
                $ticketname = 'KARUNYA';
            } else if ($identifyticket == 'P') {
                $ticketname = 'KARUNYA PLUS';
            } else if ($identifyticket == 'N') {
                $ticketname = 'NIRMAL';
            } else if ($identifyticket == 'R') {
                $ticketname = 'POURNAMI';
            } else if ($identifyticket == 'S') {
                $ticketname = 'STHREE SAKTHI';
            } else if ($identifyticket == 'W') {
                $ticketname = 'WIN-WIN';
            }
            //echo $tickettype; exit;
            if($tickettype == 'BUMPER'){
                $ticketname = 'BUMPER';
            }
            //echo $ticketname; exit;

            $host = $_SERVER['HTTP_HOST'];
            $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
            if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$ticketname/");
            } else {
                $files = directory_map(winning_file_url.$ticketname."/");
            }
            $files = array_reverse($files);
           // echo '<pre>'; print_r($files); exit;
            $inc = 1;
            $wonshopsarr = array();
            foreach ($files as $key => $filenames) {

                if ($inc < 11) {
                    $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
                    if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/data_winning/$ticketname/$filenames";
                    } else {
                        $file_path = winning_file_url.$ticketname."/".$filenames;
                    }

                    $winning_content = file_get_contents($file_path);
                    $winning_arrs = array();
                    $winnig_detail = array();
                    
                    $winning_arr = array();
                    $winning_arr = explode('$', $winning_content);
                    $winning_arrs = explode($winning_arr[0], $winning_content);
                    //echo '<pre>'; print_r($winning_arrs ); exit;
                    foreach ($winning_arrs as $key => $val) {

                        if (!empty($val)) {
                            //echo $val;
                            $winnig_detail = explode('$', $val);

                            $ticket_name = strtoupper($winnig_detail[1]);
                            $price_type = $winnig_detail[2];
                            $price_amount = $winnig_detail[3];
                            $draw_code = $winnig_detail[5].'-'.$winnig_detail[4];
                            $won_serial = $winnig_detail[6];

                            if (strlen($won_serial) == 9) {
                                $digit = "fulldigits";
                            }
                            if (strlen($won_serial) == 4) {
                                $digit = "lastfourdigits";
                            }

                            $parameters = array('ticket_name' => $ticket_name, 'price_type' => $price_type,
                                'serial_number' => $won_serial, 'price_amount' => $price_amount, 'date' => $winning_arr[0], 'scannedticket' => $scannedticket, 'drawcode' => $draw_code);
                            if (!empty($this->Lottery_model->checkbarcodewinning($parameters, $digit))) {
                                $wonshopsarr[] = $this->Lottery_model->checkbarcodewinning($parameters, $digit);
                            }
                        }
                    }
                }
                $inc++;
            }
            if (!empty($wonshopsarr)) {
                foreach ($wonshopsarr as $wonshopsarrs) {
                    foreach ($wonshopsarrs as $key => $val) {
                        $data['details'][] = $val;
                    }
                }
            }

            if (empty($data['details'])) {
                $data = 'NODATA';
            } else {
                $data = $data;
            }
            
            return $data;

    }
}
