<?php

//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Dcmanagement extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('directory');
        //$this->load->library('excel');
        $this->load->helper('form');
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
    }

    public function index() {
        //  echo 'dsdsds'; exit;
    }

    
    public function dc_details() {
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'DC');
//        }
        $data['id'] = $this->session->userdata['user']['id'];
        $data['user_type'] = $this->session->userdata['user']['user_type'];
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();  
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }
        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
         $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];
         if ( $data['user_type'] =='SHOP' || $data['user_type'] =='AGENT' || $data['user_type'] =='OTHERS')
         {
         $data["details"] = $this->Lottery_model->getByShopid( $data['id'],$data['shopcode']);
         }
     else {
                  $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);

        }
       // $data["details"] = $this->Lottery_model->getshops();
        $data['display_date'] = date("d-m-Y", strtotime($data['date_of_unsold']));
       // echo '<pre>'; print_r($data); exit;
        // $date_of_details['']
        $this->load->view('dcmanagement/dcdetails', $data);
    }

    public function sync_dcdetails_save() {
        //To get date
        $this->clear_dc_details_list();
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'DC');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();  
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);        
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }
        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];
        //To get todays ticket name
        $selected_ticketname = $data["ticketlist"][0]['ticket_name'];
        //To fetch the file 
        $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);
//        $year = (int) date("Y");
//        $month = (int) date("m");
//        $month = sprintf("%02d", $month);
//        $day = (int) date("d");
//        $day = sprintf("%02d", $day);
//        $dayname = strtoupper(date("D") . 'day');
//        $curdate = "$year/$month/$day";
//        $dcdate = "$day-$month-$year";
//        $sold_date = date("Y-m-d");

        //echo '<pre>'; print_r($data); exit;
        $dateofunsold = explode('-',$data['date_of_unsold']);
        $sold_date = $dateofunsold[0] .'-'. $dateofunsold[1] . '-'. $dateofunsold[2];
        $curdate = $dateofunsold[0] .'/'. $dateofunsold[1] . '/'. $dateofunsold[2];
        $dcdate = $dateofunsold[2].'-'.$dateofunsold[1].'-'.$dateofunsold[0];
        //$winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];

        //$winningfiledate = "20190531";
        $ticketdetails = $this->Ticket_model->getTicketDetails($data['selectedticket']);
        $data['ticket_name'] = $ticketdetails['ticket_name'];

        $finalfilename = $data['shopcode'] . '_' . 'DC' . '_' . $data['ticket_name'] . '_' . $dcdate . '.txt';
        $selectedshopcode = $data['shopcode'];

        $host = $_SERVER['HTTP_HOST'];

        if ($host == 'localhost' || $host == 'localhost:8080') {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/lotteryportalv1/data_dc/$curdate/$selectedshopcode/$finalfilename";
        } else {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_dc/$curdate/$selectedshopcode/$finalfilename";
        }
//echo $file_path; exit;
        $dc_content = file_get_contents($file_path);

        if (empty($dc_content)) {
            $this->session->set_flashdata('error', "No Data to Sync");
            redirect('/Dcmanagement/dc_details?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&shopcode=' . $data['shopcode'] . '&&data=nil', 'refresh');
        }

        $dc_arrs = array();
        $dc_detail = array();
        $dc_arrs = explode("=", $dc_content);
        $j = 0;
//              echo '<pre>';
//              print_r($dc_arrs);exit;
        foreach ($dc_arrs as $key => $val) {
            if (!empty($val)) {
                //echo $val;
                $dc_detail = explode('$', $val);
                //echo '<pre>'; print_r($dc_detail); exit;
                $dcarray['details'][$j]['price_type'] = $dc_detail[1];
                $dcarray['details'][$j]['ticket_id'] = $data['selectedticket'];
                $dcarray['details'][$j]['agent_name'] = $dc_detail[2];
                $dcarray['details'][$j]['shop_code'] = $data['shopcode'];
                $dcarray['details'][$j]['serial_code'] = $dc_detail[3];
                $dcarray['details'][$j]['ticket_number'] = $dc_detail[4];
                $dcarray['details'][$j]['bill_no'] = $dc_detail[5];
                $dcarray['details'][$j]['total_dc'] = $dc_detail[6];
                $dcarray['details'][$j]['date'] = $data['date_of_unsold'];
                $j++;
            }
        }

        foreach ($dcarray['details'] as $details) {

            $dc_insert_array = array(
                'price_type' => $details['price_type'],
                'ticket_id' => $details['ticket_id'],
                'agent_name' => $details['agent_name'],
                'shop_code' => $details['shop_code'],
                'serial_code' => $details['serial_code'],
                'ticket_number' => $details['ticket_number'],
                'bill_no' => $details['bill_no'],
                'total_dc' => $details['total_dc'],
                'payment' => "UNPAID",
                'date' => $sold_date
            );

            $this->Lottery_model->save_dc_data($dc_insert_array);
        }
        $this->session->set_flashdata('message', "DC Synced Successfully");
        redirect('/Dcmanagement/dc_details?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&shopcode=' . $data['shopcode'], 'refresh');
    }

    public function clear_dc_details_list() {
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'DC');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();  
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);         
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }
        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        //To get todays ticket name
        $selected_ticketname = $data["ticketlist"][0]['ticket_name'];
        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];
        //To fetch the file 
        $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);

        //echo '<pre>'; print_r($data); exit;

        $this->Lottery_model->delete_dc_list($data['date_of_unsold'], $data['selectedticket'], $data['shopcode']);

       // $this->session->set_flashdata('message', "DC Cleared Successfully");
      //  redirect('/Dcmanagement/dc_details?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&shopcode=' . $data['shopcode'], 'refresh');
    }

    public function list_dailydc_details() {
        $data = array();
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['shopcode'] = (isset($_GET['shopcode'])) ? $_GET['shopcode'] : '';
//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'DC');
//        }
        
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();  
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);   
        
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }
        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];

        //To get todays ticket name
        $selected_ticketname = $data["ticketlist"][0]['ticket_name'];

        //To fetch the file 
        $data["details"] = $this->Lottery_model->getByShopCode($data['shopcode']);

        $data['display_date'] = date('d-m-Y', strtotime($data['date_of_unsold']));
        
//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'DC');
//        }

        $billno = (isset($_GET['billno'])) ? $_GET['billno'] : '';
        $params = array(
            'date_of_dc' => $data['date_of_unsold'],
            'selectedticket' => $data['selectedticket'],
            'selectedshopcode' => $data['shopcode'],
            'search' => $billno
        );
          // echo '<pre>'; print_r($params); exit;

        //To get todays ticket name
        $data['todays_dc_details'] = $this->Lottery_model->get_dc_list_by_bill($params);

        $data['billno'] = $billno;

        $this->load->view('dcmanagement/list_dailydc_details', $data);
    }

}
