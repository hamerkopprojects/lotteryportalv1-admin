<?php

//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Unsold extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('directory');
        //$this->load->library('excel');
        $this->load->helper('form');
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }


        if ($this->session->userdata['user']['user_type'] != 'SUPERADMIN' &&
                $this->session->userdata['user']['user_type'] != 'ADMIN' &&
                $this->session->userdata['user']['user_type'] != 'SHOP'&&
               $this->session->userdata['user']['user_type'] != 'TECHNICIAN')
            {
            redirect(base_url('/'));
        }
    }

    public function index() {
        
    }

    public function create() {
        $ticketid = $this->uri->segment(3);
        $data["lotteryame"] = $this->Lottery_model->get_lottery_name();
//       $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));


        $data['tickets'] = $data['ticketcodes'][0]['ticket_code'];
        $data['singleticket'] = explode(',', $data['tickets']);
        $data['selectedticket'] = $this->uri->segment(3);


        if ($data['date_of_unsold'] == date('Y-m-d')) {
            $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']);
        } else {
            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
        }

        if (!empty($data["ticketlist"])) {
            $default_ticket = $data["ticketlist"][0]['id'];
            $data['url'] = base_url() . "unsold/create/" . $default_ticket;
            $data["ticketcodes"] = $this->Ticket_model->get_ticket_code($default_ticket);
        } else {
            $default_ticket = "";
            $data['url'] = base_url() . "unsold/create/" . $default_ticket;
        }

        if (empty($data['selectedticket'])) {
            if (!empty($data["ticketlist"])) {
                $data['selectedticket'] = $data["ticketlist"][0]['id'];
            }
        }

        $data["unsold"] = $this->Lottery_model->getUnsoldByUserForTheDay(date('Y-m-d'), $this->session->userdata['user']['id'], $data['selectedticket']);


        //echo '<pre>'; print_r($data); exit;
        $this->load->view('unsold/create', $data);
    }

    public function savesold() {
        $status = $this->Lottery_model->isUploadBlocked("UNSOLD");

        if ($status == "true") {
            redirect("unsold/create");
        }

        $data = $this->input->post();
        $this->Lottery_model->deleteUnsoldByUserForTheDay(date('Y-m-d'), $this->session->userdata['user']['id'], $data['ticketname']);
        $type = 1;
        if ($data['type'] == 'final') {
            $type = 0;
        }

        $dataSold = Array();
        if (!empty($data['sold'])) {
            $opt = 1;
            foreach ($data['sold']['from'] as $key => $value) {
                $singleticketcode = '';
                $allcodes = '';

                $ticket_from = $data['sold']['from'][$key];
                $ticket_to = isset($data['sold']['to'][$key]) ? $data['sold']['to'][$key] : '';

                $is_set = isset($data['sold']['set'][$key]) ? 'YES' : 'NO';
                $dataSold[] = Array(
                    'user_id' => $this->session->userdata['user']['id'],
                    'is_set' => $is_set,
                    'ticket_code' => 0,
                    'ticket_from' => $ticket_from,
                    'ticket_to' => $ticket_to,
                    'total' => $data['sold']['total'][$key],
                    'tickets_id' => $data['ticketname'],
                    'is_draft' => $type,
//                                    'ticket_name' => $ticketdetails['ticket_name'],
//                                    'draw_code' => $ticketdetails['draw_code'],
//                                    'day' => $ticketdetails['day'],
                    'sold_date' => date('Y-m-d H:i:s')
                );

                $opt++;
            }

            $this->Lottery_model->insert_unsold_data($dataSold);
        }

        $this->session->set_flashdata('message', 'Saved unsold details successfully');
        redirect('/unsold/create/' . $data['ticketname'], 'refresh');
    }

    public function listunsold() {
        $data['date_of_unsold'] = $this->uri->segment(3);

        if (empty($data['date_of_unsold'])) {
            $data['date_of_unsold'] = date('Y-m-d');
        }


        $user_type = $this->session->userdata['user']['user_type'];
        $data['user_type'] = $user_type;
        if ($user_type != 'SUPERADMIN' && $user_type != 'ADMIN') {
            $data['selectedticket'] = $this->uri->segment(4);

            if ($data['date_of_unsold'] == date('Y-m-d')) {
                $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']);
            } else {
                $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
            }

            if (empty($data['selectedticket'])) {
                if (!empty($data["ticketlist"])) {
                    $data['selectedticket'] = $data["ticketlist"][0]['id'];
                }
            }
            $id = $this->session->userdata['user']['id'];
        } else {
            $data['userlist'] = $this->Lottery_model->listUserByFullName();
            $id = $this->uri->segment(4);
            if (empty($id)) {
                $id = '';
            }
            $data['selectedticket'] = $this->uri->segment(5);

            $postdata = $this->input->post();
            if (!empty($postdata)) {
                if (!empty($postdata['dateofunsold'])) {
                    $data['date_of_unsold'] = $postdata['dateofunsold'];
                }
                if (!empty($postdata['userid'])) {
                    $id = $postdata['userid'];
                }
                if (!empty($postdata['ticketname'])) {
                    $data['selectedticket'] = $postdata['ticketname'];
                }
            }
            if ($data['date_of_unsold'] == date('Y-m-d')) {
                $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']);
            } else {
                $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
            }
            if (empty($data['selectedticket'])) {
                if (!empty($data["ticketlist"])) {
                    $data['selectedticket'] = $data["ticketlist"][0]['id'];
                }
            }

            $data['selecteduserid'] = $id;
        }




        $data['unsolddata'] = $this->Lottery_model->getUnsoldByUserForTheDay($data['date_of_unsold'], $id, $data['selectedticket']);
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $data['unsolddata'][$i]['shop_code'] = $val['shop_code'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        $this->load->view('unsold/listunsold', $data);
    }

    public function listunsoldconsolidated() {

        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['unsold_type'] = (isset($_GET['type'])) ? $_GET['type'] : 'RETAIL';

//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'UNSOLD');
//        }
        // echo '<pre>'; print_r($data["ticketlist"]); exit;
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];

        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }

//echo '<pre>'; print_r($data); exit;

        $user_type = $this->session->userdata['user']['user_type'];
        $data['user_type'] = $user_type;

        $data['unsolddata'] = $this->Lottery_model->getUnsoldConsolidatedForTheDay($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['consolidatedunsolddata'] = $this->Lottery_model->consolidatedDailyData($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['sold_data_val'] = '';
        $data['pwt_dc_val'] = '';
        $data['sold_winning'] = '';
        $data['sold_profit_loss'] = '';
        if (!empty($data['consolidatedunsolddata'])) {
            $data['sold_data_val'] = $data['consolidatedunsolddata'][0]['sold_total'];
            $data['sold_winning'] = $data['consolidatedunsolddata'][0]['winning'];
            $data['sold_profit_loss'] = $data['consolidatedunsolddata'][0]['profit_loss'];
            $data['pwt_dc_val'] = $data['consolidatedunsolddata'][0]['pwt_dc'];
        }
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $data['unsolddata'][$i]['shop_code'] = $val['shop_code'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        //echo '<pre>'; print_r($data); exit;

        $this->load->view('unsold/listunsoldconsolidated', $data);
    }

    public function printunsoldconsolidated() {
        $data['date_of_unsold'] = $this->uri->segment(3);
        if (empty($data['date_of_unsold'])) {
            $data['date_of_unsold'] = date('Y-m-d');
        }

        $data['selectedticket'] = $this->uri->segment(4);
        $data['unsold_type'] = $this->uri->segment(5);

        $user_type = $this->session->userdata['user']['user_type'];
        $data['user_type'] = $user_type;

        $data['unsolddata'] = $this->Lottery_model->getUnsoldConsolidatedForTheDay($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['consolidatedunsolddata'] = $this->Lottery_model->consolidatedDailyData($data['date_of_unsold'], $data['selectedticket'], $data['unsold_type']);
        $data['ticketdetails'] = $this->Ticket_model->getTicketDetails($data['selectedticket']);
        //echo '<pre>'; print_r($data['ticketdetails']); exit;
        $data['sold_data_val'] = '';
        $data['pwt_dc_val'] = '';
        $data['ticket_name'] = '';
        $data['draw_code'] = '';
        $data['day'] = '';
        $data['sold_winning'] = '';
        $data['sold_profit_loss'] = '';

        if (!empty($data['consolidatedunsolddata'])) {
            $data['sold_data_val'] = $data['consolidatedunsolddata'][0]['sold_total'];
            $data['pwt_dc_val'] = $data['consolidatedunsolddata'][0]['pwt_dc'];
            $data['sold_winning'] = $data['consolidatedunsolddata'][0]['winning'];
            $data['sold_profit_loss'] = $data['consolidatedunsolddata'][0]['profit_loss'];
            $data['ticket_name'] = $data['ticketdetails']['ticket_name'];
            $data['draw_code'] = $data['ticketdetails']['draw_code'];
            $data['day'] = $data['consolidatedunsolddata'][0]['day'];
            $data['sold_date'] = $data['consolidatedunsolddata'][0]['sold_date'];
        }
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $data['unsolddata'][$i]['shop_code'] = $val['shop_code'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        $this->load->view('unsold/printunsoldconsolidated', $data);
    }

    public function listunsoldconsolidatedsave() {
        $date_of_unsold = $this->uri->segment(3);
        $data = $this->input->post();
        //echo '<pre>'; print_r($data); exit;
        $this->Lottery_model->deleteConsolidatedUnsold($date_of_unsold, $data['tickets_id']);
        $ticketdetails = $this->Ticket_model->getTicketDetails($data['tickets_id']);
        //$data['sold_date'] = $date_of_unsold;
        $data['sold_date'] = $date_of_unsold;
        $data['ticket_name'] = $ticketdetails['ticket_name'];
        //$data['draw_code'] = $ticketdetails['draw_code'];
        //$data['day'] = $ticketdetails['day'];
        $this->Lottery_model->insert_consolidated_sold_data($data);
        $sold_date = date("Y-m-d", strtotime($data['sold_date']));
        $this->session->set_flashdata('message', 'Saved consolidated unsold details successfully');
        redirect('/unsold/listunsoldconsolidated?date=' . $sold_date . '&&ticket=' . $data['tickets_id'] . '&&type=' . $data['type'], 'refresh');
    }

    public function view() {

        $this->load->model("Lottery_model");
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'user/view_users';
        $config['uri_segment'] = 3;
        $config['per_page'] = 30;

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '<li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '<li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '<li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '<li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '<li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a><li>';

        $page = $this->uri->segment(3, 0);

        $data["users"] = $this->Lottery_model->get_users_list($config['per_page'], $page);
        $config['total_rows'] = $this->Lottery_model->getTotalRow();
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('user/list_user', $data);
    }

    public function printlistunsold() {

        $data['date_of_unsold'] = $this->uri->segment(3);
        $data['selectedticket'] = $this->uri->segment(4);
        $data['user_id'] = $this->uri->segment(5);
        $data['unsold_type'] = $this->uri->segment(6);
        $data['ticket_details'] = $this->Ticket_model->getTicketDetails($data['selectedticket']);
        $data['unsolddata'] = $this->Lottery_model->getUnsoldByUserForTheDay($data['date_of_unsold'], $data['user_id'], $data['selectedticket'], $data['unsold_type']);
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        $this->load->view('unsold/printlistunsold', $data);
    }

    public function viewlistunsold() {

        $data['date_of_unsold'] = $this->uri->segment(3);
        $data['selectedticket'] = $this->uri->segment(4);
        $data['user_id'] = $this->uri->segment(5);
        $data['unsold_type'] = $this->uri->segment(6);
        $data['ticket_details'] = $this->Ticket_model->getTicketDetails($data['selectedticket']);

        $data['unsolddata'] = $this->Lottery_model->getUnsoldByUserForTheDay($data['date_of_unsold'], $data['user_id'], $data['selectedticket'], $data['unsold_type']);
        //echo '<pre>'; print_r($data['unsolddata']); exit;
        $i = 0;
        $total = 0;
        foreach ($data['unsolddata'] as $key => $val) {
            $data['unsolddata'][$i]['name'] = $val['firstname'] . ' ' . $val['middlename'] . ' ' . $val['lastname'];
            $total += $val['total'];
            $i++;
        }
        $data['total'] = $total;
        $this->load->view('unsold/viewlistunsold', $data);
    }

    public function savesoldfinal() {
        $selected_ticket = $this->input->post('selected_ticket');
        $this->Lottery_model->update_unsold_data_final($selected_ticket);

        redirect('/unsold/create');
    }

    public function unlockunsoldedit() {

        $date = $this->uri->segment(3);
        $datas['selectedticket'] = $this->uri->segment(4);
        $datas['user_id'] = $this->uri->segment(5);
        $datas['selectedtickettype'] = $this->uri->segment(6);
        $this->Lottery_model->unlockunsoldedit($datas);

        redirect('/unsold/listunsoldconsolidated?date=' . $date . '&&ticket=' . $datas['selectedticket'] . '&&type=' . $datas['selectedtickettype']);
        //redirect('/unsold/create');
    }

    public function syncretailunsold() {
        $date = $this->uri->segment(3);
        // echo '<pre>'; print_r($date); exit;
        $datas['selectedticket'] = $this->uri->segment(4);
        $data['ticket_details'] = $this->Ticket_model->getTicketDetails($datas['selectedticket']);
        $data['date_of_unsold'] = $date;

        $selected_ticketname = $data['ticket_details']['ticket_name'];
        // echo '<pre>'; print_r($data['ticket_details']); exit;
        $unsold_type = end($this->uri->segment_array());
        $unsoldarray = array();
        $shopcodes = $this->Lottery_model->getShopCodes();


        $dateofunsold = explode('-', $data['date_of_unsold']);
        $sold_date = $dateofunsold[0] . '-' . $dateofunsold[1] . '-' . $dateofunsold[2];
        $curdate = $dateofunsold[0] . '/' . $dateofunsold[1] . '/' . $dateofunsold[2];
        $dcdate = $dateofunsold[2] . '-' . $dateofunsold[1] . '-' . $dateofunsold[0];


        $p = 0;

        $host = $_SERVER['HTTP_HOST'];
        $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
            $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/$subfolder/unsoldretail/$curdate/");
        } else {
            $files = directory_map($_SERVER['DOCUMENT_ROOT'] . "/data_unsoldretail/$curdate/");
        }
//        echo '<pre>';
//        print_r($files);
//        echo $selected_ticketname;
//        exit;
        foreach ($files as $key => $filenames) {

            $filename = explode("_", $filenames);
            $shopcode = $filename[0];
            $file_type = $filename[1];
            $ticketname = $filename[2];
            $ticketnamefinal = explode(".", $ticketname);

            if ($file_type == 'RETAIL') { // for retail value 200 will be got appended with shopcode for riyas software
                $shopcode = substr($shopcode, 0, -3);
            }

            if ($file_type == $unsold_type && $ticketnamefinal[0] == $selected_ticketname) {
                $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
                if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
                    $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/unsoldretail/$curdate/$filenames";
                } else {
                    $file_path = $_SERVER['DOCUMENT_ROOT'] . "/data_unsoldretail/$curdate/$filenames";
                }

                $unsold_content = file_get_contents($file_path);
                // echo '<pre>'; print_r($unsold_content); exit;

                $unsold_arrs = array();
                $unsold_detail = array();
                $unsoldsarr = array();
                $unsold_arrs = explode("#", $unsold_content);
                $i = 0;
                $j = 0;

                foreach ($unsold_arrs as $key => $val) {
                    if (!empty($val)) {
                        //echo $val;
                        $unsold_detail = explode('$', $val);

                        if ($i == 1) {
                                $recievedshopcode = $unsold_detail[2];
                            if ($file_type == 'RETAIL') { // for retail value 200 will be got appended with shopcode for riyas software
                                $shopcode = substr($recievedshopcode, 0, -3);
                            }                            
                            
                            $unsoldarray['shop_code'] = $shopcode;
                        }
                        if ($i == 3) {
                            $unsoldarray['date'] = $unsold_detail[2];
                        }
                        if ($i == 4) {
                            $unsoldarray['ticket'] = $unsold_detail[2];
                        }
                        if ($i == 5) {
                            $unsoldarray['draw_code'] = $unsold_detail[2];
                        }
                        if ($i == 6) {
                            $unsoldarray['grant_total'] = $unsold_detail[2];
                        }

                        if ($i > 7) {
                            $unsoldarray['details'][$j]['is_set'] = ($unsold_detail[1] == 'SET') ? 'YES' : 'NO';
                            $unsoldarray['details'][$j]['ticket_code'] = $unsold_detail[2];
                            $unsoldarray['details'][$j]['ticket_from'] = $unsold_detail[3];
                            $unsoldarray['details'][$j]['ticket_to'] = $unsold_detail[4];
                            $unsoldarray['details'][$j]['ticket_count'] = $unsold_detail[5];
                            $unsoldarray['details'][$j]['ticket_quantity'] = $unsold_detail[6];
                            $unsoldarray['details'][$j]['ticket_total'] = $unsold_detail[7];
                            $j++;
                        }
                    }

                    $i++;
                }
                
                //echo '<pre>'; print_r($unsoldarray['details']); 
                //exit;

                //update todays ticket date and drawcode
                $this->Lottery_model->updateTodaysTicketwithSync($ticketnamefinal[0], $unsoldarray['draw_code'], $unsoldarray['date']);

                $time = explode(" ", $unsoldarray['date']);
                //$unsolddatetime = date("Y-m-d") . " " . $time[1];
                $unsolddatetime = $sold_date . " " . $time[1];

                $useridarray = $this->Lottery_model->getShopCodeUser($unsoldarray['shop_code']);
                $ticketarray = $this->Lottery_model->getTicketId($ticketnamefinal[0]);
//                echo $unsoldarray['shop_code'];
                //echo '<pre>'; print_r($useridarray); exit;

                foreach ($unsoldarray['details'] as $details) {
                    // echo '<pre>'; print_r($details);
                    //exit;
                    $unsold_insert_array = array('user_id' => $useridarray[0]['userId'],
                        'is_set' => $details['is_set'],
                        'ticket_code' => $details['ticket_code'],
                        'ticket_from' => $details['ticket_from'],
                        'ticket_to' => $details['ticket_to'],
                        'total' => $details['ticket_total'],
                        'tickets_id' => $ticketarray[0]['ticketId'],
                        'ticket_count' => $details['ticket_count'],
                        'ticket_quantity' => $details['ticket_quantity'],
                        'is_draft' => 0,
                        'type' => $unsold_type,
                        'sold_date' => $unsolddatetime,
                    );

                       $this->Lottery_model->sync_unsold_data($unsold_insert_array);

                }

                $consolidated_array = array('tickets_id' => $ticketarray[0]['ticketId'],
                    'sold_total' => 0,
                    'count_total' => $unsoldarray['grant_total'],
                    'pwt_dc' => 0,
                    'winning' => 0,
                    'profit_loss' => 0,
                    'sold_date' => $unsolddatetime,
                    'ticket_name' => $unsoldarray['ticket'],
                    'draw_code' => $unsoldarray['draw_code'],
                    'type' => $unsold_type,
                        //'day' => $dayname
                );

                $this->session->set_flashdata('message', "Unsold Synced Successfully");
                $p++;
            }
            unset($unsoldarray['details']);
        }
        //if data is nil or no data to sync  p==0 otherwise p==1 and loop will
        if ($p == 0) {
            $data_status = '&&data=nil';
        } else {
            $data_status = '';
        }
        redirect('/unsold/listunsoldconsolidated?date=' . $sold_date . '&&ticket=' . $datas['selectedticket'] . '&&type=' . $unsold_type . $data_status, 'refresh');
    }

    public function updateTodaysTicket($ticketnamefinal, $drawcode) {

        $checkupdated = $this->Ticket_model->checkTicketUpdated($ticketnamefinal);

        if (count($checkupdated) < 1) {
            $this->Ticket_model->updateTodaysTicket($ticketnamefinal, $drawcode);
        }
    }

    public function clearunsolddata() {

        $date = $this->uri->segment(3);
        $datas['selectedticket'] = $this->uri->segment(4);
        $datas['unsold_type'] = $this->uri->segment(5);

        $this->Ticket_model->clearunsolddata($datas['selectedticket'], $datas['unsold_type'], $date);
        $this->Ticket_model->clearunsoldconsolidated($datas['selectedticket'], $datas['unsold_type']);
        $this->session->set_flashdata('message', "Unsold Cleared Successfully");
        redirect('/unsold/listunsoldconsolidated?date=' . $date . '&&ticket=' . $datas['selectedticket'] . '&&type=' . $datas['unsold_type'], 'refresh');
    }

    public function listunsoldwinning() {
        $todays_winning = array();
        $winningdata = array();
        $data['status'] = "NOTSYNCED";

        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['unsold_type'] = (isset($_GET['type'])) ? $_GET['type'] : 'RETAIL';

//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'WINNING');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];

        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        //   echo '<pre>'; print_r($data); exit; 
        $todays_winning = $this->Ticket_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
        //echo '<pre>'; print_r($todays_winning); exit;
        if (!empty($todays_winning)) {
            if ($todays_winning[0]['winning_details'] != "NODATA") {
                $winningdata = unserialize($todays_winning[0]['winning_details']);
            } else {
                $data['status'] = $todays_winning[0]['winning_details'];
            }
        }

        // echo '<pre>'; print_r($winningdata); exit;

        if (!empty($winningdata)) {
            $data = array_merge($winningdata, $data);
        }

        //echo '<pre>'; print_r($data); exit;
        $this->load->view('unsold/listunsoldwinning', $data);
    }

    public function checkPrizeIsWon($parameters, $digitcondition) {
        return $this->Ticket_model->checkPrizeIsWon($parameters, $digitcondition);
    }

    public function syncretailunsoldwinningticket() {
        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['unsold_type'] = (isset($_GET['type'])) ? $_GET['type'] : 'RETAIL';

//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'WINNING');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);

        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];

        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }
        $todays_winning = $this->Ticket_model->checkWinningSaved($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
        if (empty($todays_winning)) {
            $this->savewinning($data['selectedticket'], $data['date_of_unsold'], $data['unsold_type']);
        }

        redirect('/unsold/listunsoldwinning?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&type=' . $data['unsold_type'], 'refresh');
    }

    public function clearsyncretailunsoldwinningticket() {

        $data['date_of_unsold'] = (isset($_GET['date'])) ? $_GET['date'] : date('Y-m-d');
        $data['unsold_type'] = (isset($_GET['type'])) ? $_GET['type'] : 'RETAIL';

//        if($data['date_of_unsold'] == date('Y-m-d')){
//           $data["ticketlist"] = $this->Ticket_model->get_todays_ticket_list($data['date_of_unsold']); 
//        } else {
//            $data["ticketlist"] = $this->Ticket_model->get_ticket_list($data['date_of_unsold'], 'WINNING');
//        }
        $dayname = strtoupper(date("D"));
        $data["ticketlist"] = $this->Ticket_model->ticket_list();
        $data["todaysticket"] = $this->Ticket_model->get_todays_ticket($dayname);
        $i = 0;
        foreach ($data["ticketlist"] as $tickets) {
            $ticketarr[] = $data["ticketlist"][$i]['id'];

            $i++;
        }

        $data['selectedticket'] = (isset($_GET['ticket'])) ? $_GET['ticket'] : $data["todaysticket"][0]['id'];

        if (!in_array($data['selectedticket'], $ticketarr)) {
            $data['selectedticket'] = $data["ticketlist"][0]['id'];
        }

        $this->Ticket_model->clearwinningddata($data['selectedticket'], $data['unsold_type'], $data['date_of_unsold']);
        redirect('/unsold/listunsoldwinning?date=' . $data['date_of_unsold'] . '&&ticket=' . $data['selectedticket'] . '&&type=' . $data['unsold_type'], 'refresh');
    }

    public function savewinning($ticketId, $date, $unsold_type) {

//        $year = (int) date("Y");
//        $month = (int) date("m");
//        $month = sprintf("%02d", $month);
//        $day = (int) date("d");
//        $day = sprintf("%02d", $day);
//        $dayname = strtoupper(date("D") . 'day');
//        $curdate = "$year/$month/$day";
//        $sold_date = date("Y-m-d");
//        $winningfiledate = $year . $month . $day;
        //$winningfiledate = "20190531";
        $data['date_of_unsold'] = $date;
        //echo '<pre>'; print_r($data['date_of_unsold'] ); exit;

        $dateofunsold = explode('-', $data['date_of_unsold']);
        $sold_date = $dateofunsold[0] . '-' . $dateofunsold[1] . '-' . $dateofunsold[2];
        $curdate = $dateofunsold[0] . '/' . $dateofunsold[1] . '/' . $dateofunsold[2];
        $dcdate = $dateofunsold[2] . '-' . $dateofunsold[1] . '-' . $dateofunsold[0];
        $winningfiledate = $dateofunsold[0] . $dateofunsold[1] . $dateofunsold[2];

        $ticketdetails = $this->Ticket_model->getTicketDetails($ticketId);
        $data['ticket_name'] = $ticketdetails['ticket_name'];
        $finalfilename = $winningfiledate . '_' . $data['ticket_name'] . '.txt';
        //echo '<pre>'; print_r( $finalfilename); exit;
        $host = $_SERVER['HTTP_HOST'];

        //    $resultfilepath = $this->getResultFile();
        //echo '<pre>'; print_r($resultfilepath); exit;
        $subfolder = ($host == 'hamerkopinfotech.com') ? 'lotterymaster' : 'lotteryportalv1';
        if ($host == 'localhost' || $host == 'localhost:8080' || $host == 'hamerkopinfotech.com') {
            $file_path = $_SERVER['DOCUMENT_ROOT'] . "/$subfolder/winning/$curdate/$finalfilename";
        } else {
            //$file_path = $_SERVER['DOCUMENT_ROOT'] . "/winning/$curdate/$finalfilename";
            $file_path = winning_file_url .$data['ticket_name']."/".$winningfiledate."_".$data['ticket_name'].'.txt';
        }
        //echo '<pre>'; print_r($resultfilepath); exit;
        //$file_path = $resultfilepath['file_path'];
        $winning_content = file_get_contents($file_path);
        // echo '<pre>'; print_r($winning_content); exit;
        $winning_arrs = array();
        $winnig_detail = array();
        $wonshopsarr = array();

        $winning_arrs = explode($winningfiledate, $winning_content);
        //echo '<pre>'; print_r($winning_arrs); exit;
        foreach ($winning_arrs as $key => $val) {
            if (!empty($val)) {
                //echo $val;
                $winnig_detail = explode('$', $val);
                $ticket_name = strtoupper($winnig_detail[1]);
                $price_type = $winnig_detail[2];
                $price_amount = $winnig_detail[3];
                $won_serial = $winnig_detail[6];

                if (strlen($won_serial) == 9) {
                    $digit = "fulldigits";
                }
                if (strlen($won_serial) == 4) {
                    $digit = "lastfourdigits";
                }
                //exit;
                //echo $price_type;
                $parameters = array('ticket_name' => $ticket_name, 'price_type' => $price_type,
                    'serial_number' => $won_serial, 'price_amount' => $price_amount, 'type' => $unsold_type, 'date' => $sold_date);
                //echo '<pre>'; print_r($parameters ); exit;
                //if ($price_type == "1st Prize" || $price_type == "Consolation Prize" || $price_type == "2nd Prize" || $price_type == "3rd Prize") {
                if (!empty($this->Lottery_model->checkPrizeIsWon($parameters, $digit))) {
                    $wonshopsarr[] = $this->Lottery_model->checkPrizeIsWon($parameters, $digit);
                }
                //}
//                if ($price_type == "4th Prize" || $price_type == "5th Prize" ||
//                        $price_type == "6th Prize" || $price_type == "7th Prize" ||
//                        $price_type == "8th Prize" || $price_type == "9th Prize") {
//
//                    if (!empty($this->Lottery_model->checkPrizeIsWon($parameters, "lastfourdigits"))) {
//                        $wonshopsarr[] = $this->Lottery_model->checkPrizeIsWon($parameters, "lastfourdigits");
//                        }
//                    }
            }
        }
//              echo '<pre>'; print_r($wonshopsarr);
//              exit;
        foreach ($wonshopsarr as $wonshopsarrs) {
            foreach ($wonshopsarrs as $key => $val) {
                $data['details'][] = $val;
            }
        }
        // echo '<pre>'; print_r($data['details']); exit;
        if (empty($data['details'])) {
            $data = 'NODATA';
        } else {
            $data = serialize($data);
        }
        // echo '<pre>'; print_r($data); exit;
//            if($resultfilepath['result_published'] == "Result Not Published"){
//                $data = "Result Not Published";
//            }
        $datas = array('winning_details' => $data,
            'type' => $unsold_type,
            'ticket_id' => $ticketId,
            'date' => $sold_date);

        $this->Ticket_model->savewonshopdetails($datas);
    }

    public function getResultFile() {

        $year = (int) date("Y");
        $month = (int) date("m");
        $month = sprintf("%02d", $month);
        $day = (int) date("d");
        $day = sprintf("%02d", $day);
        $dayname = strtoupper(date("D") . 'day');
        $curdate = "$year/$month/$day";
        $sold_date = date("Y-m-d");
        $winningfiledate = $year . $month . $day;

        // $winningfiledate = "20190523";
        $data['result_published'] = '';
        $ticketlists = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
        $data['ticketlist'] = $ticketlists;

        if (isset($_GET['ticket'])) {

            $ticketdetails = $this->Ticket_model->getTicketDetails($_GET['ticket']);
            $drawcode = explode('-', $ticketdetails['draw_code']);
            $selectedticket = trim($winningfiledate . '_' . $ticketdetails['ticket_name'] . $drawcode[1]);
            $data['selectedticket'] = $_GET['ticket'];
        }
        $ticket1 = '';
        $ticket2 = '';

        $ticketdetails = $this->Ticket_model->get_todays_ticket_list(date('Y-m-d'));
        $data['ticketdetails'] = $ticketdetails;


        $file_path_main = "http://riyaskk.in/Lottery/PWT/dwnldRes.php";
        $ticket_main = file_get_contents($file_path_main);
        $ticket_main_arr = explode($winningfiledate, $ticket_main);

        if ($ticket_main_arr[1] == '') {
            $data['result_published'] = 'Result Not Published';
        }
        // echo '<pre>'; print_r($ticket_main_arr[1]); exit;
        if (isset($ticket_main_arr[1])) {
            $ticket1 = explode('[', $ticket_main_arr[1]);
            $ticket1 = $winningfiledate . $ticket1[0];
            $ticket1 = trim($ticket1);
        }


        if (isset($ticket_main_arr[2])) {
            $ticket2 = explode('[', $ticket_main_arr[2]);
            $ticket2 = explode(')', $ticket2[0]);
            $ticket2 = $winningfiledate . $ticket2[0];
            $ticket2 = trim($ticket2);
        }
        // echo $selectedticket;
        if ($ticket1 == $ticket2 && $selectedticket == '') {
            $finalticketresultname = $ticket1;
        } elseif ($selectedticket == $ticket1 && $selectedticket != '') {
            $finalticketresultname = $ticket1;
        } elseif ($selectedticket == $ticket2 && $selectedticket != '') {
            $finalticketresultname = $ticket2;
        } else {
            $finalticketresultname = $ticket1;
        }
//echo $finalticketresultname; exit;
        $finalticketresultname = str_replace(" ", '%20', $finalticketresultname);
        if (strpos($finalticketresultname, ')') !== false) {

            $finalticketresult = explode(')', $finalticketresultname);
            $finalticketresultname = trim($finalticketresult[0]);
            // echo 'true';
        }
        $ticketdate = explode('_', $finalticketresultname);
        $file_path = "http://riyaskk.in/Lottery/PWT/$finalticketresultname";
        $data['file_path'] = $file_path;

        return $data;
    }

}
