<?php

error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        if (!$this->session->userdata('user')) {
            redirect(base_url('/'));
        }
    }

    public function logout() {
        $this->session->unset_userdata('user');
        redirect(base_url('/'));
    }

    public function home() {
        if ($this->session->userdata('user')) {
            $this->load->view('admin/header');
            $this->load->view('admin/footer');
        } else {
            redirect(base_url() . $this->index());
        }
    }

    public function create_users() {
        
        
        $data['shops'] = $this->Lottery_model->getAgentShops();
        //$this->load->view('user/list_user', $data);
        $this->load->view('user/create_user', $data);
    }

    public function saveusers() {
        $data = $this->input->post();
        $data['shop_code'] = (isset($data['shop_code'])) ? $data['shop_code'] : 0;
        $userDetails = array(
            'username' => trim($data['username']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'password' => trim($data['password']),
            'user_type' => $data['usertype'],
            //'shop_code' => $data['shop_code'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
//          'deleted_at' => date('Y-m-d H:i:s'),
        );
        
            if($data['usertype'] == 'AGENT'){
             $shoparr = array('shop_code' => $data['shop_id']);
            } else {
              $shoparr = array('shop_code' => $data['shop_code']);  
            }
            
            $userDetails = array_merge($userDetails, $shoparr);
        $user_id = $this->Lottery_model->insert_users($userDetails);
        $this->session->set_flashdata('message', 'Saved User Successfully');
        redirect("user/view_users");
    }

    public function view_users() {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'user/view_users';
        $config['uri_segment'] = 3;
        //$config['per_page'] = 30;

        $page = $this->uri->segment(3, 0);

        $data["users"] = $this->Lottery_model->get_users_list($config['per_page'], $page);
        $data["superadmins"] = $this->Lottery_model->get_super_user_list();
        
        $config['total_rows'] = $this->Lottery_model->getTotalRow();
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        //echo '<pre>'; print_r($data["superadmins"]); exit;
        $this->load->view('user/list_user', $data);
    }

    public function edituser() {
        $userid = $this->uri->segment(3);
        $condition = array('userid' => $userid);
        $data['user'] = $this->Lottery_model->get_user($condition);
         //echo '<pre>'; print_r($data); exit;
        $data['shops'] = $this->Lottery_model->getAgentShops();
       //echo '<pre>'; print_r($data); exit;
        $this->load->view('user/user_view', $data);
    }

    public function updateusers() {
        $data = $this->input->post();
        //echo '<pre>'; print_r($data); exit;
        $condition = array('id' => $data['userid']);
        $this->Lottery_model->update_user($condition, $data);
        redirect("user/view_users");
    }

    public function deleteuser() {
        $userid = $this->uri->segment(3);
        $this->Lottery_model->delete_user($userid, $this->uri->segment(4));
        redirect("user/view_users");
    }

    public function username_exists() {
        $username = $this->input->post('username');
        $exists = $this->Lottery_model->username_exists($username);
        $count = count($exists);
        echo $count;
        if (empty($count)) {
            return true;
        } else {
            return false;
        }
    }
  
    public function create_user_file() {

        $id = $this->session->userdata['user']['id'];

        $data['username'] = $this->Lottery_model->selectusers($id);
        $this->load->view('user/create_user_file', $data);
    }

    public function user_file_upload() {
        
        
        $status = $this->Lottery_model->isUploadBlocked("FILE");
        
        if($status == "true"){
            redirect("user/create_user_file");
        }
        
        $file_name = $this->upload_files($_FILES['file_name']);
        // $file_name = $this->upload->data();
        $id = $this->session->userdata['user']['id'];
        $data = $this->input->post();
        $data = array(
            'file_name' => $data['filename'],
            'file_path' => $file_name,
            'created_at' => date('Y-m-d H:i:s'),
            'deleted_at' => date('Y-m-d H:i:s'),
            'enable_archive_option' => 'YES',
            'user_id' => $id
        );
        $user_id = $this->Lottery_model->user_file_upload($data);

        $viewableusers = $this->input->post('userOpt');
        foreach ($viewableusers as $key => $value) {
            $filedetails = array(
                'user_files_id' => $user_id,
                'user_id' => $value,
            );
            $this->Lottery_model->insert_userviewable($filedetails);
        }
        $this->session->set_flashdata('message', 'Saved File Successfully');
        redirect("user/assigned_user_file");
    }

    private function upload_files($files) {
        $folderPath = date('Y') . '/' . date('m') . '/' . date('d') . '/' . date('H');
        $config['upload_path'] = '../assets/uploads/' . $folderPath;
        mkdir($config['upload_path'], 0777, true);
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);


        $images = array();
        $images_data = array();

        foreach ($files['name'] as $key => $image) {
            if (!empty($files['name'][$key])) {
                $fileName = $files['name'][$key];
                $_FILES['images[]']['name'] = $fileName;
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];

                $images[] = $fileName;

                $new_name = rand() . 'random' . $_FILES['images[]']['name'];
                $config['file_name'] = str_replace(' ', '_', $new_name);

                $images_data[$key] = $folderPath . '/' . $config['file_name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('images[]')) {
                    $this->upload->data();
                } else {
                    return false;
                }
            }
        }
//        exit;

        return implode('@#@$@%*&', $images_data);
    }
    
    
    private function upload_update_files($files, $file_name_already_have) {
        
        $folderPath = date('Y') . '/' . date('m') . '/' . date('d') . '/' . date('H');
        $config['upload_path'] = '../assets/uploads/' . $folderPath;
        mkdir($config['upload_path'], 0777, true);
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);


        $images = array();
        $images_data = array();

        foreach ($files['name'] as $key => $image) {
            if (!empty($files['name'][$key])) {
                $fileName = $files['name'][$key];
                $_FILES['images[]']['name'] = $fileName;
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];

                $images[] = $fileName;

                $new_name = rand() . 'random' . $_FILES['images[]']['name'];
                $config['file_name'] = str_replace(' ', '_', $new_name);

                $images_data[$key] = $folderPath . '/' . $config['file_name'];

                $this->upload->initialize($config);

                if ($this->upload->do_upload('images[]')) {
                    $this->upload->data();
                } else {
                    return false;
                }
            } else {
                if (!empty($file_name_already_have[$key])) {
                   $images_data[$key] = $file_name_already_have[$key]; 
                }
            }
        }
//        exit;

        return implode('@#@$@%*&', $images_data);
    }

    public function assigned_user_file() {
        $id = $this->session->userdata['user']['id'];
        $user_type = $this->session->userdata['user']['user_type'];
        $data['users_file'] = $this->Lottery_model->assigned_files_view($id, $user_type);
        $i = 0;
        foreach ($data['users_file'] as $key => $val) {
            $data['users_file'][$i]['file_path'] = $data['users_file'][$i]['file_path'];
            $data['users_file'][$i]['created_at'] = $data['users_file'][$i]['created_at'];
            $data['users_file'][$i]['firstname'] = $data['users_file'][$i]['firstname'] . ' ' . $data['users_file'][$i]['lastname'];
            $assigned_users = $this->Lottery_model->user_details($data['users_file'][$i]['user_id']);

            $data['users_file'][$i]['assigned_user'] = $assigned_users[0]['username'];
            $i++;
        }
        $this->load->view('user/assigned_user_files', $data);
    }

    public function received_user_file() {
        $data['username'] = $this->Lottery_model->selectuser();
        $id = $this->session->userdata['user']['id'];
        $user_type = $this->session->userdata['user']['user_type'];

        $details = array('assigned_from' => $assigned_from,
            'assigned_to' => $assigned_to,
            'date' => $date);
        $data['users_file'] = $this->Lottery_model->received_files_view($id, $user_type, $details);
        $i = 0;
        foreach ($data['users_file'] as $key => $val) {

            $data['users_file'][$i]['created_at'] = $data['users_file'][$i]['created_at'];
            $data['users_file'][$i]['firstname'] = $data['users_file'][$i]['firstname'] . ' ' . $data['users_file'][$i]['lastname'];
            $assigned_users = $this->Lottery_model->received_user_details($data['users_file'][$i]['user_id']);
            $data['users_file'][$i]['assigned_user'] = $assigned_users[0]['username'];
            $i++;
        }

        $this->load->view('user/received_user_files', $data);
    }

    public function received_user_file_data() {
        $data['username'] = $this->Lottery_model->selectuser();
        $id = $this->session->userdata['user']['id'];
        $user_type = $this->session->userdata['user']['user_type'];
        $assigned_from = $this->input->post('assign_from');
        $assigned_to = $this->input->post('assign_to');
        $file_search = $this->input->post('file_search');
        $date = $this->input->post('datepicker');
        $details = array('assign_from' => $assigned_from,
            'assign_to' => $assigned_to,
            'file_search' => $file_search,
            'date' => $date);
        $data['users_file'] = $this->Lottery_model->received_files_view($id, $user_type, $details);
        $i = 0;
        foreach ($data['users_file'] as $key => $val) {

            $data['users_file'][$i]['created_at'] = $data['users_file'][$i]['created_at'];
            $data['users_file'][$i]['firstname'] = $data['users_file'][$i]['firstname'] . ' ' . $data['users_file'][$i]['lastname'];
            $assigned_users = $this->Lottery_model->received_user_details($data['users_file'][$i]['user_id']);
            $data['users_file'][$i]['assigned_user'] = $assigned_users[0]['username'];
            $i++;
        }


        $this->load->view('user/search_received_user_files', $data);
    }

    public function updatefiles($uid) {

        $data['assignedusers'] = $this->Lottery_model->assigneduser($uid);
        $data['assignfiles'] = $this->Lottery_model->assignedfiles($uid);
        $data['username'] = $this->Lottery_model->selectuser();
        foreach ($data['assignedusers'] as $key => $val) {
            $data['assigneduser'][] = $data['assignedusers'][$key]['user_id'];
        }
        $this->load->view('user/assigned_user_file', $data);
    }

    public function updateuserfiles() {
        
        $status = $this->Lottery_model->isUploadBlocked("FILE");
        
        if($status == "true"){
            redirect("user/updatefiles");
        }
       
        $fileid = $this->input->post('file_id');
        $data = $this->input->post();
        $filearr = array();
        $fid = $this->input->post('file_id');
        $this->Lottery_model->delete_user_with_update_files($fid);

        $id = $this->session->userdata['user']['id'];

       
        $files_name = $this->upload_update_files($_FILES['file_name'], $this->input->post('file_name_already_have'));
        $new_files_array = explode('@#@$@%*&', $files_name);
        $existing_files_name = $this->input->post('existing_files_name');
        $existing_files_array = explode('@#@$@%*&', $existing_files_name);

        $files_to_delete = array_diff($existing_files_array, $new_files_array); 
        foreach ($files_to_delete as $key => $value) {
            if(file_exists('../assets/uploads/'.$value)) {
                unlink('../assets/uploads/'.$value);
            }           
        }

        $data = array(
            'file_name' => $this->input->post('filename'),
            'file_path' => $files_name
        );

        $data = array_merge($data, $filearr);

        $this->Lottery_model->user_update_file_upload($data, $fileid);

        $viewableusers = $this->input->post('userOpt');
        foreach ($viewableusers as $key => $value) {
            $filedetails = array(
                'user_files_id' => $fileid,
                'user_id' => $value,
            );
            $this->Lottery_model->insert_userviewable($filedetails);
        }

        redirect("user/assigned_user_file");
    }

    public function deletefiles($uid) {
        $this->Lottery_model->delete_user_with_files($uid);
        redirect("user/assigned_user_file");
    }

    public function searchdata() {
        $data = array();
        $id = $this->session->userdata['user']['id'];
        $user_type = $this->session->userdata['user']['user_type'];
        $data = $this->input->post();
        $details = array(
            'assign_from' => $data['assign_from'],
            'assign_to' => $data['assign_to'],
            'date' => $data['cal_date'],
        );

        $data['users_file'] = $this->Lottery_model->received_files_view($id, $user_type, $details);
        $i = 0;
        foreach ($data['users_file'] as $key => $val) {
            $data['users_file'][$i]['created_at'] = $data['users_file'][$i]['created_at'];
            $data['users_file'][$i]['firstname'] = $data['users_file'][$i]['firstname'] . ' ' . $data['users_file'][$i]['lastname'];
            $assigned_users = $this->Lottery_model->received_user_details($data['users_file'][$i]['user_id']);
            $data['users_file'][$i]['assigned_user'] = $assigned_users[0]['username'];
            $i++;
        }
        $data['username'] = $this->Lottery_model->selectuser();
        $this->load->view('user/received_user_files', $data);
    }

    public function download() {
        $this->load->helper('download');
        $data = $this->input->get();
        $file = '../assets/uploads/' . $data['filepath'];
        force_download($file, NULL);
    }

    public function timesettings() {
        $time_settings = $this->Lottery_model->selecttimesettings();
        foreach ($time_settings as $key => $value) {
            if ($value['type'] == 'FILE') {
                $data['file_from_time'] = $value['from_time'];
                $data['file_to_time'] = $value['to_time'];
                $data['file_is_active'] = $value['is_active'];
            }

            if ($value['type'] == 'UNSOLD') {
                $data['unsold_from_time'] = $value['from_time'];
                $data['unsold_to_time'] = $value['to_time'];
                $data['unsold_is_active'] = $value['is_active'];
            }
        }
        $this->load->view('user/create_time', $data);
    }

    public function savetime() {
        $data = $this->input->post();
        $this->Lottery_model->truncate_timeDetails();
        // For handling file upload page visibility
        $is_active = (isset($data['file_is_active'])) ? "YES" : "NO";
        $timeDetails = array(
            'from_time' => $data['file_from_time'],
            'to_time' => $data['file_to_time'],
            'type' => 'FILE',
            'is_active' => $is_active,
        );

        $this->Lottery_model->insert_timeDetails($timeDetails);

        // For handling unsold create page visibility
        $is_active = (isset($data['unsold_is_active'])) ? "YES" : "NO";
        $timeDetails = array(
            'from_time' => $data['unsold_from_time'],
            'to_time' => $data['unsold_to_time'],
            'type' => 'UNSOLD',
            'is_active' => $is_active,
        );

        $this->Lottery_model->insert_timeDetails($timeDetails);

        $this->session->set_flashdata('message', 'Saved Time Successfully');
        redirect("user/timesettings");
    }

    public function list_time() {
        $data["time"] = $this->Lottery_model->get_timeDetails();
        $this->load->view('user/list_time', $data);
    }

    public function edit_time() {
        $timeid = $this->uri->segment(3);
        $condition = array('timeid' => $timeid);
        $data['time'] = $this->Lottery_model->get_time($condition);
        $this->load->view('user/edit_time', $data);
    }

    public function updatetime() {
        $data = $this->input->post();
        $condition = array('id' => $data['timeid']);
        $this->Lottery_model->update_time($condition, $data);
        redirect("user/list_time");
    }
    
    public function livestatements() {
        $this->load->view('dashboard/dashboard_home');
    }

}
