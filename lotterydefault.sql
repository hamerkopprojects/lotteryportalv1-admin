-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2019 at 09:34 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lotterydefault`
--

-- --------------------------------------------------------

--
-- Table structure for table `consolidated_sold_data`
--

CREATE TABLE `consolidated_sold_data` (
  `id` int(11) NOT NULL,
  `tickets_id` int(11) NOT NULL,
  `count_total` int(50) NOT NULL,
  `sold_total` int(11) NOT NULL,
  `pwt_dc` int(11) NOT NULL,
  `winning` int(225) NOT NULL,
  `profit_loss` int(225) NOT NULL,
  `sold_date` datetime NOT NULL,
  `ticket_name` varchar(225) NOT NULL,
  `draw_code` varchar(225) NOT NULL,
  `type` enum('RETAIL','WHOLESALE') NOT NULL,
  `day` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_dc`
--

CREATE TABLE `daily_dc` (
  `id` int(11) NOT NULL,
  `price_type` varchar(225) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `shop_code` int(20) NOT NULL,
  `serial_code` varchar(255) NOT NULL,
  `ticket_number` int(50) NOT NULL,
  `bill_no` int(255) NOT NULL,
  `total_dc` int(225) NOT NULL,
  `payment` enum('PAID','UNPAID') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_winning`
--

CREATE TABLE `daily_winning` (
  `id` int(11) NOT NULL,
  `winning_details` longtext NOT NULL,
  `type` enum('RETAIL','WHOLESALE') NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keys`
--

CREATE TABLE `tbl_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_keys`
--

INSERT INTO `tbl_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(2, 1, 'o44gcgkgg0o4g0sow4gw4ws0ww880c04kk4k0gw0', 1, 0, 0, NULL, '2019-09-23 18:12:58'),
(5, 1, '8g8ws40gskskwksskkswcc0wkowg8ow4wowc8488', 1, 0, 0, NULL, '2019-09-24 14:49:36'),
(6, 1, 'wkogsssoswwsg0cckcscoocsgs80kk8ksgg4ws8c', 1, 0, 0, NULL, '2019-09-24 17:01:14'),
(7, 1, 'sswwo8gwkg88c088kos4gkwsgckk4cc0o4wocsoo', 1, 0, 0, NULL, '2019-09-24 19:13:58'),
(8, 1, '848ccw4co4os4k4ksogwcwkc0kgk8gwkco0o4ogc', 1, 0, 0, NULL, '2019-09-24 21:12:53'),
(9, 1, 'sw04c0koos00g884gcw00804kwwcs80k4csg04cw', 1, 0, 0, NULL, '2019-09-24 21:36:29'),
(10, 1, '88s8ksg0ckcgc08wk4co40wgg4owwocowoo4okcs', 1, 0, 0, NULL, '2019-09-24 21:50:05'),
(11, 1, 'o0ow8wg0c44sc4cc0cg080c0ks0koc0g0gwo4cc0', 1, 0, 0, NULL, '2019-09-25 07:58:13'),
(12, 1, '8kk4ows4kcogccsc0oos40g0skowog0owk0ggck8', 1, 0, 0, NULL, '2019-09-25 08:00:19'),
(13, 1, 'gkkggck0wwkkso8cow4k4gs8ws0w88840oko408k', 1, 0, 0, NULL, '2019-09-25 08:05:17'),
(14, 1, '8wk8gc480k8sgg8008w8o884wkscg0gcgcg8wocg', 1, 0, 0, NULL, '2019-09-25 10:10:00'),
(15, 1, 'ckgk8ss00w044cwgss0844csc0k8swk0wk44ggkw', 1, 0, 0, NULL, '2019-09-25 11:33:02'),
(16, 1, 'wkk0sw440socco4gs4owcswwwc8gk0co0kw4s8go', 1, 0, 0, NULL, '2019-09-25 13:33:19'),
(17, 1, 'w40w8k4wkww8ww0c0coc004s0k48wco8w4cwks0c', 1, 0, 0, NULL, '2019-09-25 13:43:52'),
(20, 1, 'css8okww8cws08go0w4cwgcgc4c8k0ks4cc40g48', 1, 0, 0, NULL, '2019-09-25 17:41:31'),
(21, 1, 'wws8000ks0sw4s4w0wkoc0soocogs4g0o8s0csg4', 1, 0, 0, NULL, '2019-09-25 17:43:40'),
(25, 1, 'gkwsgw04k8ososkko00w4oc4w0gw8ggggcc4swkk', 1, 0, 0, NULL, '2019-09-26 13:47:57'),
(26, 1, '4cgc4s4ko0gosk48s8ssc84ks0ssw0gcg4o8wg8s', 1, 0, 0, NULL, '2019-09-26 13:50:06'),
(27, 1, 'g00sg04cw4cssk4kc44wogkkogk00ck84gss44gs', 1, 0, 0, NULL, '2019-09-26 14:53:14'),
(28, 1, 'gwkc40ow4k4ogcowsc8wskkc48c0w40kwo0ww0w4', 1, 0, 0, NULL, '2019-09-26 17:24:22'),
(30, 1, 'wow4gk80go0wc008g0kg4g00gkkgoogkc0scsws0', 1, 0, 0, NULL, '2019-09-27 11:21:08'),
(31, 1, 'w444scwso0484k400k0g8wwkgcg0o84s88cgkos8', 1, 0, 0, NULL, '2019-09-27 11:32:43'),
(32, 1, 'wowc0w0gs4c8wowco8488oogk0cwc8cwwss0s48g', 1, 0, 0, NULL, '2019-09-27 11:38:56'),
(33, 1, '4so8888osck0cw48o0wow4og0oowo4wkoowowscs', 1, 0, 0, NULL, '2019-09-27 13:01:01'),
(35, 1, 'gkw80sc8s404ccw8kw0ss0gso4cs80og0wc480k8', 1, 0, 0, NULL, '2019-09-27 13:10:29'),
(39, 1, 'g8g0w0kwk0oskog4wwg0wo8c4k044g4ccs0g4w80', 1, 0, 0, NULL, '2019-09-27 17:43:27'),
(41, 1, 's4440c004kccccs8sss8o4s4w8kswk0sckcocgow', 1, 0, 0, NULL, '2019-09-28 17:15:40'),
(42, 1, 'wkg4gckgggwgkco0wocw4w4owkg00osowoscwgkg', 1, 0, 0, NULL, '2019-09-28 17:47:34'),
(43, 1, '0c44ggoswowoc48g4kwc04o88sc84488sgkk4ggk', 1, 0, 0, NULL, '2019-09-29 11:07:08'),
(44, 1, 'c0c08sks888c80ocgww8skggok0c48g4gww44g88', 1, 0, 0, NULL, '2019-09-29 14:06:55'),
(45, 1, 'wkww04g8scgg0g88008swowsk80s8gc080k80o0g', 1, 0, 0, NULL, '2019-09-29 14:18:56'),
(46, 1, 'k88k8g8swkgg44ss8040kc0gs8ow848ggc8kccg0', 1, 0, 0, NULL, '2019-09-30 12:43:35'),
(47, 1, 'w8g44o08sgso08wwc0wgos8gwoo08koogcgs00c0', 1, 0, 0, NULL, '2019-09-30 12:48:27'),
(48, 1, 'w8ossgwwck88ssw088w4kkg044oc08wg8k0wwkcs', 1, 0, 0, NULL, '2019-09-30 14:51:37'),
(49, 1, 'w8o8cc4ssw80og0gswcsgw0oo8ggk4k40cw8c8s0', 1, 0, 0, NULL, '2019-09-30 14:51:47'),
(50, 1, 'kc80ck0g84cwwc4c4gw80s0o8sswwc4kws8swcg8', 1, 0, 0, NULL, '2019-09-30 16:44:25'),
(51, 1, '00c04kw0gk0g8ogso404ws8008kk80wos08g8w8o', 1, 0, 0, NULL, '2019-09-30 22:16:46'),
(52, 1, '48sk40sss8owk0w04wsocwsscwgsosccc8occcgk', 1, 0, 0, NULL, '2019-09-30 22:17:30'),
(53, 1, 'c0wo00o4scs4s40gookko004o8wcgkgs8skgg0go', 1, 0, 0, NULL, '2019-09-30 22:18:29'),
(54, 1, 'g0gkwww0wswks008884g4kwwoskk48w04gwwocc4', 1, 0, 0, NULL, '2019-09-30 22:33:03'),
(55, 1, 'kgg80s8wgcw4wcc88cgoo084wkckccgcsw40wgok', 1, 0, 0, NULL, '2019-10-01 11:16:32'),
(56, 1, '048swk8wgk0k0ckskgg44o4csgsc8cgc48gwg8c8', 1, 0, 0, NULL, '2019-10-01 17:26:27'),
(57, 1, 'goooss80kgg4gckc8sksg0wwgg00g8socwkkcg8c', 1, 0, 0, NULL, '2019-10-01 18:42:21'),
(59, 1, '8g8gooksssok0o48swgws8w88c8ckkgs0ckckos8', 1, 0, 0, NULL, '2019-10-04 15:11:59'),
(60, 1, '4ccg00044wck088wskosk8gcsgw08wck8gwwgwg0', 1, 0, 0, NULL, '2019-10-04 17:37:28'),
(61, 1, 'wwssw4ssk4os8s4ssgosk8k88gc4cwso4cgcc04g', 1, 0, 0, NULL, '2019-10-04 20:16:32'),
(62, 1, 's0gkwskcoogckk40o8go488s48woocwwo8w0c4og', 1, 0, 0, NULL, '2019-10-04 20:17:51'),
(65, 1, 'c4g8k4g8osckwskcsooswo88g0o4s400g4ww04c4', 1, 0, 0, NULL, '2019-10-07 23:03:45'),
(66, 1, 'ksw4gco0swk840coswcocw4cs0cggog8c0oc88o4', 1, 0, 0, NULL, '2019-10-08 18:06:24'),
(67, 81, '0osk0s8c8so84ksk4k0oso88ocg04k8cgc4cwwc8', 1, 0, 0, NULL, '2019-10-08 18:11:12'),
(68, 81, 'kos8w8g4kogos8kcgkww88kw8o4occsokw8wokow', 1, 0, 0, NULL, '2019-10-08 18:11:58'),
(69, 81, '80gg4gkg8cc4s8g8o4so040008cc48k4sscos480', 1, 0, 0, NULL, '2019-10-08 18:12:50'),
(70, 85, 'cws4cwgs4cgg88csck8kg48ss8wc8440kgog480k', 1, 0, 0, NULL, '2019-10-08 18:15:58'),
(71, 85, '04kgw0cc0gwgssgc8cks088w4oo8c8g4w0kscso4', 1, 0, 0, NULL, '2019-10-08 18:18:06'),
(72, 1, '80wgc8w404owok00c0ckow00s8kccwkgcgsgw0s8', 1, 0, 0, NULL, '2019-10-08 18:22:50'),
(73, 81, 'w4ck04gcss8c4so4kgc8oscgc80gcs8c4kwgok0o', 1, 0, 0, NULL, '2019-10-08 18:23:43'),
(74, 85, 'c4wgwko00owwwoc0w08ws8s44gok0w8g8840w0o0', 1, 0, 0, NULL, '2019-10-10 12:24:11'),
(76, 1, 'ck48sgk0gos848swg40ggssw8cg4ow4ksk0w4k40', 1, 0, 0, NULL, '2019-10-10 19:12:13'),
(77, 1, '40w8wsokcc8o40o4gg4k0w0cc4sg4c8g04wc4goc', 1, 0, 0, NULL, '2019-10-10 19:16:35'),
(78, 1, '0ogkcg0o4w8sgo04wo80wsow40sgsksggskgc8oo', 1, 0, 0, NULL, '2019-10-10 19:25:19'),
(79, 1, '0ckgccsgk4kss0w448c0s4w8w44sccsgsosokogc', 1, 0, 0, NULL, '2019-10-10 19:26:21'),
(80, 1, 'sscggo8ggk4gco0g8s4o84ckgkcwccgk8go4wkc8', 1, 0, 0, NULL, '2019-10-10 19:27:20'),
(81, 1, 'ogk00g000wog4go08gco04k04sokcocc4w8o4sss', 1, 0, 0, NULL, '2019-10-10 19:28:03'),
(82, 1, 'ksgwc8sco4swo8cwcwscg4ck4ccgkowg8ccwkcgk', 1, 0, 0, NULL, '2019-10-10 21:02:00'),
(83, 1, '044cog4ggkwwgkwsksc4gskogw80sggwog4gc844', 1, 0, 0, NULL, '2019-10-10 21:12:41'),
(84, 1, 'o0sk4kscs00cg88scgwowswgockg44gksws40oc0', 1, 0, 0, NULL, '2019-10-10 21:41:55'),
(85, 1, 'gwkkkwc0s8kg44wggkw000444ks88800wk8kkkck', 1, 0, 0, NULL, '2019-10-10 21:47:25'),
(86, 1, 'k440cc8o0c8o8g4gkggws88044k80g8ogockwo4c', 1, 0, 0, NULL, '2019-10-10 21:50:51'),
(87, 1, 'cogo0wsoskws4w0ook8kokwsk0kswwgw4o0gs080', 1, 0, 0, NULL, '2019-10-10 21:53:12'),
(88, 1, 'occgw084c0ws8wookw8wcgk400ww048ckssokc8k', 1, 0, 0, NULL, '2019-10-10 22:04:04'),
(89, 76, '0wo8c0ss8ok4gok0cw8co4kkocks888kg40owk88', 1, 0, 0, NULL, '2019-10-10 22:04:45'),
(90, 1, 'kc4k448wk84kk884s0cg44gc84kscsw8ww0cwwsg', 1, 0, 0, NULL, '2019-10-10 22:06:25'),
(91, 1, '4wks8484kkg4o48wo0ckg80kkwo44okow4k80ks0', 1, 0, 0, NULL, '2019-10-11 10:11:26'),
(92, 1, '0s8skogcc84wwk44s4s0gs88sk000ws8k00ssg4g', 1, 0, 0, NULL, '2019-10-11 12:41:57'),
(94, 1, 'gsogscow40s0wwsksgkkg0k488w8ksock088wcc4', 1, 0, 0, NULL, '2019-10-11 14:59:20'),
(96, 1, 'gss4ws0coc4cg4c8wo0wsc08g4wgssogo8oo8ow0', 1, 0, 0, NULL, '2019-10-11 16:19:34'),
(98, 1, 'w8go00wk880wcw8cg40g0s4ogso4g4484w8ocogo', 1, 0, 0, NULL, '2019-10-11 16:39:04'),
(99, 1, '40go0wc44cgsc4gg4g084g0oc0o4owoss8kk00wg', 1, 0, 0, NULL, '2019-10-11 16:46:45'),
(100, 1, 'oc8wkw4gowg0gk0gw08cc8sckc0w4woogoc8ks8s', 1, 0, 0, NULL, '2019-10-11 16:55:06'),
(102, 1, '8c8k4kc044wo8skg4ws0g8444k4k08w0g0ow0gsg', 1, 0, 0, NULL, '2019-10-11 21:03:30'),
(103, 1, '0scgkckkw4g0c8c0488884s8g0kks0scc4swo044', 1, 0, 0, NULL, '2019-10-11 22:08:02'),
(104, 1, '08wc8sw4gw844s4gskosw4kss8kocosgwg0wok0c', 1, 0, 0, NULL, '2019-10-11 22:30:38'),
(105, 1, 'gwks44wws884cswoss804gs0k4k08cc4skw8gcgk', 1, 0, 0, NULL, '2019-10-12 10:11:05'),
(107, 1, '04ck8cgcowo08ssgw8scsok4w4o00wgww880skoc', 1, 0, 0, NULL, '2019-10-12 11:36:14'),
(108, 1, 's04gs0gso8o080s08o40w44wo8k8ks4cswcw00w0', 1, 0, 0, NULL, '2019-10-12 11:58:17'),
(109, 77, 'owwckocokwk0480owk8sgksckkgws8cgows0c0g4', 1, 0, 0, NULL, '2019-10-12 14:16:33'),
(110, 1, '8o4s0wo0ocs04occc4wwk0s4ogg0g8oggwk4kgk0', 1, 0, 0, NULL, '2019-10-12 14:26:03'),
(111, 1, 'cco80o8c4wwgscgw4wgc8ccsg804o4ksgskokkoc', 1, 0, 0, NULL, '2019-10-12 15:12:11'),
(112, 1, '4c8cgkooowkkcsow4w4cgwccos048cwo8s8880sw', 1, 0, 0, NULL, '2019-10-12 15:19:01'),
(115, 1, 'kkwg0ogckcs4wcccgk8cokogsccs448gkgkggkog', 1, 0, 0, NULL, '2019-10-13 15:12:46'),
(116, 1, 'cwgcg44wwkwoc0k44cco444s48kg0c804k8g8osc', 1, 0, 0, NULL, '2019-10-13 15:14:42'),
(117, 1, 'gocgwgcocw4soggc48ok0s488gkcc0o84wo0ckkk', 1, 0, 0, NULL, '2019-10-14 16:33:59'),
(118, 1, 'ocs0oo0wk44wowsw0sosgckccoo8sskk0gck4s4o', 1, 0, 0, NULL, '2019-10-14 16:35:08'),
(122, 87, 'gs4sgkwowkw4kg4g4004sg0cwko0kc0c0oc4c4w0', 1, 0, 0, NULL, '2019-10-14 16:58:57'),
(124, 87, 'cw8osowkskck0k8w4kk88sgcgcscgskokg000sco', 1, 0, 0, NULL, '2019-10-14 17:06:03'),
(129, 87, 'o8ocgc0gcso8kgw40wg4wcs80cwgokgc8sg8c4ww', 1, 0, 0, NULL, '2019-10-14 19:18:06'),
(131, 1, 'kog4ccgscc0c4ks0os88080g0k00gkcggssssw40', 1, 0, 0, NULL, '2019-10-15 15:46:22'),
(132, 1, '0wo0ggsg0cowgkwoc0048cgowskwskwgo4os8wkg', 1, 0, 0, NULL, '2019-10-15 17:06:58'),
(133, 1, '4cw8sgw084cw4ks4gs0kkww84084kwck4o4c80ks', 1, 0, 0, NULL, '2019-10-15 17:18:56'),
(134, 1, 'g0wkwowwwogo0ooco4ow4cok0oo4ksckwsgc4o8c', 1, 0, 0, NULL, '2019-10-15 17:35:51'),
(138, 1, '84sgg048oc4k8kkk0cowo48s0so4k4400ow84kg8', 1, 0, 0, NULL, '2019-10-15 18:26:27'),
(141, 1, 'okk08ss440kkw0o0cggsw8scs00gw4kssoksksco', 1, 0, 0, NULL, '2019-10-16 12:21:03'),
(143, 1, '8k8k8044kw40wowwokgowcw4woos8c4okwc44k0g', 1, 0, 0, NULL, '2019-10-16 14:31:55'),
(145, 1, 'k8s088goso48s8w8844ogwowo80cg0ggk4coccs8', 1, 0, 0, NULL, '2019-10-16 14:41:52'),
(146, 1, '44ggs0g84oss484kko8kgggoc0owggwok8ssk4kc', 1, 0, 0, NULL, '2019-10-16 14:43:50'),
(155, 85, 'skc0o88ssg84gso4gswok00sgs88c08o84c80o8w', 1, 0, 0, NULL, '2019-10-17 11:30:50'),
(162, 87, 'gkgk8kcsw48sw8kk0co8ww0kcwwcw40okksg0s48', 1, 0, 0, NULL, '2019-10-17 12:06:47'),
(163, 1, '4coos4wgc088skkw88g40gkgoswo8c4s8040k088', 1, 0, 0, NULL, '2019-10-17 18:55:34'),
(164, 1, 'okww8kswkok84cgwk44kc4kwsw04w8k88s044ss8', 1, 0, 0, NULL, '2019-10-18 10:13:19'),
(170, 81, 'w0404wgkosgg08owgoggkksg4cc04swg4o4o0k08', 1, 0, 0, NULL, '2019-10-18 10:37:47'),
(171, 92, '0wgs00o4kkgs0sgok000o8g4gcckkwkw8ggg0skw', 1, 0, 0, NULL, '2019-10-18 16:53:58'),
(172, 92, 'k0sws8c0oook888so44gc4g4k8k848css88kk4oo', 1, 0, 0, NULL, '2019-10-18 16:54:45'),
(173, 92, 'cg0ws4ocgsow44o8ocgckcogo8o4o400sogow8sg', 1, 0, 0, NULL, '2019-10-18 17:07:20'),
(174, 92, 's0cwck4kkow48ggksk04co880w48wskc0wo4cgco', 1, 0, 0, NULL, '2019-10-18 17:13:52'),
(175, 92, 'gswwk8wcwc44gsw4c40so484sw8k0w8gcg84swkw', 1, 0, 0, NULL, '2019-10-18 17:32:23'),
(176, 1, 'sg4ckkw8wso44gkwcwgwoscog40goo4wg0og4kkc', 1, 0, 0, NULL, '2019-10-18 17:40:01'),
(177, 1, '08wookgcco8cgok8gkowkk08w8kckg0w48cgk08s', 1, 0, 0, NULL, '2019-10-18 17:42:12'),
(178, 1, '0k8g48kwo0sow0gc4kckscw08c84sg440kowcwcg', 1, 0, 0, NULL, '2019-10-18 17:43:09'),
(180, 1, 'kw4g0w48goo4okk08skwocgk4s8wwoccs0k0ckos', 1, 0, 0, NULL, '2019-10-19 08:45:40'),
(181, 1, '8ws4ksg4skckkgksws4448gssosgoko4ocggg44k', 1, 0, 0, NULL, '2019-10-19 09:49:56'),
(182, 1, '0004k4kk8ggcgw0ww48k8k08oo0kscgg88ko0o4s', 1, 0, 0, NULL, '2019-10-19 10:17:42'),
(183, 1, 'cwks8scg0wok8kgocs4gk0w4kgoc80k4so8ww400', 1, 0, 0, NULL, '2019-10-19 15:04:32'),
(184, 92, '44w8o444koww88ggsk0sgoowc0sko0ckco4osc0g', 1, 0, 0, NULL, '2019-10-19 15:46:25'),
(185, 92, 'soc88ko8kggg4k0k4oc0wksc48wc40ow0kcogk08', 1, 0, 0, NULL, '2019-10-19 15:46:40'),
(186, 92, 'kkw4wowogg0g40cgswcwwkk4cgskck44w4s8css4', 1, 0, 0, NULL, '2019-10-19 15:46:49'),
(187, 79, 'c8w4swg08cw8ko04wgcg4kcwg404o8cc8scw4c40', 1, 0, 0, NULL, '2019-10-20 13:46:39'),
(188, 79, 'w8s04g0s4g0wswkoow80gw0wwogoow0s4o8sk4oc', 1, 0, 0, NULL, '2019-10-20 13:48:50'),
(189, 92, 'swwc8koww800swo8w0oogs4gsw0ogccow00kc4o0', 1, 0, 0, NULL, '2019-10-20 16:00:06'),
(190, 79, 'c4wwggwggco88gwkoco0w8css04cgkgc0wsss8cw', 1, 0, 0, NULL, '2019-10-20 17:09:53'),
(191, 92, 'csc4g00w8oss08kgk8sgco4400c4g0wk0o4k4ksg', 1, 0, 0, NULL, '2019-10-20 21:16:04'),
(192, 92, 'csc0gs4848koosgg88k8k04wkcssoscg8o004s84', 1, 0, 0, NULL, '2019-10-22 16:24:44'),
(195, 1, 'w0cc44kksgos4ookwko8s0gwgskw8s0osogsgogw', 1, 0, 0, NULL, '2019-10-22 18:13:43'),
(198, 1, 'gw04csocskcsw804k8ggccg0csgo4kcwog4k4o8c', 1, 0, 0, NULL, '2019-10-23 11:10:08'),
(199, 1, 'k40ksgkg4s4w8wkwwgsc4ok40w8wc8wokoos4os8', 1, 0, 0, NULL, '2019-10-24 09:23:33'),
(200, 92, 'sc4k8s084w4kcco88s4cccs00w84swskgk8080c8', 1, 0, 0, NULL, '2019-10-24 10:08:48'),
(201, 92, 'gwww4s4swoo8cso0kkcsowss8kc4g8o8ksw00ckk', 1, 0, 0, NULL, '2019-10-24 10:19:35'),
(202, 92, 'cw484cs8gggocko48oksso80scw8ocksgssckk8c', 1, 0, 0, NULL, '2019-10-24 10:19:53'),
(203, 92, 'owkk4ow4okk44o8c4480444cwk400o4w0c08s4o4', 1, 0, 0, NULL, '2019-10-24 11:42:29'),
(204, 92, 'ocsokgs080wwk88ss4scc4kc04wowws00gckscw0', 1, 0, 0, NULL, '2019-10-24 11:53:58'),
(205, 92, 'w0kcwswog8g4cckk08804kckc0wwwococw84g0cc', 1, 0, 0, NULL, '2019-10-24 12:09:02'),
(206, 92, 'c8cook444kkcs8ws0w0wkkckwwkwcs84wk08w488', 1, 0, 0, NULL, '2019-10-24 12:31:59'),
(207, 92, 's88804sk88wccgc8g8c8k4gsks08owwk0ss0g0ws', 1, 0, 0, NULL, '2019-10-24 12:48:04'),
(208, 92, '04kw0wkcgcc0cwswckwo08g48o4k44k4owcck0oo', 1, 0, 0, NULL, '2019-10-24 12:48:17'),
(209, 92, '404os8kow08c8wk0s40w0ck0o8c84o04gw04og0k', 1, 0, 0, NULL, '2019-10-24 17:19:34'),
(210, 1, 'k40s0s80og88ckgw4ckok00sk0ws48oows4wo8gc', 1, 0, 0, NULL, '2019-10-24 22:45:05'),
(211, 85, 'gcck0kk40ws8gosg8swggcowcs0wgkgkswow8gsc', 1, 0, 0, NULL, '2019-10-24 22:46:04'),
(212, 1, 'ok8kcowk0kc8o40g8ks8g84s00o0wg8cgskwwoco', 1, 0, 0, NULL, '2019-10-24 22:48:24'),
(213, 85, '8sgc4wso08oo08k48ok4gw8ss8wsgkog408wsog0', 1, 0, 0, NULL, '2019-10-24 22:56:39'),
(216, 92, '88cgswsow84w8cskk48wwg0gskcs0804kwscwgks', 1, 0, 0, NULL, '2019-10-25 10:47:06'),
(217, 92, 'kccs4gccswkc88gskscs808kg0g0so40w8osw4cg', 1, 0, 0, NULL, '2019-10-25 11:39:09'),
(218, 92, 'cs00coo88wswoog0c804880o0gs8ss48cswc0so8', 1, 0, 0, NULL, '2019-10-25 11:45:53'),
(222, 1, 'c8cgo0w80o0wwss00c0o40kk0ks8go0c4ogcc4co', 1, 0, 0, NULL, '2019-10-26 13:21:08'),
(223, 85, 'cswgws88ckckc0wc4k8kskksss40448go8kkwc8g', 1, 0, 0, NULL, '2019-10-26 21:28:11'),
(224, 85, 'kgcckskwosc88cwkww8ckw800gk4ggsw0wc0sgos', 1, 0, 0, NULL, '2019-10-26 21:46:24'),
(225, 85, '00w4s48o0004s400c04ssosswgsw0ccgowggskoc', 1, 0, 0, NULL, '2019-10-26 21:48:38'),
(226, 92, 'gogsgocskw8k0o0cg80w8gwsw0coksckogggcwc4', 1, 0, 0, NULL, '2019-10-28 10:12:10'),
(227, 92, 'wg4s8ccoo0ogckk4w880w8o8c8ssc4w8o40kosk8', 1, 0, 0, NULL, '2019-10-31 13:25:40'),
(228, 92, '4k0gso00ckowog4s8wskk0c0scgocss8ks4cgc0o', 1, 0, 0, NULL, '2019-10-31 16:42:05'),
(229, 92, 'k8008gs4ck4o044gk8gkk0w8k4oo8448w8g804sw', 1, 0, 0, NULL, '2019-11-14 09:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `ticket_name` varchar(255) NOT NULL,
  `draw_code` varchar(225) NOT NULL,
  `day` enum('SUN','MON','TUE','WED','THU','FRI','SAT','OTHERS') NOT NULL,
  `date` date NOT NULL,
  `set_count` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `ticket_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `ticket_name`, `draw_code`, `day`, `date`, `set_count`, `cost`, `ticket_code`) VALUES
(1, 'POURNAMI', 'RN-416', 'SUN', '2019-11-03', 20, 30, ''),
(2, 'WIN-WIN', 'W-536', 'MON', '2019-11-11', 12, 30, ''),
(3, 'STHREE SAKTHI', 'SS-182', 'TUE', '2019-11-05', 12, 30, ''),
(4, 'AKSHAYA', 'AK-419', 'WED', '2019-11-13', 5, 30, ''),
(5, 'KARUNYA PLUS', 'KN-290', 'THU', '2019-11-14', 56, 30, ''),
(6, 'NIRMAL', 'NR-147', 'FRI', '2019-11-15', 45, 30, 'kr'),
(7, 'KARUNYA', 'KR-422', 'SAT', '2019-11-16', 4, 30, ''),
(8, 'VISHU-BUMPER', 'BR-67', 'OTHERS', '2019-03-04', 4, 30, 'kr'),
(9, 'THIRUVONAM-BUMPER', '', 'OTHERS', '1970-01-01', 0, 30, ''),
(10, 'MONSOON-BUMPER', 'BR-66', 'OTHERS', '1970-01-01', 0, 30, ''),
(11, 'NEWYEAR-BUMPER', '', 'OTHERS', '1970-01-01', 0, 30, ''),
(12, 'SUMMER-BUMPER', '', 'OTHERS', '0000-00-00', 0, 0, ''),
(13, 'POOJA-BUMPER', '', 'OTHERS', '0000-00-00', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `time_settings`
--

CREATE TABLE `time_settings` (
  `id` int(15) NOT NULL,
  `from_time` varchar(50) NOT NULL,
  `to_time` varchar(50) NOT NULL,
  `type` enum('FILE','UNSOLD') NOT NULL,
  `is_active` enum('YES','NO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_settings`
--

INSERT INTO `time_settings` (`id`, `from_time`, `to_time`, `type`, `is_active`) VALUES
(1, '11:15 AM', '11:15 AM', 'FILE', 'NO'),
(2, '11:15 AM', '11:15 AM', 'UNSOLD', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `unsold_data`
--

CREATE TABLE `unsold_data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_set` enum('YES','NO') NOT NULL,
  `ticket_code` varchar(500) NOT NULL,
  `ticket_from` varchar(50) NOT NULL,
  `ticket_to` varchar(50) NOT NULL,
  `total` int(11) NOT NULL,
  `tickets_id` int(11) NOT NULL,
  `ticket_count` int(50) NOT NULL,
  `ticket_quantity` int(225) NOT NULL,
  `is_draft` int(11) NOT NULL DEFAULT '1',
  `type` enum('RETAIL','WHOLESALE') NOT NULL,
  `sold_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_type` enum('ADMIN','AGENT','SHOP','SUPERADMIN','OTHERS','TECHNICIAN') DEFAULT NULL,
  `shop_code` varchar(50) NOT NULL,
  `system_ip` varchar(225) NOT NULL,
  `logged_in` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `middlename`, `lastname`, `password`, `created_at`, `updated_at`, `deleted_at`, `user_type`, `shop_code`, `system_ip`, `logged_in`) VALUES
(1, 'superadmin', 'Agency', '', 'Name', 'agency1234', NULL, NULL, NULL, 'SUPERADMIN', '', '', '0000-00-00 00:00:00'),
(83, 'hamerkop', 'Hamerkop', NULL, 'Infotech', 'hamerkop@123', NULL, NULL, NULL, 'TECHNICIAN', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_files`
--

CREATE TABLE `user_files` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `enable_archive_option` enum('YES','NO') DEFAULT 'YES',
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_files_viewable`
--

CREATE TABLE `user_files_viewable` (
  `id` int(11) NOT NULL,
  `user_files_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `is_enabled` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `user_type`, `is_enabled`) VALUES
(1, 'SUPERADMIN', 1),
(2, 'ADMIN', 1),
(3, 'SHOP', 1),
(4, 'AGENT', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consolidated_sold_data`
--
ALTER TABLE `consolidated_sold_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tickets_id` (`tickets_id`);

--
-- Indexes for table `daily_dc`
--
ALTER TABLE `daily_dc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `shop_code` (`shop_code`),
  ADD KEY `bill_no` (`bill_no`),
  ADD KEY `date` (`date`),
  ADD KEY `ticket_id` (`ticket_id`),
  ADD KEY `price_type` (`price_type`);

--
-- Indexes for table `daily_winning`
--
ALTER TABLE `daily_winning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_settings`
--
ALTER TABLE `time_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unsold_data`
--
ALTER TABLE `unsold_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `unsold_data_ibfk_1` (`tickets_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `id` (`id`),
  ADD KEY `sold_date` (`sold_date`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_files`
--
ALTER TABLE `user_files`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_user_files_user_idx` (`user_id`);

--
-- Indexes for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  ADD PRIMARY KEY (`id`,`user_files_id`,`user_id`),
  ADD KEY `fk_user_files_viewable_user_files1_idx` (`user_files_id`),
  ADD KEY `fk_user_files_viewable_user1_idx` (`user_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consolidated_sold_data`
--
ALTER TABLE `consolidated_sold_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_dc`
--
ALTER TABLE `daily_dc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_winning`
--
ALTER TABLE `daily_winning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_keys`
--
ALTER TABLE `tbl_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `time_settings`
--
ALTER TABLE `time_settings`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `unsold_data`
--
ALTER TABLE `unsold_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `user_files`
--
ALTER TABLE `user_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `consolidated_sold_data`
--
ALTER TABLE `consolidated_sold_data`
  ADD CONSTRAINT `fk_tickets_id` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`);

--
-- Constraints for table `daily_dc`
--
ALTER TABLE `daily_dc`
  ADD CONSTRAINT `dailydcticket` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_files`
--
ALTER TABLE `user_files`
  ADD CONSTRAINT `fk_user_files_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_files_viewable`
--
ALTER TABLE `user_files_viewable`
  ADD CONSTRAINT `fk_user_files_viewable_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_files_viewable_user_files1` FOREIGN KEY (`user_files_id`) REFERENCES `user_files` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
